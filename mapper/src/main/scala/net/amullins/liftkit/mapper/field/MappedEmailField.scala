package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.mapper._

import scala.xml._

import net.amullins.liftkit.common.StringHelpers._


class MappedEmailField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, maxLen: Int)
	extends MappedStringField(fieldOwner, maxLen)
{

	private def isValid_?(v: String): List[FieldError] = {
		if ( (!required_? && this.i_is_! == defaultValue) || MappedEmail.validEmailAddr_?(this.i_is_!) ) Nil
		else List(FieldError(this, Text(S ? "invalid.email.address")))
	}

	override def validations = valMaxLen(maxLen, S ? "email.address.too.long") _ :: isValid_? _ :: super.validations
	override def setFilter = toLower _ :: trim _ :: super.setFilter

}


// comma-separated list of email address (email@domain.com,email1@domain.com,email2@domain.com)
class MappedEmailListField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedTextField(fieldOwner)
{

	def toList: List[String] = {
		this.get.box match { case Full(str) => str.split(",").toList case _ => Nil }
	}

}
