package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.mapper._
import net.liftweb.http.js.JsExp
import net.liftweb.http.js.JE
import net.liftweb.json.JsonAST

import java.sql.Types
import java.lang.reflect.Method
import java.util.Date

import scala.xml._
import scala.reflect.runtime.universe.typeTag

import net.amullins.liftkit.common.StringHelpers._


class MappedTextField[T <: Mapper[T]](val fieldOwner: T)
	extends MappedField[String, T]
	with Validatable[String, T]
{
	private val data: FatLazy[String] = FatLazy(defaultValue)
	private val orgData: FatLazy[String] = FatLazy(defaultValue)

	val manifest = typeTag[String]

	def sourceInfoMetadata(): SourceFieldMetadata{type ST = String} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
		  import net.liftweb.json.JsonAST._

	    type T = String
	    def asString(v: T): String = v
	    def asNodeSeq(v: T): Box[NodeSeq] = Full(Text(v))
	    def asJson(v: T): Box[JValue] = Full(JsonAST.JString(v))
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

	def valMinLen(n: Int, msg: => String)(v: String): List[FieldError] = {
		if (v != null) {
			if ( v.length < n ) List(FieldError(this, Text(msg)))
			else Nil
		} else {
			List(FieldError(this, Text(msg)))
		}
	}

	// to html textarea element
	def toTextarea: Elem = toTextarea()
	def toTextarea(attrs: (String, String)*): Elem = {
		addCssClass(
			getMessageClass,
			SHtml.textarea(this.get, saveInput, attrs:_*)
		)
	}
	def toTextarea(onSave: String => Any = saveInput, attributes: List[(String, String)] = Nil): Elem = {
		addCssClass(
			getMessageClass,
			SHtml.textarea(this.get, onSave, attributes:_*)
		)
	}

	// to html input[type=text] element
	def toInput: Elem = toInput(attributes = List("class" -> "text"))
	def toInput(onSave: String => Any = saveInput, attributes: List[(String, String)] = Nil): Elem = {
		addCssClass(
			getMessageClass,
			SHtml.text(this.get, onSave, attributes:_*)
		)
	}

	def saveInput(str: String) {
		str.box match {
			case Full(_) => this.apply(str)
			case _ if null == this.defaultValue => this.apply(null)
			case _ => this.apply(str)
		}
	}


	protected def real_i_set_!(value: String): String = {
		if (value != data.get) {
			data() = value
			this.dirty_?(true)
		}
		value
	}

	def dbFieldClass = classOf[String]

	/**
	 * Get the JDBC SQL Type for this field
	 */
	//  def getTargetSQLType(field : String) = Types.BINARY
	def targetSQLType = Types.VARCHAR

	def defaultValue: String = ""

	override def writePermission_? = true

	override def readPermission_? = true

	protected def i_is_! = data.get

	protected def i_was_! = orgData.get

	override def doneWithSave() {orgData.setFrom(data)}

	def asJsExp: JsExp = JE.Str(this.get)

	def asJsonValue: Box[JsonAST.JValue] = Full(this.get match {
		case null => JsonAST.JNull
		case str => JsonAST.JString(str)
	})

	protected def i_obscure_!(in: String): String = ""

	override def setFromAny(in: Any): String = {
		in match {
			case JsonAST.JNull => this.set(null)
			case JsonAST.JString(str) => this.set(str)
			case seq: Seq[_] if seq.nonEmpty => seq.map(setFromAny).apply(0)
			case (s: String) :: _ => this.set(s)
			case s :: _ => this.setFromAny(s)
			case null => this.set(null)
			case s: String => this.set(s)
			case Some(s: String) => this.set(s)
			case Full(s: String) => this.set(s)
			case None | Empty | Failure(_, _, _) => this.set(null)
			case o => this.set(o.toString)
		}
	}

	def jdbcFriendly(field: String): Object = real_convertToJDBCFriendly(data.get)


	def real_convertToJDBCFriendly(value: String): Object = value match {
		case null => null
		case s => s
	}

	def buildSetActualValue(accessor: Method, inst: AnyRef, columnName: String): (T, AnyRef) => Unit =
		(inst, v) => doField(inst, accessor, {
			case f: MappedTextField[T] =>
				val toSet = v match {
					case null => null
					case s: String => s
					case ba: Array[Byte] => new String(ba, "UTF-8")
					case clob: java.sql.Clob => clob.getSubString(1, clob.length.toInt)
					case other => other.toString
				}
				f.data() = toSet
				f.orgData() = toSet
		})

	def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit = null

	def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedTextField[T] =>
        val toSet = v match {
          case null => null
          case other => other
        }
        f.data() = toSet
        f.orgData() = toSet
    })

	def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit = null

	def buildSetBooleanValue(accessor: Method, columnName: String): (T, Boolean, Boolean) => Unit = null

	def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.clobColumnType + notNullAppender()
}