package net.amullins.liftkit.mapper

import net.liftweb.mapper._


/**
 * For cloning a mapper instance.
 * Copies everything from an existing instance to a new instance (except the primary key)
 * @tparam K The net.liftweb.mapper.Mapper key type
 * @tparam M The net.liftweb.mapper.Mapper type
 */
trait CanClone[K, M <: KeyedMapper[K, M]] {
	self: KeyedMetaMapper[K, M] with KeyedMapper[K, M] =>

	def cloneInstance(in: M): M = cloneInstance(in, create)

	def cloneInstance(in: M, out: M): M = {
		// copy from existing instance to new instance
		def cp(bmf: BaseMappedField) {
			for {
				imf: MappedField[Any, M] <- in.fieldByName[Any](bmf.name)
				omf: MappedField[Any, M] <- out.fieldByName[Any](bmf.name)
			} omf.setFromAny(imf.get)
		}
		mappedFields.filterNot(bmf => columnPrimaryKey_?(bmf.name)).foreach(cp)

		out
	}

}

/**
 * @tparam K The net.liftweb.mapper.Mapper key type
 * @tparam M The net.liftweb.mapper.Mapper type
 */
trait CanCloneThis[K <: Any, M <: KeyedMapper[K, M]] {
	self: KeyedMapper[K, M] {
		def getSingleton: KeyedMetaMapper[K, M] with KeyedMapper[K, M] with CanClone[K, M]
	} =>

	override def clone() = getSingleton.cloneInstance(this)

	/**
	 * Set an existing instance's fields' values to match those of this instance
	 * @param out The existing instance that will be modified.
	 * @return The modified instance.
	 */
	def cloneTo(out: M): M = getSingleton.cloneInstance(this, out)
}