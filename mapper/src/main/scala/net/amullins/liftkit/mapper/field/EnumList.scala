package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util._
import net.liftweb.http.SHtml, SHtml._

import scala.xml._


private[mapper] trait EnumList[
  T <: Mapper[T],
  EnumType <: { def id: Int; def toString(): String }
] {
  self: MappedField[Seq[EnumType], T] with Validatable[Seq[EnumType], T]  =>

  def getValue(i: Int): EnumType
  def allValues: Seq[EnumType]
  def validValues: Seq[EnumType] = allValues.filter(_.id > 0)

  // this may contain invalid values (for default item in a select input)
  def buildDisplayList: List[(Int, String)] = allValues.toList.map(a => (a.id, a.toString())).sortWith(_._1 < _._1)

  def toCheckboxGroup: ChoiceHolder[EnumType] =
  	SHtml.checkbox(
      validValues,
  		this.get,
  		this.set _
  	)

  def toSelect: Elem =
    SHtml.multiSelectObj[Int](
  		buildDisplayList,
  		this.get.map(_.id),
  		v => this.set(v.map(getValue)),
  		"class" -> getMessageClass.openOr("")
  	)

  override def _toForm: Box[NodeSeq] = Full(toSelect)


  // keep this list tidy by removing duplicates on save
  def uniqueValues_? = true

  // "Please Select" has an id of 0 and is an INVALID selection
  def isValid_?(vSeq: Seq[EnumType]): List[FieldError] =
  	vSeq.toList.filter(_.id == 0).map(v =>
  		FieldError(this, "\"%s\" is an invalid option for %s.".format(v.toString(), displayName))
  	)

  def minSelectErrMsg = "You must select an item from " + displayName + "."
  protected def minSelection(n: Int, errorMsg: String = minSelectErrMsg)(vSeq: Seq[EnumType]): List[FieldError] = {
  	if (vSeq.lengthCompare(n) >= 0) Nil
  	else List(FieldError(this, errorMsg))
  }

  // removes duplicate values from Seq
  def distinct(vSeq: Seq[EnumType]): Seq[EnumType] = if ( uniqueValues_? ) vSeq.distinct else vSeq

}
