package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers.tryo
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST._

import java.lang.reflect.Method
import java.util.Date

import scala.xml._
import scala.collection.JavaConverters._


abstract class MappedStringArray[T <: Mapper[T]](val fieldOwner: T)
  extends MappedArray[T, String]("varchar")

abstract class MappedIntArray[T <: Mapper[T]](val fieldOwner: T)
  extends MappedArray[T, Int](
    "int4",
    convertToJson = value => Full(JArray(value.map(v => JInt(v)).toList)),
    convertToJsExp = value => JE.JsArray(value.map(v => JE.Num(v)) :_*)
  )

abstract class MappedLongArray[T <: Mapper[T]](val fieldOwner: T)
  extends MappedArray[T, Long](
    "int8",
    convertToJson = value => Full(JArray(value.map(v => JInt(v)).toList)),
    convertToJsExp = value => JE.JsArray(value.map(v => JE.Num(v)) :_*)
  )


/**
 * PostgreSQL array field.
 *
 * @param jdbcTypeName The field type according to the PostgreSQL driver
 * @param convertToString How to convert from the ArrayType to a string.
 * @param convertToNodeSeq How to convert from the ArrayType to a NodeSeq.
 * @param convertToJson How to convert from the ArrayType to JSON.
 * @param convertToJsExp How to convert from the ArrayType to JavaScript.
 * @tparam T The mapper instance type to which this field belongs.
 * @tparam ArrayType The scala type to which this field is mapped.
 */
abstract class MappedArray[T <: Mapper[T], ArrayType](
	val jdbcTypeName: String,
  val convertToString: Seq[ArrayType] => String = (value: Seq[ArrayType]) => value.map(_.toString).mkString(", "),
  val convertToNodeSeq: Seq[ArrayType] => Box[NodeSeq] = (value: Seq[ArrayType]) => Full(Text(value.map(_.toString).mkString(", "))),
  val convertToJson: Seq[ArrayType] => Box[JValue] = (value: Seq[ArrayType]) => Full(JArray(value.toList.map(x => JString(x.toString)))),
  val convertToJsExp: Seq[ArrayType] => JsExp = (value: Seq[ArrayType]) => JE.JsArray(value.map(v => JE.Str(v.toString)) :_*)
)
	extends MappedField[Seq[ArrayType], T]
	with Validatable[Seq[ArrayType], T]
	with HtmlHelpers
  with Logger
{

	private var data: Seq[ArrayType] = defaultValue
	private var orgData: Seq[ArrayType] = defaultValue
	def defaultValue: Seq[ArrayType] = Nil
	def dbFieldClass = manifest.tpe.erasure.asInstanceOf[Class[Seq[ArrayType]]]

  def targetSQLType = java.sql.Types.ARRAY

	def sourceInfoMetadata(): SourceFieldMetadata{type ST = Seq[ArrayType]} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
	    type T = Seq[ArrayType]
	    def asString(v: T): String = convertToString(v)
	    def asNodeSeq(v: T): Box[NodeSeq] = convertToNodeSeq(v)
	    def asJson(v: T): Box[JValue] = convertToJson(v)
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

  protected def i_is_! = data
  protected def i_was_! = orgData

  override def doneWithSave() {
    orgData = data
  }

  protected def real_i_set_!(value: Seq[ArrayType]): Seq[ArrayType] = {
    if (value != data) {
      data = value
      dirty_?(true)
    }
    data
  }
  override def readPermission_? = true
  override def writePermission_? = true

  def asJsExp: JsExp = convertToJsExp(this.get)

  def asJsonValue: Box[JValue] = convertToJson(this.get)

  private def asJavaArray(value: Seq[ArrayType]) = seqAsJavaListConverter(value).asJava.toArray

  // override this if the ArrayType is something other than a basic value
  protected def convertToJDBCValue(value: Seq[ArrayType]): Seq[Any] = value

  def real_convertToJDBCFriendly(value: Seq[ArrayType]): Object =
    net.liftweb.db.DB.currentConnection.map { conn =>
      conn.connection.createArrayOf(jdbcTypeName, seqAsJavaListConverter(convertToJDBCValue(value)).asJava.toArray)
    } openOr null

  def jdbcFriendly(field: String) = real_convertToJDBCFriendly(this.get)

  // override this if you can convert TO ArrayType from an Int
  protected def setFromInt(i: Int): Seq[ArrayType] = {
    warn("Trying to set a array field's value from an Int without a conversion function. Field's value will be set to [].")
    this.set(Seq.empty)
  }

  override def setFromAny(in: Any): Seq[ArrayType] = {
    in match {
      case JInt(bi) => setFromInt(bi.intValue())
      case n: Long => setFromInt(n.toInt)
      case n: Number => setFromInt(n.intValue())
      case (n: Number) :: _ => setFromInt(n.intValue())
      case Some(n: Number) => setFromInt(n.intValue())
      case None => this.set(Nil)
      case (s: String) :: _ => setFromInt(Helpers.toInt(s))
      case vs: List[ArrayType] => this.set(vs)
      case null => this.set(Nil)
      case s: String => setFromInt(Helpers.toInt(s))
      case o => setFromInt(Helpers.toInt(o))
    }
  }

	def +=(in: ArrayType) = {
		this.set(this.i_is_! :+ in)
	}

	def ++=(in: Seq[ArrayType]) = {
		this.set(this.i_is_! ++ in)
	}

  protected def i_obscure_!(in : Seq[ArrayType]) = Nil

  private def st(in: Seq[ArrayType]) {
    data = in
    orgData = in
  }

  def buildSetActualValue(accessor: Method, data: AnyRef, columnName: String): (T, AnyRef) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedArray[T, ArrayType] =>
        v match {
          case null => f.st(Seq.empty[ArrayType])
          case jArr: java.sql.Array =>
            f.st(jArr.getArray.asInstanceOf[Array[ArrayType]].toSeq)
        }
	  })

  def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedArray[T, ArrayType] =>
		    f.st(defaultValue)
	  })

  def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedArray[T, ArrayType] =>
		    f.st(defaultValue)
	  })

  def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedArray[T, ArrayType] =>
		    f.st(defaultValue)
	  })

  def buildSetBooleanValue(accessor: Method, columnName : String): (T, Boolean, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedArray[T, ArrayType] =>
		    f.st(defaultValue)
	  })

  def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.enumListColumnType + notNullAppender()

	override def get_? : Box[Seq[ArrayType]] = this.get match {
		case Nil => Empty
		case _ => Full(this.get)
	}

	def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

  // keep this list tidy by removing duplicates on save
  def uniqueValues_? = true

  // removes duplicate values from Seq
  def distinct(vSeq: Seq[ArrayType]): Seq[ArrayType] = if ( uniqueValues_? ) vSeq.distinct else vSeq

  override def setFilter = distinct _ :: super.setFilter
}