package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.http.js.JsCmd
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.mapper._

import scala.xml._


class MappedLongForeignField[FieldOwner <: Mapper[FieldOwner], Foreign <: KeyedMapper[Long, Foreign]](
	fieldOwner: FieldOwner,
	_foreign: => KeyedMetaMapper[Long, Foreign]
)
	extends MappedLongForeignKey(fieldOwner, _foreign)
	with Validatable[Long, FieldOwner]
	with HtmlHelpers
	with Foreigner
{
	def defaultErrorMsg = this.displayName + " is required."

	override val valHasObj = { (v: Long) =>
		if (obj.isEmpty) List(FieldError(this, defaultErrorMsg))
		else Nil
	}

	override def get_? : Box[Long] = if (this.get == 0L) Empty else Full(this.get)

	def getOrElse[V](v: V): Either[V, Long] = if (this.get == 0L) Left(v) else Right(this.get)

	// to html select element
	def toSelect(defaultItem: Box[String] = Empty): Elem = {
		(for(vals <- validSelectValues if vals.nonEmpty) yield {
			SHtml.selectObj(
				defaultItem.map(d => List((0L, d)) ++ vals).openOr(vals),
				if (this.get == 0L) Empty else Full(this.get),
				this.set,
        getMessageClass.toSeq.map("class" -> _) : _*
			)
		}) openOr <select><option>No Values Specified</option></select>
	}

	def toAjaxSelect(defaultItem: Box[String] = Empty, onSelect: Long => JsCmd): Elem = {
		(for(vals <- validSelectValues if vals.nonEmpty) yield {
			SHtml.ajaxSelectObj(
				defaultItem.map(d => List((0L, d)) ++ vals).openOr(vals),
				if (this.get == 0L) Empty else Full(this.get),
				(selected: Long) => {
					set(selected)
					onSelect(selected)
				},
        getMessageClass.toSeq.map("class" -> _) : _*
			)
		}) openOr <select><option>No Values Specified</option></select>
	}
}