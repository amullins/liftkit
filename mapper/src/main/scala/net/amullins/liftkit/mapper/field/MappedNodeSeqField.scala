package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.mapper._
import net.liftweb.json._
import net.liftweb.http.js._

import java.util.Date
import java.sql.Types
import java.lang.reflect.Method

import scala.xml._
import scala.reflect.runtime.universe.TypeTag

import net.amullins.liftkit.common.date.{Helpers => DateHelpers}


abstract class MappedNodeSeqField[T <: Mapper[T]](val fieldOwner: T)(implicit val manifest: TypeTag[NodeSeq])
	extends MappedField[NodeSeq, T]
	with Validatable[NodeSeq, T]
{

	private val data: FatLazy[NodeSeq] = FatLazy(defaultValue)
	private val orgData: FatLazy[NodeSeq] = FatLazy(defaultValue)

	protected def real_i_set_!(value: NodeSeq): NodeSeq = {
		data() = value
		this.dirty_?(true)
		value
	}

	def toTextarea: Elem = addCssClass(getMessageClass.openOr(""), SHtml.textarea(Html5.toString(Group(this.get)), this.setFromAny))

	def dbFieldClass = classOf[NodeSeq]

	def targetSQLType = Types.VARCHAR

	import JsonAST._
	def sourceInfoMetadata(): SourceFieldMetadata{type ST = NodeSeq} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
	    type T = NodeSeq
	    def asString(v: T): String = v.toString()
	    def asNodeSeq(v: T): Box[NodeSeq] = if (v.isEmpty) Empty else Full(v)
	    def asJson(v: T): Box[JValue] = if (v.isEmpty) Empty else Full(JString(v.toString()))
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

	def defaultValue: NodeSeq = NodeSeq.Empty

	override def writePermission_? = true

	override def readPermission_? = true

	protected def i_is_! = data.get

	protected def i_was_! = orgData.get

	override def doneWithSave() {
		orgData.setFrom(data)
	}

	def asJsExp: JsExp = JE.Str(this.get.toString())

	def asJsonValue: Box[JsonAST.JValue] = Full(this.get match {
		case null => JsonAST.JNull
		case ns => JsonAST.JString(ns.toString())
	})

	protected def i_obscure_!(in: NodeSeq): NodeSeq = NodeSeq.Empty

	override def setFromAny(in: Any): NodeSeq = {
		in match {
			case JsonAST.JNull => this.set(null)
			case JsonAST.JString(str) => this.set(Html5.parse(str).openOr(NodeSeq.Empty))
			case seq: Seq[_] if seq.nonEmpty => seq.map(setFromAny).apply(0)
			case (s: String) :: _ => this.set(Html5.parse(s).openOr(NodeSeq.Empty))
			case s :: _ => this.setFromAny(s)
			case null => this.set(null)
			case s: String => this.set(Html5.parse(s).openOr(NodeSeq.Empty))
			case Some(s: String) => this.set(Html5.parse(s).openOr(NodeSeq.Empty))
			case Full(s: String) => this.set(Html5.parse(s).openOr(NodeSeq.Empty))
			case None | Empty | Failure(_, _, _) => this.set(null)
			case o => this.set(Html5.parse(o.toString).openOr(NodeSeq.Empty))
		}
	}

	def jdbcFriendly(field: String): Object = real_convertToJDBCFriendly(data.get)

	def real_convertToJDBCFriendly(value: NodeSeq): Object = value match {
		case null => null
		case ns => Html5.toString(Group(value))
	}

	def buildSetActualValue(accessor: Method, inst: AnyRef, columnName: String): (T, AnyRef) => Unit =
		(inst, v) => doField(inst, accessor, {
			case f: MappedNodeSeqField[T] =>
				val toSet = {
					if (v eq null) {
						null
					} else {
						v match {
							case ns: NodeSeq => ns
							case _ =>
								Html5.parse(v match {
									case s: String => s
									case ba: Array[Byte] => new String(ba, "UTF-8")
									case clob: java.sql.Clob => clob.getSubString(1, clob.length.toInt)
									case other => other.toString
								}).openOr(NodeSeq.Empty)
						}
					}
				}
				f.data() = toSet
				f.orgData() = toSet
		})

	def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit =
    (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedNodeSeqField[T] =>
        val toSet = if (isNull) null else Html5.parse(v.toString).openOr(defaultValue)
        f.data() = toSet
        f.orgData() = toSet
    })

	def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedNodeSeqField[T] =>
        val toSet = if (v eq null) null else Html5.parse(v).openOr(defaultValue)
        f.data() = toSet
        f.orgData() = toSet
    })

	def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedNodeSeqField[T] =>
        val toSet = if (v eq null) null else Html5.parse(DateHelpers.java.fullDateTime.format(v)).openOr(defaultValue)
        f.data() = toSet
        f.orgData() = toSet
    })

	def buildSetBooleanValue(accessor: Method, columnName: String): (T, Boolean, Boolean) => Unit =
    (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedNodeSeqField[T] =>
        val toSet = if (isNull) null else Html5.parse(v.toString).openOr(defaultValue)
        f.data() = toSet
        f.orgData() = toSet
    })

	def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.clobColumnType + notNullAppender()
}