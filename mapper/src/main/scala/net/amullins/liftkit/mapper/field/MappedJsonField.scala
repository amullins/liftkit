package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.json._


class MappedJsonField[FieldOwner <: Mapper[FieldOwner], ToExtract <: AnyRef](fieldOwner: FieldOwner)(implicit man: Manifest[ToExtract])
  extends MappedTextField(fieldOwner)
{
  protected implicit val __formats: Formats = DefaultFormats

  def extract: Box[ToExtract] = {
    for {
      jString <- super.get_?
      extracted <- parse(jString).extractOpt[ToExtract]
    } yield extracted
  }

	override def setFromAny(in: Any) = in match {
		case t: ToExtract => this.set(Serialization.write(t))
		case _ => super.setFromAny(in)
	}

	def apply(t: ToExtract)(implicit tman: Manifest[ToExtract]) = {
		this.set(Serialization.write(t))
		this.fieldOwner
	}
}
