package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.mapper._

import scala.xml._

import net.amullins.liftkit.common.HtmlHelpers


class MappedIntField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
    extends MappedInt(fieldOwner)
    with Validatable[Int, FieldOwner]
    with HtmlHelpers
{

	def valMin(msg: String)(v: Int): List[FieldError] =
		if ( v<=0 ) List(FieldError(this, Text(msg)))
		else Nil

  def toInput: NodeSeq = this.toInput("class" -> "text")
  def toInput(attrs: (String, String)*): NodeSeq = {
  	val _attrs = {
  		attrs.filterNot(_._1 == "class") ++
  		attrs.find(_._1 == "class").map(attr =>
  			("class", (attr._2.trim() + " digits " + getMessageClass.openOr("")).trim())
  		)
  	}

  	val e = HtmlHelpers.form.int(this.get_?, set _, _attrs : _*)

  	e :+ HtmlHelpers.form.numberMaskScript(e)
  }

}
