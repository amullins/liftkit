package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST

import java.lang.reflect.Method
import java.util.Date

import scala.xml._
import scala.reflect.runtime.universe.TypeTag

import net.amullins.liftkit.common.CustomEnum


abstract class MappedCustomEnumField[T <: Mapper[T], EnumType <: CustomEnum[EnumType]#Value, ENUM <: CustomEnum[EnumType]](
	val fieldOwner: T,
	val enum: ENUM
)(
	implicit val manifest: TypeTag[EnumType]
)
	extends MappedField[EnumType, T]
	with Validatable[EnumType, T]
	with HtmlHelpers
{

	private var data: EnumType = defaultValue
	private var orgData: EnumType = defaultValue
	def defaultValue: EnumType = enum.all.head
	def dbFieldClass = manifest.tpe.erasure.asInstanceOf[Class[EnumType]]

	def targetSQLType = java.sql.Types.BIGINT

	def sourceInfoMetadata(): SourceFieldMetadata{type ST = EnumType} =
		SourceFieldMetadataRep(name, manifest, new FieldConverter {
		  type T = EnumType
		  def asString(v: T): String = v.toString()
		  def asNodeSeq(v: T): Box[NodeSeq] = Full(Text(asString(v)))
		  def asJson(v: T): Box[JsonAST.JValue] = Full(JsonAST.JInt(v.id))
		  def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
		})

	protected def i_is_! = data
	protected def i_was_! = orgData

	override def doneWithSave() {
		orgData = data
	}

	protected def real_i_set_!(value: EnumType): EnumType = {
		if (value != data) {
			data = value
			dirty_?(true)
		}
		data
	}
	override def readPermission_? = true
	override def writePermission_? = true

	def real_convertToJDBCFriendly(value: EnumType): Object = new java.lang.Integer(value.id)

	def toInt = this.get.id
	def fromInt(in: Int): EnumType = enum(in)

	def jdbcFriendly(field: String) = new java.lang.Integer(toInt)
	override def jdbcFriendly = new java.lang.Integer(toInt)

	def asJsExp: JsExp = JE.Num(this.get.id)

	def asJsonValue: Box[JsonAST.JValue] = Full(JsonAST.JInt(this.get.id))


	override def setFromAny(in: Any): EnumType = {
		in match {
			case JsonAST.JInt(bi) => this.set(fromInt(bi.intValue()))
			case n: Int => this.set(fromInt(n))
			case n: Long => this.set(fromInt(n.toInt))
			case n: Number => this.set(fromInt(n.intValue))
			case (n: Number) :: _ => this.set(fromInt(n.intValue))
			case Some(n: Number) => this.set(fromInt(n.intValue))
			case Full(n: Number) => this.set(fromInt(n.intValue))
			case None | Empty | Failure(_, _, _) => this.set(defaultValue)
			case (s: String) :: _ => this.set(fromInt(Helpers.toInt(s)))
			case vs if vs.getClass.isAssignableFrom(manifest.tpe.erasure.getClass) => this.set(vs.asInstanceOf[EnumType])
			case null => this.set(defaultValue)
			case s: String => this.set(fromInt(Helpers.toInt(s)))
			case o => this.set(fromInt(Helpers.toInt(o)))
		}
	}

	protected def i_obscure_!(in: EnumType) = defaultValue

	private def st(in: EnumType) {
		data = in
		orgData = in
	}


	def buildSetActualValue(accessor: Method, data: AnyRef, columnName: String): (T, AnyRef) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumField[T, EnumType, ENUM] =>
        f.st(if (v eq null) defaultValue else fromInt(Helpers.toInt(v.toString)))
    })

	def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit =
    (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumField[T, EnumType, ENUM] =>
        f.st(if (isNull) defaultValue else fromInt(v.toInt))
    })

	def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumField[T, EnumType, ENUM] =>
        f.st(if (v eq null) defaultValue else fromInt(Helpers.toInt(v)))
    })

	def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit =
    (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumField[T, EnumType, ENUM] =>
        f.st(if (v eq null) defaultValue else fromInt(Helpers.toInt(v)))
    })

	def buildSetBooleanValue(accessor: Method, columnName: String): (T, Boolean, Boolean) => Unit =
    (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumField[T, EnumType, ENUM] =>
        f.st(defaultValue)
    })


	def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.enumColumnType + notNullAppender()

	override def get_? : Box[EnumType] = {
		if (enum.validValues.contains(this.get)) Full(this.get)
		else Empty
	}

	def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

	def isValid_?(v: EnumType): List[FieldError] = {
		if (enum.validValues.contains(this.get)) Nil
		else List(FieldError(this, Text(defaultErrorMsg)))
	}

	def buildDisplayList: List[(Int, String)] =
    enum.all.toList.map(a => (a.id, a.toString())).sortWith(_._1 < _._1)

	override def _toForm: Box[NodeSeq] = Full(toSelect)

	// to html select element
	def toSelect: Elem = toSelect()
	def toSelect(attrs: (String, String)*): Elem = toSelect((v: EnumType) => {}, attrs:_*)
	def toSelect(onSubmit: EnumType => Any, attrs: (String, String)*): Elem = {
		addCssClass(
			getMessageClass,
			SHtml.selectObj[Int](
				buildDisplayList,
				if (this.get != null) Full(toInt) else Empty,
				(v: Int) => {
					this.set(this.fromInt(v))
					onSubmit(this.fromInt(v))
				},
				attrs : _*
			)
		)
	}


	def toAjaxSelect: Elem = toAjaxSelect()
	def toAjaxSelect(attrs: (String, String)*): Elem = toAjaxSelect(v => JsCmds.Noop, attrs:_*)
	def toAjaxSelect(onChange: EnumType => JsCmd, attrs: (String, String)*): Elem = {
		import net.liftweb.http.SHtml._
		SHtml.ajaxSelectObj[Int](
			buildDisplayList,
			Full(this.toInt),
			(v: Int) => {
				this.set(this.fromInt(v))
				onChange(this.fromInt(v))
			},
			attrs.map(a => a: ElemAttr) : _*
		)
	}
}
