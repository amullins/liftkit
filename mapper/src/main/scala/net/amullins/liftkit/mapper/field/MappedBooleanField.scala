package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.util._

import scala.xml._


abstract class MappedBooleanField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedBoolean(fieldOwner)
	with Validatable[Boolean, FieldOwner]
	with HtmlHelpers
{

	def toCheckbox: NodeSeq = toCheckbox("class" -> "checkbox")
	def toCheckbox(attr: (String, String)*): NodeSeq = toCheckbox(b => {}, attr)
	def toCheckbox(onSubmit: Boolean => Any, attr: Seq[(String, String)] = Seq.empty): NodeSeq = {
		var cssClass = getMessageClass.openOr("")
		val attrs = attr.flatMap {
			case ("class", clazz) =>
				cssClass += (" " + clazz.trim)
				Empty
			case t => Full(pairToBasic(t))
		} :+ pairToBasic("class" -> cssClass.trim)

		SHtml.checkbox(this.get, v => {
			this.apply(v)
			onSubmit(v)
		}, attrs : _*)
	}

}
