package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.mapper._
import net.liftweb.http.js.JsCmd

import scala.xml._


class MappedPostalCodeField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, country: MappedCountry[FieldOwner])
	extends MappedPostalCode(fieldOwner, country)
	with Validatable[String, FieldOwner]
	with HtmlHelpers
{

	// to html input element
  def toInput: Elem = toInput((str: String) => {})
	def toInput(onSubmit: String => Any, attr: (String, String)*): Elem = {
		val e = SHtml.text(this.get, str => {
			this.set(str)
			onSubmit(str)
		}, attr : _*)
		if (this.required_?) addCssClass(getMessageClass.openOr("") + " text ", e)
		else addCssClass("text", e)
	}


	def toAjaxInput(onSubmit: String => JsCmd, attr: (String, String)*): Elem = {
		val e = SHtml.ajaxText(this.get, str => {
			this.setFromAny(str)
			onSubmit(str)
		}, attr : _*)
		if (this.required_?) addCssClass(getMessageClass.openOr("") + " text ", e)
		else addCssClass("text", e)
	}

	override def validations =
		valMaxLen(maxLen, S ? """%s is too long. Maximum length: %s characters.""".format(displayName, maxLen.toString)) _ ::
		super.validations

}
