package net.amullins.liftkit.mapper

import net.liftweb.mapper._
import net.amullins.liftkit.common.CustomEnum

import field.MappedCustomEnumArrayField


trait ArrayQueryParams {

  def ArrayContains[M <: Mapper[M], V <: Enum#Value, Enum <: CustomEnum[V]](field: MappedCustomEnumArrayField[M, V, Enum], values: V*): BySql[M] =
    BySql[M](
      "%s @> ARRAY[%s]".format(field.dbSelectString, values.map(_.id).mkString(",")),
      IHaveValidatedThisSQL("Andrew Mullins", "051314")
    )


  def ArrayDoesNotContain[M <: Mapper[M], V <: Enum#Value, Enum <: CustomEnum[V]](field: MappedCustomEnumArrayField[M, V, Enum], values: V*): BySql[M] =
    BySql[M](
      "NOT (%s @> ARRAY[%s])".format(field.dbSelectString, values.map(_.id).mkString(",")),
      IHaveValidatedThisSQL("Andrew Mullins", "051314")
    )


  def ArrayContainsAny[M <: Mapper[M], V <: Enum#Value, Enum <: CustomEnum[V]](field: MappedCustomEnumArrayField[M, V, Enum], values: V*): BySql[M] =
    BySql[M](
      "%s && ARRAY[%s]".format(field.dbSelectString, values.map(_.id).mkString(",")),
      IHaveValidatedThisSQL("Andrew Mullins", "051314")
    )

}
