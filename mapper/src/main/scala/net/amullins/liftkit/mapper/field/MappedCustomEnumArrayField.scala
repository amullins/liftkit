package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers.tryo
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST._

import java.lang.reflect.Method
import java.util.Date

import scala.xml._
import scala.reflect.runtime.universe.TypeTag
import scala.collection.JavaConverters._

import net.amullins.liftkit.common.CustomEnum


/**
 * Store an array of enum values in a single column.
 * * tested only with PostgreSQL
 */
abstract class MappedCustomEnumArrayField[T <: Mapper[T], EnumType <: CustomEnum[EnumType]#Value, ENUM <: CustomEnum[EnumType]](
	val fieldOwner: T,
	val enum: ENUM
)(
	implicit val manifest: TypeTag[Seq[EnumType]]
)
	extends MappedField[Seq[EnumType], T]
  with EnumList[T, EnumType]
	with Validatable[Seq[EnumType], T]
	with HtmlHelpers
{

	private var data: Seq[EnumType] = defaultValue
	private var orgData: Seq[EnumType] = defaultValue
	def defaultValue: Seq[EnumType] = Nil
	def dbFieldClass = manifest.tpe.erasure.asInstanceOf[Class[Seq[EnumType]]]

  def targetSQLType = java.sql.Types.ARRAY

	def sourceInfoMetadata(): SourceFieldMetadata{type ST = Seq[EnumType]} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
	    type T = Seq[EnumType]
	    def asString(v: T): String = v.map(_.name).mkString(", ")
	    def asNodeSeq(v: T): Box[NodeSeq] = Full(Text(asString(v)))
	    def asJson(v: T): Box[JValue] = Full(JArray(v.toList.map(x => JInt(x.id))))
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

  protected def i_is_! = data
  protected def i_was_! = orgData

  override def doneWithSave() {
    orgData = data
  }

  protected def real_i_set_!(value: Seq[EnumType]): Seq[EnumType] = {
    if (value != data) {
      data = value
      dirty_?(true)
    }
    data
  }
  override def readPermission_? = true
  override def writePermission_? = true

  def asJsExp: JsExp = JE.JsArray(this.get.map(v => JE.Num(v.id)) :_*)

  def asJsonValue: Box[JValue] = Full(JArray(this.get.map(v => JInt(v.id)).toList))

  private def asJavaArray(value: Seq[EnumType]) = seqAsJavaListConverter(value).asJava.toArray

  def real_convertToJDBCFriendly(value: Seq[EnumType]): Object =
    net.liftweb.db.DB.currentConnection.map { conn =>
      conn.connection.createArrayOf("int4", seqAsJavaListConverter(value.map(_.id)).asJava.toArray)
    } openOr null

  def jdbcFriendly(field: String) = real_convertToJDBCFriendly(this.get)

  override def setFromAny(in: Any): Seq[EnumType] = {
    def setFromInt(i: Int): Seq[EnumType] = tryo { this.set(Seq(enum.apply(i))) } openOr Seq.empty

    in match {
      case JInt(bi) => setFromInt(bi.intValue())
      case n: Long => setFromInt(n.toInt)
      case n: Number => setFromInt(n.intValue())
      case (n: Number) :: _ => setFromInt(n.intValue())
      case Some(n: Number) => setFromInt(n.intValue())
      case None => this.set(Nil)
      case (s: String) :: _ => setFromInt(Helpers.toInt(s))
      case vs: List[EnumType] => this.set(vs)
      case null => this.set(Nil)
      case s: String => setFromInt(Helpers.toInt(s))
      case o => setFromInt(Helpers.toInt(o))
    }
  }

	def +=(in: EnumType) = {
		this.set(this.i_is_! :+ in)
	}

	def ++=(in: Seq[EnumType]) = {
		this.set(this.i_is_! ++ in)
	}

  protected def i_obscure_!(in : Seq[EnumType]) = Nil

  private def st(in: Seq[EnumType]) {
    data = in
    orgData = in
  }

  def buildSetActualValue(accessor: Method, data: AnyRef, columnName: String): (T, AnyRef) => Unit =
	  (inst, v) => doField(inst, accessor, { case f: MappedCustomEnumArrayField[T, EnumType, ENUM] =>
      v match {
        case null => f.st(Seq.empty[EnumType])
        case jArr: java.sql.Array =>
          val intSeq = jArr.getArray.asInstanceOf[Array[java.lang.Integer]].toSeq
          val valsFromEnum = intSeq.flatMap(i => tryo(enum.apply(i)))
          f.st(valsFromEnum)
      }
	  })

  def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumArrayField[T, EnumType, ENUM] =>
		    f.st(defaultValue)
	  })

  def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumArrayField[T, EnumType, ENUM] =>
		    f.st(defaultValue)
	  })

  def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumArrayField[T, EnumType, ENUM] =>
		    f.st(defaultValue)
	  })

  def buildSetBooleanValue(accessor: Method, columnName : String): (T, Boolean, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumArrayField[T, EnumType, ENUM] =>
		    f.st(defaultValue)
	  })

  def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.enumListColumnType + notNullAppender()

	override def get_? : Box[Seq[EnumType]] = this.get match {
		case Nil => Empty
		case _ => Full(this.get)
	}

	def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

  def getValue(i: Int) = enum.apply(i)
  val allValues = enum.all
  override def validValues = enum.validValues

  override def validations = isValid_? _ :: super.validations
  override def setFilter = distinct _ :: super.setFilter
}