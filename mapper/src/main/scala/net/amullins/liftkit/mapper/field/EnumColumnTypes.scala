package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.http.SHtml._
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.mapper._
import net.liftweb.http.js._

import scala.xml._
import scala.reflect.runtime.universe.TypeTag


object EnumColumnTypes {

	abstract class MappedEnumField[FieldOwner <: Mapper[FieldOwner], ENUM <: Enumeration](
		fieldOwner: FieldOwner,
		enum: ENUM
	)(
		override implicit val manifest: TypeTag[ENUM#Value]
	)
		extends MappedEnum[FieldOwner, ENUM](fieldOwner, enum)
		with Validatable[ENUM#Value, FieldOwner]
		with HtmlHelpers
	{
		def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

		override def buildDisplayList: List[(Int, String)] =
			enum.values.toList.map(v => (v.id, v.toString)).sortWith(_._1 < _._1)

		override def get_? : Box[ENUM#Value] = {
			if (this.get.id == 0) Empty
			else Full(this.get)
		}

		// "Please Select" has an id of 0 and is an INVALID selection
		def isValid_?(v: Enumeration#Value): List[FieldError] = {
			if ( v.id == 0 ) List(FieldError(this, Text(defaultErrorMsg)))
			else Nil
		}

		override def validations = isValid_? _ :: super.validations

		// to html select element
		def toSelect: Elem = toSelect()
		def toSelect(attrs: (String, String)*): Elem = toSelect((v: Enumeration#Value) => {}, attrs:_*)
		def toSelect(onSubmit: ENUM#Value => Any, attrs: (String, String)*): Elem = {
			addCssClass(
				getMessageClass,
				SHtml.selectObj[Int](buildDisplayList, if (this.get != null) Full(toInt) else Empty, v => {
					val e = fromInt(v)
					set(e)
					onSubmit(e)
				}, attrs:_*)
			)
		}

		def toAjaxSelect(onChange: ENUM#Value => JsCmd, attrs: (String, String)*): Elem = {
			import net.liftweb.http.SHtml._
			SHtml.ajaxSelectObj[Int](buildDisplayList, Full(toInt), (v: Int) => {
				set(fromInt(v))
				onChange(fromInt(v))
			}, attrs.map(a => a: ElemAttr):_*)
		}
		def toAjaxSelect(attrs: (String, String)*): Elem = toAjaxSelect(v => JsCmds.Noop, attrs:_*)
	}


	class MappedEnumListField[FieldOwner <: Mapper[FieldOwner], ENUM <: Enumeration](
		fieldOwner: FieldOwner,
		override val enum: ENUM
	)(
    override implicit val manifest: TypeTag[Seq[ENUM#Value]]
  )
		extends MappedEnumList(fieldOwner, enum)
    with EnumList[FieldOwner, ENUM#Value]
		with Validatable[Seq[ENUM#Value], FieldOwner]
		with HtmlHelpers
	{

    def getValue(i: Int) = enum.apply(i).asInstanceOf[ENUM#Value]
    val allValues = enum.values.toSeq.map(_.asInstanceOf[ENUM#Value])

		def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

		override def validations = isValid_? _ :: super.validations
		override def setFilter = distinct _ :: super.setFilter
	}

}
