package net.amullins.liftkit.mapper

import net.liftweb.common._
import net.liftweb.mapper._

import java.util.Date
import org.joda.time.DateTime

import net.amullins.liftkit.common.date.DateRanges


trait DateQueryParams {

  def ByDate[M <: Mapper[M]](dateField: MappedDateTime[M], date: DateTime): QueryParam[M] = By(dateField, date.toDate)
  def ByDate[M <: Mapper[M]](dateField: MappedDateTime[M], date: Date): QueryParam[M] = By(dateField, date)

  def ByDate_>[M <: Mapper[M]](dateField: MappedDateTime[M], date: DateTime): QueryParam[M] = By_>(dateField, date.toDate)
  def ByDate_>[M <: Mapper[M]](dateField: MappedDateTime[M], date: Date): QueryParam[M] = By_>(dateField, date)

  def ByDate_<[M <: Mapper[M]](dateField: MappedDateTime[M], date: DateTime): QueryParam[M] = By_<(dateField, date.toDate)
  def ByDate_<[M <: Mapper[M]](dateField: MappedDateTime[M], date: Date): QueryParam[M] = By_<(dateField, date)


  object ByDateRange extends Logger {

  	def apply[M <: Mapper[M]](dateField: MappedDateTime[M], inclusive: Boolean, start: Date, end: Date): BySql[M] = {
  		val f = dateField.dbSelectString
  		val sql = {
  			if (!inclusive) f + " > ? and " + f + " < ?"
  			else f + " >= ? and " + f + " <= ?"
  		}
  		BySql[M](sql, IHaveValidatedThisSQL("Andrew Mullins", "06/01/2011"), start, end)
  	}

  	def apply[M <: Mapper[M]](dateField: MappedDateTime[M], inclusive: Boolean, start: DateTime, end: DateTime): BySql[M] =
  		this.apply(dateField, inclusive, start.toDate, end.toDate)

  	def apply[M <: Mapper[M]](dateField: MappedDateTime[M], inclusive: Boolean, range: DateRanges.DRange): BySql[M] =
  		this.apply(dateField, inclusive, range.from.toDate, range.to.toDate)

  	def overlaps[M <: Mapper[M]](
  		fromDateField: MappedDateTime[M],
  		toDateField: MappedDateTime[M],
  		range: DateRanges.DRange
  	): BySql[M] = {
  		val fromDate = range.from.toDate
  		val toDate = range.to.toDate
  		// "from" is in range OR "to" is in range or Range(from, to) spans entire range
  		BySql[M](
  			"((%s >= ? AND %s <= ?) OR (%s >= ? AND %s <= ?) OR (%s < ? AND %s > ?))".format(
  				fromDateField.dbSelectString, fromDateField.dbSelectString,
  				toDateField.dbSelectString, toDateField.dbSelectString,
  				fromDateField.dbSelectString, toDateField.dbSelectString
  			),
  			IHaveValidatedThisSQL("Andrew Mullins", "06/01/2011"),
  			fromDate, toDate, fromDate, toDate, fromDate, toDate
  		)
  	}

  }


  def ByNamedDateRange[M <: Mapper[M]](
  	dateField: MappedDateTime[M],
  	range: DateRanges.NamedDateRange,
  	inclusive: Boolean = true
  ): QueryParam[M] =
    ByDateRange[M](dateField, inclusive, range.from, range.to)

}