package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.mapper._

import scala.annotation.tailrec

import net.amullins.liftkit.common.StringHelpers._


abstract class MappedSlugField[FieldOwner <: Mapper[FieldOwner]](
	fieldOwner: FieldOwner,
	genFrom: MappedStringField[FieldOwner]
)
	extends MappedString(fieldOwner, genFrom.maxLen)
	with Validatable[String, FieldOwner]
	with HtmlHelpers
{
	override def dbIndexed_? = true

	/**
	 * Find an existing instance that matches the slug we're trying to save.
	 * @param s The slug we *want* to assign.
	 * @return
	 */
	def findExisting(s: String): Box[FieldOwner]

	/**
	 * Reset this slug using the current value from the `genFrom` field
	 * @return
	 */
	def reset() = {
		this.set(genSlug)
		fieldOwner
	}

	private def genSlug: String = {
		@tailrec def gen(i: Int, str: String): String = {
			val s =
				if (i > 0) str.sluggify + "(" + i.toString + ")"
				else str.sluggify

			findExisting(s) match {
				case Full(existing) => gen(i + 1, str)
				case _ => s
			}
		}

		genFrom.get_? match {
			case Full(gName) => gen(0, gName)
			case _ => ""
		}
	}

}
