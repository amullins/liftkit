package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.mapper._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST


abstract class MappedForeignField[T <: Mapper[T], R, RelationType](_owner: T, _relationOn: MappedField[RelationType, T])
	extends MappedLong[T](_owner)
	with Validatable[Long, T]
	with HtmlHelpers
	with Foreigner
{
	def defined_? = i_is_! > 0L

	def relation: PartialFunction[RelationType, ForeignRelation[_]]
	def obj =
		if (relation.isDefinedAt(_relationOn.get)) relation.apply(_relationOn.get).retrieve(i_is_!)
		else Empty
	def related: Box[R] = obj.flatMap((m: LongKeyedMapper[_]) => tryo(m.asInstanceOf[R]))

	override def jdbcFriendly(field : String) = if (defined_?) new java.lang.Long(i_is_!) else null
	override def jdbcFriendly = if (defined_?) new java.lang.Long(i_is_!) else null

	override def dbIndexed_? = true

	override def asJsExp: JsExp = if (defined_?) super.asJsExp else JE.JsNull

	override def asJsonValue: Box[JsonAST.JValue] = if (defined_?) super.asJsonValue else Full(JsonAST.JNull)

	override def setFromAny(in: Any): Long = {
		in match {
		  case JsonAST.JNull => this.set(0L)
		  case JsonAST.JInt(bigint) => this.set(bigint.longValue())
			case r: ForeignRelation[_] => this.set(r.fKey.get)
		  case o => super.setFromAny(o)
		}
	}

	def apply(r: ForeignRelation[_]) = {
		this.set(r.fKey.get)
		fieldOwner
	}

	override def toString() = if (defined_?) super.toString() else "NULL"
}


trait ForeignRelation[T <: LongKeyedMapper[T]] {
	self: LongKeyedMapper[T] =>

	private[field] def fKey: MappedField[Long, T] = primaryKeyField
	private[field] def retrieve(objId: Long): Box[LongKeyedMapper[T]] = getSingleton.find(By(fKey, objId))
}


trait Foreigner extends LifecycleCallbacks {
  self: { def obj: Box[KeyedMapper[_, _]] } =>
	def retrieve: Box[KeyedMapper[_, _]] = obj
}