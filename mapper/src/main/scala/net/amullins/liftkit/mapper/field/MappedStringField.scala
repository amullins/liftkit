package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.mapper._
import net.liftweb.http.js.JsCmd

import scala.xml._

import net.amullins.liftkit.common.StringHelpers._


class MappedStringField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, maxLen: Int)
	extends MappedString(fieldOwner, maxLen)
	with Validatable[String, FieldOwner]
	with HtmlHelpers
{

	// to html input element
	def toInput: Elem = toInput("class" -> "text")
	def toInput(attr: (String, String)*): Elem = toInput((str: String) => {}, attr : _*)
	def toInput(onSubmit: String => Any, attr: (String, String)*): Elem = {
		val e = SHtml.text(this.get, str => {
			str.box match {
				case Full(_) => this.apply(str)
				case _ if null == this.defaultValue => this.apply(null)
				case _ => this.apply(str)
			}
			onSubmit(str)
		}, attr : _*)
		if (this.required_? && this.validate.nonEmpty) addCssClass(getMessageClass, e)
		else e
	}

	def toAjaxInput(onSubmit: String => JsCmd, attr: (String, String)*): Elem = {
		val e = SHtml.ajaxText(this.get, str => {
			str.box match {
				case Full(_) => this.apply(str)
				case _ if null == this.defaultValue => this.apply(null)
				case _ => this.apply(str)
			}
			onSubmit(str)
		}, attr : _*)
		if (this.required_? && this.validate.nonEmpty) addCssClass(getMessageClass, e)
		else e
	}


	override def validations =
    valMaxLen(maxLen, "%s is too long. Maximum length: %s".format(displayName, maxLen.toString)) _ ::
      super.validations
}
