package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.mapper._

import scala.xml._

import net.amullins.liftkit.common.HtmlHelpers


class MappedLongField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
  extends MappedLong(fieldOwner)
  with Validatable[Long, FieldOwner]
  with HtmlHelpers
{

	def valMin(msg: String)(v: Long): List[FieldError] = {
		if ( v<=0 ) List(FieldError(this, Text(msg)))
		else Nil
	}

  def toInput: NodeSeq = this.toInput("class" -> "text")
  def toInput(attrs: (String, String)*): NodeSeq = {
  	val _attrs = {
  		attrs.filterNot(_._1 == "class") ++
  		attrs.find(_._1 == "class").map(attr =>
  			("class", (attr._2.trim() + " digits " + getMessageClass.openOr("")).trim())
  		)
  	}

  	val e = HtmlHelpers.form.long(this.get_?, set _, _attrs : _*)

  	e :+ HtmlHelpers.form.numberMaskScript(e)
  }

}
