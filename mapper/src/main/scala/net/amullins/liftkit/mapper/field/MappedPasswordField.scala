package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._, S._, SHtml.ElemAttr
import net.liftweb.mapper._

import scala.xml._


class MappedPasswordField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedPassword(fieldOwner)
	with Validatable[String, FieldOwner]
	with HtmlHelpers
{

	override def validate : List[FieldError] = {
		val tooShort = S ? "password.too.short"
		super.validate match {
			case FieldError(f, Text(`tooShort`)) :: Nil => List(FieldError(f, Text(S ? "password.too.short")))
			case ls => ls
		}
	}

	private def pwInput(funcName: String, n: String, placeholder: String = ""): Elem =
		addCssClass(
			getMessageClass.openOr(""),
			<input id={funcName + n} type="password" name={funcName} class="textbox password" placeholder={placeholder}/>
		)

	/*
	 * Outputs 2 inputs (for entering a new password that must be substantiated by a second entry)
	 */
	def toInput: NodeSeq =
		S.fmapFunc(AFuncHolder.listStrToAF((s: List[String]) => this.setFromAny(s))){funcName =>
		  <div id="password1_wrap"><label for={funcName + "1"}>{S ? "password"}</label>{pwInput(funcName, "1", S ? "password")}</div>
		  <div id="password2_wrap"><label for={funcName + "2"}>{S ? "repeat.password"}</label>{pwInput(funcName, "2", S ? "repeat.password")}</div>
	  }

	def toInputs =
		S.fmapFunc(AFuncHolder.listStrToAF((s: List[String]) => this.setFromAny(s))){funcName =>
			(pwInput(funcName, "1"), pwInput(funcName, "2"))
		}

	// to html password element
	def toLogin(v: String, f: String => Any, attrs: (String, String)*): Elem =
		addCssClass(getMessageClass.openOr(""), SHtml.password(v, f, attrs : _*))

	// validation for login
	def validateLogin(v: String): List[FieldError] =
		if ( v.isEmpty ) List(FieldError(this, Text(S ? "password.required")))
		else Nil

}
