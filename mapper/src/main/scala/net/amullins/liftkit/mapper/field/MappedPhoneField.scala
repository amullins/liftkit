package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.mapper._

import java.util.Locale

import scala.xml._

import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat

import net.amullins.liftkit.common.PhoneHelpers
import net.amullins.liftkit.common.StringHelpers._


class MappedPhoneField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, val countryCode: String = Locale.getDefault.getCountry)
	extends MappedStringField(fieldOwner, 25)
{

	def defaultErrorMsg: String = S ? "invalid.phone"
	def phoneNumberFormat = PhoneNumberFormat.NATIONAL

	override def setFromAny(in: Any): String = {
		in match {
      case ph: PhoneHelpers.ConvertableToPhone => this.set(ph.toPhoneNumber.number)
			case Full(ph: PhoneHelpers.ConvertableToPhone) => this.set(ph.number)
			case _ => super.setFromAny(in)
		}
	}

	def toE164 = PhoneHelpers.toE164(this.get, countryCode.toString)

	// checks number validity
	private def isValid_?(v: String): List[FieldError] = {
		if (!v.null_?) {
			if (PhoneHelpers.isValid_?(v.trim, countryCode)) Nil
			else List(FieldError(this, Text(defaultErrorMsg)))
		} else if (required_?) {
			List(FieldError(this, Text("%s is required.".format(
				if (this.displayName.isEmpty) "Phone Number" else this.displayName
			))))
		} else {
			Nil
		}
	}

	private def format(f: PhoneNumberFormat)(in: String): String = {
    PhoneHelpers.normalize(in, countryCode.toString) match {
			case Full(n) => tryo(PhoneHelpers.phoneNumberUtil.format(n, f)) openOr in
			case _ => in
		}
	}

	override def validations = valMaxLen(maxLen, defaultErrorMsg) _ :: isValid_? _ :: super.validations

	override def setFilter = format(PhoneNumberFormat.NATIONAL) _ :: super.setFilter

	def toInput(mask_? : Boolean, attr: (String, String)*): NodeSeq =
		if (!mask_?) super.toInput(attr : _*)
		else super.toInput(attr :+ "data-inputmask" -> "'mask' : '(999) 999-9999'" : _*)

}
