package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.S._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.{JsCmd, JsExp}
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad
import net.liftweb.http.SHtml.makeAjaxCall
import net.liftweb.json.JsonAST

import java.util.{Date, Calendar}

import org.joda.time.{DateTime, LocalTime, DateTimeZone}
import org.joda.time.base.AbstractDateTime

import scala.xml._

import net.amullins.liftkit.common.date
import net.amullins.liftkit.js.JsHelpers


class MappedDateTimeField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedDateTime(fieldOwner)
	with Validatable[Date, FieldOwner]
{

	// After parsing the date string, converts resulting Date to UTC
	override def parse(s: String): Box[Date] =
		LiftRules.dateTimeConverter().parseDateTime(s).map(d =>
			new DateTime(d.getTime, DateTimeZone.UTC).toDate
		)

	implicit def anyToJsExp(in: Any): JsExp = JsHelpers.anyToJsExp(in)

	// convert supplied value to a UTC date and set this field's value
	override def setFromAny(f: Any): Date = f match {
	  case JsonAST.JNull => this.set(null)
	  case JsonAST.JInt(v) => this.set(new DateTime(v.longValue(), DateTimeZone.UTC).toDate)
	  case n: Number => this.set(new DateTime(n.longValue(), DateTimeZone.UTC).toDate)
	  case "" | null => this.set(null)
	  case s: String => parse(s).map(d => this.set(d)).openOr(this.get)
	  case (s: String) :: _ => parse(s).map(d => this.set(d)).openOr(this.get)
	  case d: Date => this.set(d)
	  case Some(d: Date) => this.set(d)
	  case Full(d: Date) => this.set(d)
	  case None | Empty | Failure(_, _, _) => this.set(null)
		case Some(dt: AbstractDateTime) => this.set(dt.toDateTime(DateTimeZone.UTC).toDate)
		case Full(dt: AbstractDateTime) => this.set(dt.toDateTime(DateTimeZone.UTC).toDate)
		case dt: AbstractDateTime => this.set(dt.toDateTime(DateTimeZone.UTC).toDate)
	  case _ => this.get
	}

	def apply[DT <: AbstractDateTime](dateTime: DT): FieldOwner = {
		this.setFromAny(dateTime)
		fieldOwner
	}

	def toCalendar: Calendar = {
		val c = Calendar.getInstance()
		c.setTime(this.get)
		c
	}

	@throws(classOf[NullPointerException])
	def toDateTime(tz: DateTimeZone = DateTimeZone.UTC): DateTime = {
		new DateTime(this.get.getTime).withZone(tz)
	}

	def asDateTime(tz: DateTimeZone = DateTimeZone.UTC): Box[DateTime] = {
		this.get_?.map(d => new DateTime(d.getTime).withZone(tz))
	}

	@throws(classOf[NullPointerException])
	def toUserDateTime: DateTime = toDateTime(date.Helpers.joda.currentTimeZone)

	def asUserDateTime: Box[DateTime] = asDateTime(date.Helpers.joda.currentTimeZone)

	val datepickerOptions: Map[String, JsExp] = Map(
		"showOn" -> Str("both"),
    "buttonImage" -> Str("/images/calendar-icon.png"),
    "buttonImageOnly" -> JsTrue,
    "duration" -> Str("fast"),
    "showAnim" -> Str("fadeIn")
	)

	private def _getDateTime = asDateTime() openOr new DateTime()

	private def _getScript(e: Elem, opts: Map[String, Any]) = {
		val options = datepickerOptions ++ opts.map(kv => (kv._1, kv._2: JsExp))
		Script(JqOnLoad(JsRaw("""$("input[name=%s]").datepicker(%s)""".format(
			(e \\ "@name").text,
			JsObj(options.toSeq:_*)
		)).cmd))
	}

	def toInput(
		displayTimeZone: Any
	)(
		withDatepicker: Boolean,
		onSubmit: DateTime => Unit,
		value: DateTime = _getDateTime,
		opts: Map[String, Any] = Map.empty
	): NodeSeq = {
		val inputTimeZone = displayTimeZone match {
			case Full(dtz: DateTimeZone) => dtz
			case Full(tz: java.util.TimeZone) => DateTimeZone.forTimeZone(tz)
			case dtz: DateTimeZone => dtz
			case tz: java.util.TimeZone => DateTimeZone.forTimeZone(tz)
			case str: String => DateTimeZone.forID(str)
			case _ => DateTimeZone.getDefault
		}

		val e: Elem = addCssClass(
			getMessageClass.openOr("") + " text date",
			SHtml.text(
        date.Helpers.joda.fullDate.print(value.withZone(inputTimeZone)),
				(dateString: String) => {
          date.Helpers.joda.parseDate(dateString) match {
						case Full(dt) => onSubmit(dt)
						case _ => S.error(List(FieldError(this, "The date you entered is invalid. Please enter date as MM/DD/YYYY.")))
					}
				}
			)
		)

		if (withDatepicker)	e % ("id" -> (e \\ "@name").text) :+ _getScript(e, opts)
		else e % ("id" -> (e \\ "@name").text)
	}

	def toAjaxInput(
		withDatepicker: Boolean,
		onSubmit: DateTime => JsCmd,
		value: DateTime = _getDateTime,
		opts: Map[String, Any] = Map.empty,
		id: Box[String] = Empty
	): NodeSeq = {
		fmapFunc(contextFuncBuilder(SFuncHolder(v => {
      date.Helpers.joda.parseDate(v) match {
				case Full(d) => onSubmit(d)
				case _ =>
					S.error(List(FieldError(this, "The date you entered is invalid. Please enter date as MM/DD/YYYY.")))
					Noop
			}
		}))) {
			funcName => {
				val doAjax = makeAjaxCall(JsRaw("""'%s=' + encodeURIComponent(this.value)""".format(funcName)))
				val i = {
					<input type="text" name={funcName} value={date.Helpers.joda.fullDate.print(value)} class={getMessageClass.openOr("") + " text date"} /> %
					("onkeypress" -> "liftUtils.lift_blurIfReturn(event)".toJsCmd) %
					("onblur" -> doAjax.toJsCmd) %
					id.map(i => new UnprefixedAttribute("id", i, Null)).openOr(Null)
				}
				Group(
					if (withDatepicker)	i :+ _getScript(i, opts ++ Map("onSelect" -> AnonFunc(doAjax)))
					else i
				)
			}
		}
	}

	protected def before(f: MappedDateTimeField[FieldOwner], msg: String)(d2: Date): List[FieldError] = {
		if (d2 == null) List(FieldError(this, msg))
		else if (f.get_?.isDefined) {
			if (d2.before(f.get)) Nil
			else List(FieldError(this, msg))
		} else {
			Nil // cannot validate - f is null
		}
	}

	// must be before supplied date
	protected def before(d1: Date, msg: String)(d2: Date): List[FieldError] = {
		if (d2 == null) List(FieldError(this, msg))
		else if (d2.before(d1)) Nil
		else List(FieldError(this, msg))
	}

	// must be after a date from another field
	protected def after(f: MappedDateTimeField[FieldOwner], msg: String)(d2: Date): List[FieldError] = {
		if (d2 == null) List(FieldError(this, msg))
		else if (f.get_?.isDefined) {
			if (d2.after(f.get)) Nil
			else List(FieldError(this, msg))
		} else {
			Nil // cannot validate - f is null
		}
	}

	// must be after supplied date
	protected def after(d1: Date, msg: String)(d2: Date): List[FieldError] = {
		if (d2 == null) List(FieldError(this, msg))
		else if (d2.after(d1)) Nil
		else List(FieldError(this, msg))
	}

	// must be in the future
	protected def future(msg: String)(d: Date): List[FieldError] = {
	  if (d.after(now)) Nil
	  else List(FieldError(this, msg))
	}
}


class MappedTimeField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedTime(fieldOwner)
	with Validatable[Date, FieldOwner]
{

	def toLocalTime: LocalTime = new LocalTime(this.get.getTime)

}


class MappedTimeZoneField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedTimeZone(fieldOwner)
	with Validatable[String, FieldOwner]
{

	def asDateTimeZone =
    tryo(DateTimeZone.forID(this.get)) openOr
      DateTimeZone.forID(this.defaultValue)

  def asTimeZone = isAsTimeZone

}