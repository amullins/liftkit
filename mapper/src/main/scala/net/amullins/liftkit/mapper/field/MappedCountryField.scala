package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.mapper._


class MappedCountryField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedCountry(fieldOwner)
	with Validatable[Countries.Value, FieldOwner]
{

	override def setFromAny(v: Any): Countries.Value = {
		v match {
			case "United States" => Countries.USA
			case "USA" => Countries.USA
			case "US" => Countries.USA
			case "Sweden" => Countries.Sweden
			case "SE" => Countries.Sweden
			case "Australia" => Countries.Australia
			case "AU" => Countries.Australia
			case "Canada" => Countries.Canada
			case "CA" => Countries.Canada
			case "Germany" => Countries.Germany
			case "DE" => Countries.Germany
			case "United Kingdom" => Countries.UK
			case "UK" => Countries.UK
			case unknownString: String => this.defaultValue
			case _ => super.setFromAny(v)
		}
	}

}
