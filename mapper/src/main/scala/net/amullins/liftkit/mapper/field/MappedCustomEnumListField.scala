package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.mapper._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST

import java.lang.reflect.Method
import java.util.Date

import scala.xml._
import scala.reflect.runtime.universe.TypeTag

import net.amullins.liftkit.common.CustomEnum


/**
 * Stores enum values in a single field as an Integer using the same method for
 * transforming the enum values as is used in Lift's own MappedEnumList.
 *
 * Using [[net.amullins.liftkit.mapper.field.MappedCustomEnumArrayField]] is preferable to [[net.amullins.liftkit.mapper.field.MappedCustomEnumListField]] since it
 * allows searching the individual enum values with the use of [[net.amullins.liftkit.mapper.ArrayQueryParams]]
 */
abstract class MappedCustomEnumListField[T <: Mapper[T], EnumType <: CustomEnum[EnumType]#Value, ENUM <: CustomEnum[EnumType]](
	val fieldOwner: T,
	val enum: ENUM
)(
	implicit val manifest: TypeTag[Seq[EnumType]]
)
	extends MappedField[Seq[EnumType], T]
  with EnumList[T, EnumType]
	with Validatable[Seq[EnumType], T]
	with HtmlHelpers
{

	private var data: Seq[EnumType] = defaultValue
	private var orgData: Seq[EnumType] = defaultValue
	def defaultValue: Seq[EnumType] = Nil
	def dbFieldClass = manifest.tpe.erasure.asInstanceOf[Class[Seq[EnumType]]]

  def targetSQLType = java.sql.Types.BIGINT

	import JsonAST._
	def sourceInfoMetadata(): SourceFieldMetadata{type ST = Seq[EnumType]} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
	    type T = Seq[EnumType]
	    def asString(v: T): String = v.map(_.name).mkString(", ")
	    def asNodeSeq(v: T): Box[NodeSeq] = Full(Text(asString(v)))
	    def asJson(v: T): Box[JValue] = Full(JArray(v.toList.map(x => JsonAST.JInt(x.id))))
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

  protected def i_is_! = data
  protected def i_was_! = orgData

  override def doneWithSave() {
    orgData = data
  }

  protected def real_i_set_!(value: Seq[EnumType]): Seq[EnumType] = {
    if (value != data) {
      data = value
      dirty_?(true)
    }
    data
  }
  override def readPermission_? = true
  override def writePermission_? = true

  def asJsExp: JsExp = JE.JsArray(this.get.map(v => JE.Num(v.id)) :_*)

  def asJsonValue: Box[JsonAST.JValue] = Full(JsonAST.JInt(toLong))

  def real_convertToJDBCFriendly(value: Seq[EnumType]): Object = new java.lang.Long(Helpers.toLong(value))

  private def rot(in: Int): Long = 1L << in

  private def toLong: Long = this.get.foldLeft(0L)((a,b) => a + rot(b.id))

  def fromLong(in: Long): Seq[EnumType] = enum.validValues.filter(v => (in & rot(v.id)) != 0)

  def jdbcFriendly(field: String) = new java.lang.Long(toLong)
  override def jdbcFriendly = new java.lang.Long(toLong)

  override def setFromAny(in: Any): Seq[EnumType] = {
    in match {
      case JsonAST.JInt(bi) => this.set(fromLong(bi.longValue()))
      case n: Long => this.set( fromLong(n))
      case n: Number => this.set(fromLong(n.longValue))
      case (n: Number) :: _ => this.set(fromLong(n.longValue))
      case Some(n: Number) => this.set(fromLong(n.longValue))
      case None => this.set(Nil)
      case (s: String) :: _ => this.set(fromLong(Helpers.toLong(s)))
      case vs: List[EnumType] => this.set(vs)
      case null => this.set(Nil)
      case s: String => this.set(fromLong(Helpers.toLong(s)))
      case o => this.set(fromLong(Helpers.toLong(o)))
    }
  }

	def +=(in: EnumType) = {
		this.set(this.i_is_! :+ in)
	}

	def ++=(in: Seq[EnumType]) = {
		this.set(this.i_is_! ++ in)
	}

  protected def i_obscure_!(in : Seq[EnumType]) = Nil

  private def st(in: Seq[EnumType]) {
    data = in
    orgData = in
  }

  def buildSetActualValue(accessor: Method, data: AnyRef, columnName: String): (T, AnyRef) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumListField[T, EnumType, ENUM] =>
		    f.st(if (v eq null) defaultValue else fromLong(Helpers.toLong(v)))
	  })

  def buildSetLongValue(accessor: Method, columnName: String): (T, Long, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumListField[T, EnumType, ENUM] =>
		    f.st(if (isNull) defaultValue else fromLong(v))
	  })

  def buildSetStringValue(accessor: Method, columnName: String): (T, String) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumListField[T, EnumType, ENUM] =>
		    f.st(if (v eq null) defaultValue else fromLong(Helpers.toLong(v)))
	  })

  def buildSetDateValue(accessor: Method, columnName: String): (T, Date) => Unit =
	  (inst, v) => doField(inst, accessor, {
      case f: MappedCustomEnumListField[T, EnumType, ENUM] =>
		    f.st(if (v eq null) defaultValue else fromLong(Helpers.toLong(v)))
	  })

  def buildSetBooleanValue(accessor: Method, columnName : String): (T, Boolean, Boolean) => Unit =
	  (inst, v, isNull) => doField(inst, accessor, {
      case f: MappedCustomEnumListField[T, EnumType, ENUM] =>
		    f.st(defaultValue)
	  })

  def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.enumListColumnType + notNullAppender()

	override def get_? : Box[Seq[EnumType]] = this.get match {
		case Nil => Empty
		case _ => Full(this.get)
	}

	def defaultErrorMsg = (S ? "please.select.partial") + " " + displayName

  def getValue(i: Int) = enum.apply(i)
  val allValues = enum.all
  override def validValues = enum.validValues

  override def validations = isValid_? _ :: super.validations
  override def setFilter = distinct _ :: super.setFilter
}