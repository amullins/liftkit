package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.http._

import scala.xml._


trait Validatable[TheFieldType <: Any, TheOwnerType <: Mapper[TheOwnerType]] {
	self: MappedField[TheFieldType, TheOwnerType] =>

	/**
   * Get the field's value in a Box. null values will be Empty
	 */
	def get_? : Box[TheFieldType] = {
		def filter(v: TheFieldType): Boolean = {
			v match {
				// for strings, we trim and check that they're NOT empty
				case str: String => str.trim().nonEmpty
				case _ => true
			}
		}
		for (v <- Option(this.get) if filter(v)) yield v
	}

	// css class to be applied to field in case of error
	def errorClass: Box[String] = Full("error")

	// css class to be applied to field in case of warning
	def warningClass: Box[String] = Full("warn")

	// css class to be applied to field in case of notice
	def noticeClass: Box[String] = Full("notice")

	def cssClassFromNoticeType(t: NoticeType.Value): Box[String] = t match {
		case NoticeType.Error => errorClass
		case NoticeType.Warning => warningClass
		case NoticeType.Notice => noticeClass
	}

  /**
   * Get the current messages for field.
   */
	def currentMessages: List[(Box[String], NodeSeq)] =
    S.getNotices.collect {
      case (noticeType, ns, fieldId)
        if fieldId == uniqueFieldId => (cssClassFromNoticeType(noticeType), ns)
    }

  /**
   * Get the current messages of type for field.
   * @param t The type of messages to retrieve (Error|Warning|Notice)
   */
	def currentMessages(t: NoticeType.Value): List[(Box[String], NodeSeq)] =
    S.getNotices.collect {
      case (noticeType, ns, fieldId)
        if noticeType == t && fieldId == uniqueFieldId => (cssClassFromNoticeType(t), ns)
    }

	// errors for field?
	def hasError_? = currentMessages(NoticeType.Error).nonEmpty

	// warnings for field?
	def hasWarning_? = currentMessages(NoticeType.Warning).nonEmpty

	// notices for field?
	def hasNotice_? = currentMessages(NoticeType.Notice).nonEmpty

	private def appendClass(in: Box[String], c: Box[String]): String = in.openOr("").trim + " " + c.openOr("")

	// returns error class if hasError_?
	def getMessageClass: Box[String] = {
		var messageClass: Box[String] = Empty

		if (hasError_?) messageClass = Full(appendClass(messageClass, errorClass))
		if (hasWarning_?) messageClass = Full(appendClass(messageClass, warningClass))
		if (hasNotice_?) messageClass = Full(appendClass(messageClass, noticeClass))

		messageClass
	}
}

/**
 * Use this trait to have the foreign object deleted along with the foreign key field's owner.
 */
trait Pwned { self: Foreigner =>
	override def afterDelete {
		this.retrieve.foreach(_.delete_!)
	}
}
