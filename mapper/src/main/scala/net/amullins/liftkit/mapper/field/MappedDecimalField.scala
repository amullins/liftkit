package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.mapper._
import net.liftweb.http.js._
import net.liftweb.json.JsonAST

import java.lang.reflect.Method
import java.math.MathContext

import scala.xml._
import scala.reflect.runtime.universe.TypeTag

import net.amullins.liftkit.common.HtmlHelpers


class MappedDecimalField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, mathContext: MathContext = MathContext.DECIMAL64, s: Int = 2)
	extends MappedDecimal(fieldOwner, mathContext, s)
	with Validatable[BigDecimal, FieldOwner]
	with HtmlHelpers
{

	/*
	 * This was a workaround to bug #1116
	 */
	def setIt(value: BigDecimal): BigDecimal = {
		if (!this.i_is_!.equals(value)) {
			isDirty_?(true)
			this.real_i_set_!(value)
		}
		else this.i_is_!
	}
	private var isDirty_? = false
	private def isDirty_?(d: Boolean): Unit = isDirty_? = d
	//override def dirty_? = !dbPrimaryKey_? && isDirty_?

	def inputMask: String = "n"

	def toInput: NodeSeq = this.toInput("class" -> "text")
	def toInput(attrs: (String, String)*): NodeSeq = {
		val _attrs = {
			attrs.filterNot(_._1 == "class") ++
			attrs.find(_._1 == "class").map(attr =>
				("class", (attr._2.trim() + " digits " + getMessageClass.openOr("")).trim())
			)
		}

		val e = {
      HtmlHelpers.form.decimal(this.get_?, setFromAny _, mathContext, _attrs:_*)
		}

		e :+ HtmlHelpers.form.numberMaskScript(e, inputMask)
	}

}


abstract class MappedNullableDecimalField[FieldOwner <: Mapper[FieldOwner]](
	val fieldOwner: FieldOwner,
	val context : MathContext = MathContext.DECIMAL64,
	val scale : Int = 2
)(
	implicit val manifest: TypeTag[Box[BigDecimal]]
)
	extends MappedNullableField[BigDecimal, FieldOwner]
	with Validatable[Box[BigDecimal], FieldOwner]
	with HtmlHelpers
{
  private var data: Box[BigDecimal] = defaultValue
  private var orgData: Box[BigDecimal] = defaultValue

  def defaultValue: Box[BigDecimal] = Empty
  def dbFieldClass = classOf[Box[BigDecimal]]

  def targetSQLType = java.sql.Types.DECIMAL

	import JsonAST._
	def sourceInfoMetadata(): SourceFieldMetadata{type ST = Box[BigDecimal]} =
	  SourceFieldMetadataRep(name, manifest, new FieldConverter {
	    type T = Box[BigDecimal]
	    def asString(v: T): String = v.map(_.toString()) openOr ""
	    def asNodeSeq(v: T): Box[NodeSeq] = v.map(d => Text(d.toString()))
	    def asJson(v: T): Box[JValue] = v.map(d => JDouble(d.toDouble))
	    def asSeq(v: T): Box[Seq[SourceFieldInfo]] = Empty
	  })

  protected def i_is_! = data
  protected def i_was_! = orgData

  override def doneWithSave() {
    orgData = data
  }

  protected def real_i_set_!(value: Box[BigDecimal]): Box[BigDecimal] = {
    if (value != data) {
      data = value
      dirty_?(true)
    }
    data
  }

  def asJsExp: JsExp = this.get.map(v => JE.Num(v)) openOr JE.JsNull

  def asJsonValue: Box[JsonAST.JValue] = Full(this.get.map(v => JsonAST.JDouble(v.doubleValue())) openOr JsonAST.JNull)

  override def readPermission_? = true
  override def writePermission_? = true

  def real_convertToJDBCFriendly(value: Box[BigDecimal]): Object = value match {
    case Full(value) => value.bigDecimal
    case _ => null
  }

  def jdbcFriendly(field : String) = real_convertToJDBCFriendly(i_is_!)
  override def jdbcFriendly = real_convertToJDBCFriendly(i_is_!)

  override def setFromAny(in: Any): Box[BigDecimal] = {
	  in match {
		  case bd : BigDecimal => setAll(bd)
		  case n :: _ => setFromString(n.toString)
		  case Some(n) => setFromString(n.toString)
		  case Full(n) => setFromString(n.toString)
		  case None | Empty | Failure(_, _, _) | null => setFromString("0")
		  case n => setFromString(n.toString)
	  }
  }

	def setFromString (in : String): Box[BigDecimal] = {
	  this.setAll(BigDecimal(in))
	  data
	}

	protected def setAll(in : BigDecimal) = this.set(coerce(in))

	protected def coerce(in : BigDecimal) = tryo(new BigDecimal(in.bigDecimal.setScale(scale, context.getRoundingMode)))

  protected def i_obscure_!(in: Box[BigDecimal]) = defaultValue

  private def st(in: Box[BigDecimal]) {
    data = in
    orgData = in
  }


	def buildSetBooleanValue(accessor : Method, columnName : String) : (FieldOwner, Boolean, Boolean) => Unit = null

	def buildSetDateValue(accessor : Method, columnName : String): (FieldOwner, java.util.Date) => Unit =
    (inst, v) => doField(inst, accessor, {
			case f: MappedNullableDecimalField[FieldOwner] =>
        f.st(if (v == null) defaultValue else coerce(BigDecimal(v.getTime)))
		})

	def buildSetStringValue(accessor: Method, columnName: String): (FieldOwner, String) => Unit =
    (inst, v) => doField(inst, accessor, {
			case f: MappedNullableDecimalField[FieldOwner] =>
        f.st(if (v == null) defaultValue else coerce(BigDecimal(v)))
		})

	def buildSetLongValue(accessor: Method, columnName : String) : (FieldOwner, Long, Boolean) => Unit =
    (inst, v, isNull) => doField(inst, accessor, {
			case f: MappedNullableDecimalField[FieldOwner] =>
        f.st(if (isNull) defaultValue else coerce(BigDecimal(v)))
		})

	def buildSetActualValue(accessor: Method, data: AnyRef, columnName: String) : (FieldOwner, AnyRef) => Unit =
    (inst, v) => doField(inst, accessor, {
			case f: MappedNullableDecimalField[FieldOwner] =>
        f.st(if (v == null) defaultValue else coerce(BigDecimal(v.toString)))
		})


  def fieldCreatorString(dbType: DriverType, colName: String): String =
    colName + " " + dbType.longColumnType + notNullAppender()
}