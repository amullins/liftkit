package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.mapper._
import net.liftweb.http.SHtml.ElemAttr
import net.liftweb.http.SHtml.ElemAttr._

import java.net.URI

import org.apache.commons.validator.routines.UrlValidator, UrlValidator._

import scala.xml._

import net.amullins.liftkit.common.UrlHelpers.{uriToSuperURI, asURI => strAsURI}
import net.amullins.liftkit.common.StringHelpers._


class MappedUrlField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner)
	extends MappedTextField(fieldOwner)
{

	def acceptedProtocols: List[String] = List("http", "https")
	def defaultProtocol: Box[String] = Full("http://")

	private def autoAppendDefaultProtocol_?(): Boolean = defaultProtocol.isDefined

	def asURI: Box[URI] = this.get

  def params: Map[String, List[String]] = asURI.map(u => uriToSuperURI(u).params) openOr Map.empty

	override def toInput: Elem = this.toInput("class" -> "text url")
	def toInput(attr: ElemAttr*): Elem = {
		val e = SHtml.text(this.get, str => {
			if (! ((str == "") && (null == this.defaultValue))) {
				setFromAny(str)
			}
		}, attr:_*)

		if (this.required_? && this.validate.nonEmpty) addCssClass(getMessageClass, e)
		else e
	}

	// checks url and formats current value, if valid
	private def validUrl_?(v: String): List[FieldError] = {
		fixUrl(v) match {
			case Full(str) => Nil
			case _ => List(FieldError(this, S ? "invalid.url"))
		}
	}

	private def fixUrl(str: String): Box[String] = {
		var addedProtocol: Box[String] = Empty

		def check(u: String): Box[String] = {
			if (new UrlValidator(acceptedProtocols.toArray, NO_FRAGMENTS).isValid(u.trim)) {
				Full(u.trim)
			} else if (!new UrlValidator(ALLOW_ALL_SCHEMES + NO_FRAGMENTS).isValid(u.trim) && autoAppendDefaultProtocol_? && addedProtocol.isEmpty) {
				addedProtocol = defaultProtocol
				check(defaultProtocol.openOr("") + u.trim)
			} else {
				Empty
			}
		}

		str.box match {
			case Full(_) => check(str)
			case _ => Empty
		}
	}

	private def fix(str: String): String = fixUrl(str).openOr(str)

	override def validations = validUrl_? _ :: super.validations
	override def setFilter = fix _ :: super.setFilter

}
