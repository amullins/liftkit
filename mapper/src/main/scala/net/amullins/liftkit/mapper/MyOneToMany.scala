package net.amullins.liftkit.mapper

import net.liftweb.common._
import net.liftweb.mapper._


/**
 * Adds a couple methods to the MappedOneToMany class for searching WITHIN the
 * @tparam K The Key type.
 * @tparam T The Mapper type.
 */
trait MyOneToMany[K, T <: KeyedMapper[K, T]] extends KeyedMapper[K, T] with OneToMany[K, T] { self: T =>

  class MappedOneToMany[OK, O <: KeyedMapper[OK, O]](meta: KeyedMetaMapper[OK, O], foreign: MappedForeignKey[K,O,T], qp: QueryParam[O]*)
    extends MappedOneToManyBase[O](
      ()=>{
        val ret = meta.findAll(By(foreign, primaryKeyField.get) :: qp.toList : _*)
        for(child <- ret) {
          foreign.actualField(child).asInstanceOf[MappedForeignKey[K,O,T]].primeObj(Full(MyOneToMany.this: T))
        }
        ret
      },
      foreign
    )
  {

    def findAll(qp: QueryParam[O]*) = meta.findAll(qp :+ By(foreign, primaryKeyField.get) : _*)

    def find(qp: QueryParam[O]*) = meta.find(qp :+ By(foreign, primaryKeyField.get) : _*)
    //def find(key: Long): Box[O] = find(By(meta.primaryKeyField, key))

    def count(qp: QueryParam[O]*) = meta.count(By(foreign, primaryKeyField.get) +: qp : _*)

  }

}
