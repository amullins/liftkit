package net.amullins.liftkit.mapper

import net.liftweb.mapper._

import java.sql.{PreparedStatement, Types}


trait FindableMetaMapper[M <: KeyedMapper[_, M]] {
	self: KeyedMetaMapper[_, M] =>

	def findAllByPreparedStatement(query: String, params: Seq[Any]): List[M] = {
		findAllByPreparedStatement(conn => {
			setPreparedParams(conn.connection.prepareStatement(query), params)
		})
	}

	def countByPreparedStatement(query: String, params: Seq[Any]): Long = {
		DB.use(DefaultConnectionIdentifier)(conn =>
			DB.prepareStatement(query, conn)(stmt => {
				val rs = setPreparedParams(stmt, params).executeQuery
				if (rs.next) rs.getLong(1) else 0L
			})
		)
	}

	// taken from DB1
	protected def setPreparedParams(ps : PreparedStatement, params: Seq[Any]): PreparedStatement = {
	  params.zipWithIndex.foreach {
	    case (null, idx) => ps.setNull(idx + 1, Types.VARCHAR)
	    case (i: Int, idx) => ps.setInt(idx + 1, i)
	    case (l: Long, idx) => ps.setLong(idx + 1, l)
	    case (d: Double, idx) => ps.setDouble(idx + 1, d)
	    case (f: Float, idx) => ps.setFloat(idx + 1, f)
	    // Allow the user to specify how they want the Date handled based on the input type
	    case (t: java.sql.Timestamp, idx) => ps.setTimestamp(idx + 1, t)
	    case (d: java.sql.Date, idx) => ps.setDate(idx + 1, d)
	    case (t: java.sql.Time, idx) => ps.setTime(idx + 1, t)
	    /* java.util.Date has to go last, since the java.sql date/time classes subclass it. By default we
	     * assume a Timestamp value */
	    case (d: java.util.Date, idx) => ps.setTimestamp(idx + 1, new java.sql.Timestamp(d.getTime))
	    case (b: Boolean, idx) => ps.setBoolean(idx + 1, b)
	    case (s: String, idx) => ps.setString(idx + 1, s)
	    case (bn: java.math.BigDecimal, idx) => ps.setBigDecimal(idx + 1, bn)
	    case (obj, idx) => ps.setObject(idx + 1, obj)
	  }
	  ps
	}
}
