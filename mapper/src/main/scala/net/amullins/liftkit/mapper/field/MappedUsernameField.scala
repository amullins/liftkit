package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.mapper._

import scala.xml._


class MappedUsernameField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, maxLen: Int)
	extends MappedStringField(fieldOwner, maxLen)
{

	// to html input element (only used for login form)
	def toLogin(v: String, f: String => Any): Elem =
    addCssClass(getMessageClass.openOr("") + " text ", SHtml.email(v, f))

	def validateLogin(v: String): List[FieldError] = {
		if ( MappedEmail.validEmailAddr_?(v) ) Nil
		else List(FieldError(this, Text(S ? "invalid.email.address")))
	}

	override def validations =
		valMaxLen(maxLen, S ? "email.address.too.long") _ :: valUnique(S ? "unique.email.address") _ :: validateLogin _ :: super.validations

}
