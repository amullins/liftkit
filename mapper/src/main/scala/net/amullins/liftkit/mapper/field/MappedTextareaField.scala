package net.amullins.liftkit.mapper.field

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.mapper._

import scala.xml._


class MappedTextareaField[FieldOwner <: Mapper[FieldOwner]](fieldOwner: FieldOwner, maxLen: Int)
	extends MappedTextarea(fieldOwner, maxLen)
	with Validatable[String, FieldOwner]
	with HtmlHelpers
{

	// to html input element
	def toTextarea: Elem = toTextarea()
	def toTextarea(attrs: (String, String)*): Elem =
		addCssClass(getMessageClass.openOr(""), SHtml.textarea(this.get, this.set, attrs : _*))

	def toInput: Elem = addCssClass(getMessageClass.openOr("") + " text ", SHtml.text(this.get, set))

	override def validations =
		valMaxLen(maxLen, """"%s" is too long. Maximum length: %s characters.""".format(displayName, maxLen)) _ ::
			super.validations

}
