package net.amullins.liftkit.common

import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._


object JsonHelpers extends JsonHelpers


trait JsonHelpers {

  implicit def anyToJValue(a: Any): JValue = {
  	a match {
  		case str: String => str
  		case dec: BigDecimal => dec
  		case i: BigInt => i
  		case bool: Boolean => bool
  		case dbl: Double => dbl
  		case float: Float => float
  		case i: Int => i
  		case long: Long => long
  		case obj: JObject => obj
  		case list: List[_] => list
  		case map: Map[_, _] => map
  		case opt: Option[_] => opt
  		case seq: Traversable[_] => seq
  		case sym: Symbol => sym
  		case other => other.toString
  	}
  }

}
