package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util.Helpers._

import com.google.i18n.phonenumbers._
import com.google.i18n.phonenumbers.PhoneNumberUtil._


object PhoneHelpers extends PhoneHelpers {

  object SuperPhone {
    def apply(number: String, ext: String, countryCode: String): SuperPhone =
      new SuperPhone(number, countryCode) {
        override val extension = Full(ext)
      }

    def apply(number: String, ext: Box[String], countryCode: String): SuperPhone =
      new SuperPhone(number, countryCode) {
        override val extension = ext
      }
  }
  case class SuperPhone(number: String, countryCode: String = "US") {
    val extension: Box[String] = Empty
    def hasExtension = extension.isDefined
  }


  trait ConvertableToPhone {
    def toPhoneNumber: PhoneHelpers.SuperPhone
  }


  implicit def strToPhone(str: String): Phonenumber.PhoneNumber = phoneNumberUtil.parse(str, "US")
  implicit def convToSuper(c: ConvertableToPhone): SuperPhone = c.toPhoneNumber
  implicit def bOfConvToSuper(c: Box[ConvertableToPhone]): SuperPhone = c.map(_.toPhoneNumber) openOr SuperPhone("")

}


trait PhoneHelpers {

	// clean phone number input - returns a string of digits with no symbols
	def clean(phone: String): String = phone.replaceAll("[^\\d]", "")

	val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance

	val Extractor = """^\((\d{3})\)\s(\d{3})-(\d{4})""".r

	def mask(replacement: String = "x")(number: String): Box[String] = {
		number match {
			case Extractor(_, _, suffix) =>
				val replacementSequence: String = replacement * 3
				Full("(%s) %s-%s".format(replacementSequence, replacementSequence, suffix))

			case _ => Empty
		}
	}

	// normalize phone number for country
	def normalize(phone: String, country: String = "US"): Box[Phonenumber.PhoneNumber] = {
		tryo(phoneNumberUtil.parse(phone, country))
	}

	def isValid_?(n: String, country: String = "US") : Boolean = {
		tryo(phoneNumberUtil.parse(n, country)) match {
			case Full(ph) if phoneNumberUtil.isValidNumberForRegion(ph, country) => true
			case _ => false
		}
	}

	def nationalFormat(ph: Phonenumber.PhoneNumber): String = {
		phoneNumberUtil.format(ph, PhoneNumberFormat.NATIONAL)
	}

	def nationalFormat(ph: Box[Phonenumber.PhoneNumber]): String = {
		ph match {
			case Full(num) => phoneNumberUtil.format(num, PhoneNumberFormat.NATIONAL)
			case _ => ""
		}
	}

	def nationalFormat(ph: String, country: String = "US"): String = {
		normalize(ph, country) match {
			case Full(n) => nationalFormat(n)
			case _ => ph
		}
	}

	// http://en.wikipedia.org/wiki/E.164
	def toE164(phone: String, country: String = "US"): Box[String] = {
		normalize(phone, country) match {
			case Full(p) => Full(phoneNumberUtil.format(p, PhoneNumberFormat.E164))
			case f: Failure => f
			case _ => Failure("Error Processing Phone Number")
		}
	}

	def format_*(
		num: String,
		extension: Box[String],
		frmt: PhoneNumberFormat,
		ext_? : Boolean = true,
		country: String = "US"
	): String = {
		def withExtension(ph: String) = (for (e <- extension) yield ph + " ext. " + e) openOr ph

		tryo {
			val ph = phoneNumberUtil.format(
				phoneNumberUtil.parse(num, country),
				frmt
			)
			if (ext_?) withExtension(ph) else ph
		} openOr {
			if (ext_?) withExtension(num) else num
		}
	}

  def format(phone: PhoneHelpers.SuperPhone, frmt: PhoneNumberFormat, ext_? : Boolean): String =
    format_*(phone.number, phone.extension, frmt, phone.hasExtension, phone.countryCode)


	def npa(phone: PhoneHelpers.SuperPhone): String = npa_*(PhoneHelpers.strToPhone(phone.number))
	def npa(phone: String): String = npa_*(PhoneHelpers.strToPhone(phone))
	def npa_*(phone: Phonenumber.PhoneNumber): String = {
		phoneNumberUtil.getNationalSignificantNumber(phone).substring(0, phoneNumberUtil.getLengthOfGeographicalAreaCode(phone))
	}

}
