package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.http._

import scala.io.Source


object RequestHelpers extends RequestHelpers


/**
 * Must have an implicit of type UrlHelpers in scope
 */
trait RequestHelpers {

	/**
	 * URL encoded request parameters
	 * @param _req The request from which we're retrieving parameters
	 * @return
	 */
  def rawParams(_req: Box[Req]) = (for {
    req <- _req
    qs <- req.request.queryString
  } yield UrlHelpers.params(qs)) openOr Map[String, List[String]]()


	/**
	 * Raw POST data from a net.liftweb.http.Req
	 * @param req The request from which we're retrieving raw data.
	 * @return
	 */
  def rawPostData(req: Req): String = {
    (for (input <- req.rawInputStream) yield {
      Source.fromInputStream(input, charSet(req)).mkString("")
    }) openOr ""
  }
  def rawPostData(_req: Box[Req]): String = (for (r <- _req) yield rawPostData(r)) openOr ""


  /**
   * Get the character set from a Content-Type header
   * * taken from net.liftweb.http.Req
   * ** as of 01/11/2012 [[https://github.com/lift/framework/blob/master/web/webkit/src/main/scala/net/liftweb/http/Req.scala#L961]]
   */
  def charSet(req: Req, defaultCharSet: String = "UTF-8"): String = {
    def r = """; *charset=(.*)""".r
    def r2 = """[^=]*$""".r
    req.contentType.flatMap(ct => r.findFirstIn(ct).flatMap(r2.findFirstIn)).getOrElse(defaultCharSet)
  }
  def charSet(_req: Box[Req]): String = (for (req <- _req) yield charSet(req)) openOr ""

	/**
	 * Is the supplied request gzipped?
	 * @param req The request to evaluate.
	 * @return
	 */
	def gzip_?(req: Req): Boolean = req.header("Accept-Encoding").filter(_.contains("gzip")).isDefined

	/**
	 * @param req The request we're testing.
	 * @return True if the request url's scheme is https.
	 */
	def https_?(req: Req): Boolean = req.request.scheme == "https"


  /**
   * Amazon-specific stuff
   */
  object aws {
    /**
     * X-Forwarded-Proto is a header sent from AWS ELB.
     * [[http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/TerminologyandKeyConcepts.html]]
     * @param req The request we're testing.
     * @return True if the request url's scheme is https.
     */
    def https_?(req: Req): Boolean = req.header("X-Forwarded-Proto").map(_.toLowerCase) == Full("https")
  }
}