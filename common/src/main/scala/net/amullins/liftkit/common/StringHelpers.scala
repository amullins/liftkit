package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util.StringHelpers._


object StringHelpers extends StringHelpers with StringHelperImplicits


trait StringHelperImplicits {
	implicit def string2Str(in: String): SuperStr = new SuperStr(in)

	implicit def str2BoxStr(in: String): Box[String] = in.box
}


trait StringHelpers {

	/** partition a string into 2 strings by the first occurrence of a substring */
	def partition(str: String, substring: String = " "): (String, String) = {
		str.splitAt(substring) match {
			case Nil => (str, "")
			case x :: Nil => x
			case _ => ("","")
		}
	}

}


class SuperStr(str: String) {

  def crop(max: Int) = str.substring(0, math.min(str.length, max))

	def truncate(max: Int, tail: String = "...") = if (str.length > max) crop(max) + tail else str

	def null_? : Boolean = (null == str) || str.trim().isEmpty

	def box = if (this.null_?) Empty else Full(str)

	def dashed = str.replaceAll(" ", "-").replaceAll("[^a-zA-Z0-9-_]", "")

	def urlSafe = {
		val safe = """[^\w]""".r
		safe.replaceAllIn(str.trim, "-")
	}

	def sluggify = urlSafe.toLowerCase

}