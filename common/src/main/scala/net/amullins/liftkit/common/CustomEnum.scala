package net.amullins.liftkit.common

import net.liftweb.common.Box
import net.liftweb.util.Helpers._

import scala.collection.mutable


/**
 * A typed, thread-safe Enumeration (of sorts).
 * @tparam EnumType The enum's value type.
 *
 * {{{
 * case class FieldType(name: String) extends FieldTypes.Value
 * object FieldTypes extends CustomEnum[FieldType] {
 * 	 val Text = FieldType("Text")
 *	 val Int = FieldType("Int")
 *	 val Currency = FieldType("Currency")
 * }
 * }}}
 */
trait CustomEnum[EnumType] {
	protected var nextId = 0
	protected val values = new mutable.HashMap[Int, EnumType]

	def isValidValue(id: Int, v: EnumType): Boolean = id > 0
	def validValues = values.toList.filter(v => isValidValue(v._1, v._2)).map(_._2)

	lazy val all = values.toSeq.sortWith(_._1 < _._1).map(_._2)

	def apply(id: Int): EnumType = values(id)
	def withName(n: String): Box[EnumType] = values.find(_._2.toString == n).map(_._2)
	def get(n: String) = this.getClass.getMethods.find(_.getName == n).get.invoke(this).asInstanceOf[EnumType]
	def get_?(n: String) = tryo(get(n))

  private val _this = this

	trait Value { self: EnumType =>
		val id = nextId
		synchronized { nextId = id + 1 }
    values += (id -> this)

		def name: String

    def getEnum = _this

		override def toString = name
	}
}
