package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http.S
import net.liftweb.sitemap.{SiteMap, Loc}

import scala.collection.JavaConversions._
import scala.xml._

import java.net.URI

import org.apache.http.client.utils.URLEncodedUtils

import com.google.common.net.InternetDomainName

import net.amullins.liftkit.common.StringHelpers._


object UrlHelpers extends UrlHelpers with UrlHelperImplicits


trait UrlHelperImplicits {

	/**
	 * Safely convert a uri string to a java URI object
	 * ** for absolute urls, ONLY (checks for existence of host via `URI.getHost`)
	 */
	implicit def asURI(str: String): Box[URI] =
    for (uri <- tryo(new URI(str)) if null != uri.getHost) yield uri

	/**
	 * Convert a string to a SuperURI object
	 * * WARNING - the URI string must be properly encoded or this WILL throw an error
	 */
	@throws(classOf[RuntimeException])
	implicit def asSuperURI_!(uriString: String): SuperURI =
    new SuperURI((for (u <- asURI(uriString)) yield u) openOr {
	    throw new RuntimeException("Invalid URI String")
	  })

	/**
	 * Safely convert a java URI object to a SuperURI object. ~fancy~
	 */
	implicit def uriToSuperURI(uri: URI): SuperURI = new SuperURI(uri)

	/**
	 * Safely convert a SuperURI to a URI
	 */
	implicit def superURIToURI(s: SuperURI): URI = s.uri

	/**
	 * Meh - Do I really need to explain this...? Ok, it converts a Box of URI to a Box of SuperURI. Yippee!
	 */
	implicit def boxOfUriToSuperUri(uri: Box[URI]): Box[SuperURI] = uri.map(uriToSuperURI)

	implicit def boxOfStringToBoxedSuperURI(str: Box[String]): Box[SuperURI] = str.flatMap(asURI)
}


trait UrlHelpers {

  /**
   * Get the host and port information from props files
   */
  lazy val host = emptyForBlank(Props.get("host.name") openOr "")
  lazy val port = emptyForBlank(Props.get("host.port") openOr "")

	type Params = Map[String, List[String]]


  /**
   * Get a Loc URL from Loc name
   * @param locName The name of the Loc
   * @param absolute_? Should we absolute-ize this href?
   * @param https_? Should the absolute link be https instead of http?
   * @return The URL of the Loc (if the user has access).
   */
  def href(locName: String, absolute_? : Boolean = false, https_? : Boolean = false): Box[String] = {
    val h = SiteMap.findAndTestLoc(locName).flatMap(_.createDefaultLink.map(_.text))
    if (absolute_?) h.map(u => makeAbsolute(u, https_?)) else h
  }

  def href(loc: Loc[_]): String = loc.createDefaultLink.getOrElse(Text("/")).text
  def href(loc: Loc[_], absolute_? : Boolean): String = href(loc, absolute_?, false)
  def href(loc: Loc[_], absolute_? : Boolean, https_? : Boolean): String = {
    val h = href(loc)
    if (absolute_?) makeAbsolute(h, https_?) else h
  }

  /**
   * Get the current Loc's URL
   * @return The URL of the current Loc
   */
  def href: Box[String] =
  	S.location.flatMap(_.createDefaultLink.map(_.text))

	/**
	 * Extract parameters from a URI
	 * @param uri A URI from which to extract parameters
	 * @return
	 */
  def params(uri: Box[URI]): Params = uri.map(params) openOr Map.empty
  def params(uri: URI): Params = {
    URLEncodedUtils.parse(uri, "UTF-8").map(param => {
	    val v = param.getValue
	    (param.getName, if (null == v) "" else v)
    }).toList.groupBy(
      _._1 // name
    ).map(param =>
      (param._1, param._2.map(_._2))
    )
  }
  def params(uriString: String, encoded_? : Boolean = false): Params = {
    val firstIndex = uriString.indexOf("?")
    val queryString = if ((firstIndex != -1) && (uriString.length >= (firstIndex + 1))) {
      var _qs = uriString.splitAt(firstIndex + 1)._2

	    // remove hash
	    val hashIndex = _qs.indexOf("#")
	    if (hashIndex >= 0) _qs = _qs.substring(0, hashIndex)

      if (_qs=="") Empty else Full(_qs)
    } else Empty

    (for {
      qs <- queryString.toList
      nameVal <- qs.split("&").toList
      (name, value) <- nameVal.split("=").toList match {
        case Nil => Empty
        case n :: v :: rest if rest.nonEmpty => Full(n, v.box.openOr("")+"="+rest.map(_.box.openOr("")).mkString("="))
        case n :: v :: Nil => Full(n, v.box.openOr(""))
        case n :: _ => Full(n, "")
      }
    } yield if (encoded_?) (urlDecode(name), urlDecode(value)) else (name, value)).groupBy(
      _._1 // name
    ).map(param =>
      (param._1, param._2.map(_._2))
    )
  }


	/** Build a query string from a Map of parameters
   *  Map("foo"->"bar", "bar"->"foo", "q"->"spaced out") => foo=bar&bar=foo&q=spaced%20out
   */
  def buildQueryString(_params: Params): Box[String] = {
    val explodedParams = _params.toList.flatMap {
      case (paramName, paramValues) => paramValues.map(v => (urlEncode(paramName), urlEncode(v)))
    }
    if (explodedParams.nonEmpty) {
      Full(explodedParams.map(kv => kv._1 + "=" + kv._2).mkString("&"))
    } else Empty
  }

	def buildQueryString(_params: (String, String)*): Box[String] =
		buildQueryString(
			_params.groupBy(_._1).map {
				case (k, v) => (k, v.map(_._2).toList)
			}
		)


  /** Encode ONLY the query string of a given url
   *  http://www.test.com?foo=bar&q=spaced out => http://www.test.com?foo=bar&q=spaced%20out
   */
  def encodeQueryString(uriString: String): String = {
    val hostAndPath = uriString.indexOf("?") match {
	    case queryIndex if queryIndex < 0 => uriString
	    case queryIndex => uriString.substring(0, queryIndex)
    }
	  val hashPart = uriString.indexOf("#") match {
		  case hashIndex if hashIndex < 0 => ""
			case hashIndex => uriString.substring(hashIndex)
	  }
	  val queryString =  buildQueryString(params(uriString)).map("?"+_).openOr("")

	  hostAndPath + queryString + hashPart
  }


	/**
	 * Concatenate the host and port info into an absolute url to be used as the "base" url for making absolute links from relative links
	 */
	def getBaseUrl(https_? : Boolean = false) = "http%s://%s".format(
		if (https_?) "s" else "",
		List(host, port).flatten.mkString(":")
	)


	/**
	 * Makes an absolute uri.
	 * * If the passed uri is already absolute, it's simply passed through without modification.
	 */
	def makeAbsolute(
		uriString: String,
		https_? : Boolean = S.request.map(RequestHelpers.https_?).openOr(false)
	): String = {
		makeAbsoluteURI(uriString, https_?).map(_.toString).openOr("")
	}


	/**
	 * Makes an absolute URI.
	 * * If the passed uri is already absolute, it's simply passed through without modification.
	 */
	def makeAbsoluteURI(uriString: String, https_? : Boolean = false): Box[URI] = {
		tryo(new URI(uriString)) match {
			case Full(uri) if uri.isAbsolute => Full(uri)
			case Full(uri) => Full(new URI(getBaseUrl(https_?)).resolve(uri))
			case _ => Empty
		}
	}

}


/**
 * The SuperURI contains some convenience methods for working with a URI's parameters/query string
 */
class SuperURI(var uri: URI) {

	object domain {
		private var _name: Box[InternetDomainName] = tryo(InternetDomainName.from(uri.getHost))

		// the suffix (sld +) tld
		def suffix: List[String] = _name.map(_.publicSuffix().parts().toList) openOr Nil
		// all parts up to the domain
		def subdomain: List[String] = _name.map(_.parts().toList.dropRight(name.length)) openOr Nil
		// domain + suffix
		def name: List[String] = _name.map(_.parts().toList.takeRight(suffix.length + 1)) openOr Nil

		def reset() {
			_name = tryo(InternetDomainName.from(uri.getHost))
		}
	}

  def hasParam_?(param: String) = UrlHelpers.params(uri).exists(_._1 == param)

  def params: Map[String, List[String]] = UrlHelpers.params(uri)

  def param(paramName: String): List[String] = {
    UrlHelpers.params(uri).find(_._1 == paramName).map(_._2).getOrElse(Nil)
  }

  def findParam(paramName: String, paramValue: String): Box[String] = {
    param(paramName).find(_ == paramValue)
  }

	def withoutParams = {
		val uriString = uri.toString
		val firstIndex = uriString.indexOf("?")
		uri = new URI(if (firstIndex != -1) uriString.splitAt(firstIndex)._1 else uriString)
		domain.reset()
		uri.toString
	}

	def removeParams(p: String*) = {
		val uriString = uri.toString
		val firstIndex = uriString.indexOf("?")
		uri = new URI(
			(if (firstIndex != -1) uriString.splitAt(firstIndex)._1 else uriString) +
        UrlHelpers.buildQueryString(params -- p).map("?"+_).openOr("")
		)
		uri.toString
	}

	// encodes params automatically via `appendQueryParameters`
	def addParams(p: (String, String)*): String = {
		uri = new URI(appendQueryParameters(uri.toString, p.toList))
		domain.reset()
		uri.toString
	}

  def buildQueryString: Box[String] = UrlHelpers.buildQueryString(params)

  def encodeQueryString: String = uri.toString

	def secure_? : Boolean = uri.getScheme == "https"

	def setHost(newHost: String) = {
		uri = new URI(uri.toString.replaceFirst(uri.getHost, newHost))
		domain.reset()
		uri.toString
	}

}