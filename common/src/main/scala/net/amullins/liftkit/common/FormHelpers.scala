package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.http.S._
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad

import scala.xml._

import java.math.MathContext


/**
 * Some helpers for rendering custom form inputs.
 */
trait FormHelpers extends SHtml {

  def selectTimes = TimeSelects()

	def ajaxSubmitButton(label: NodeSeq, func: () => JsCmd, attrs: (String, String)*): Elem = {
		val funcName = "z" + nextFuncName

		S.addFunctionMap(funcName, S.contextFuncBuilder(func))

		attrs.foldLeft(<button type="submit" name={funcName}>{label}</button>)(_ % _) %
			("onclick" -> ("liftAjax.lift_uriSuffix = '" + funcName + "=_'; return true;"))
	}
	def ajaxSubmitButton(label: String, func: () => JsCmd, attrs: List[(String, String)] = Nil): Elem = {
		ajaxSubmitButton(Text(label), func, attrs:_*)
	}
	def ajaxSubmitButton(label: String, func: () => JsCmd, singleUse_? : Boolean, attrs: List[(String, String)]): Elem = {
		val btn = ajaxSubmitButton(Text(label), func, attrs:_*)
		if (singleUse_?) addCssClass("single-use", btn) else btn
	}

	def ajaxButton(label: NodeSeq, func: () => JsCmd, singleUse_? : Boolean, attrs: List[(String, String)]): Elem = {
		val btn = ajaxButton(label, func, attrs.map(a => ElemAttr.pairToBasic(a)):_*)
		if (singleUse_?) addCssClass("single-use", btn) else btn
	}

	def decimal(value: BigDecimal, func: BigDecimal => Any, attrs: (String, String)*): Elem =
		decimal(Full(value), func, java.math.MathContext.DECIMAL64, attrs : _*)

	def decimal(value: BigDecimal, func: BigDecimal => Any, mathContext: MathContext, attrs: (String, String)*): Elem =
		decimal(Full(value), func, mathContext, attrs : _*)

	def decimal(value: Box[BigDecimal], func: BigDecimal => Any, mathContext: MathContext, attrs: (String, String)*): Elem = {
    formField(
			"text",
			SFuncHolder(s => NumberHelpers.asDecimal(s, mathContext).map(func)),
			attrs:_*
		) % ("value" -> value.map(d => if (d == BigDecimal(0)) "0" else d.toString()).openOr(""))
	}

  def int(value: Int, func: Int => Any, attrs: (String, String)*): Elem =
    int(Full(value), func, attrs : _*)

  def int(value: Box[Int], func: Int => Any, attrs: (String, String)*): Elem = {
    formField(
      "text",
      SFuncHolder(s => asInt(s).map(func)),
      attrs:_*
    ) % ("value" -> value.map(i => if (i==0) "0" else i.toString).openOr(""))
  }

	def long(value: Long, func: Long => Any, attrs: (String, String)*): Elem =
		long(Full(value), func, attrs : _*)

	def long(value: Box[Long], func: Long => Any, attrs: (String, String)*): Elem = {
	  formField(
	    "text",
	    SFuncHolder(s => asLong(s).map(func)),
	    attrs:_*
	  ) % ("value" -> value.map(l => if (l==0L) "0" else l.toString).openOr(""))
	}

	def numberMaskScript(field: Elem, mask: String = """n""") = Script(JqOnLoad(
		JsRaw("""$("input[name=%s]").inputmask({mask:"%s",greedy:false, repeat:15})""".format(
			(field \\ "@name").text,
			mask
		)).cmd
	))


	def formField(fieldType: String, func: AFuncHolder, attrs: (String, String)*) = {
		fmapFunc(func)(funcName => {
			attrs.foldLeft(<input type={fieldType} name={funcName} />)(_ % _)
		})
	}


	def sel(opts: Seq[(String, String)], selected: Box[String], attrs: (String, String)*) = {
    attrs.foldLeft(<select>{
			opts.flatMap {
				case (value, text) => <option value={value}>{text}</option> % HtmlHelpers.selectedAttribute(selected.exists(_ == value))
			}
		}</select>)(_ % _)
	}

	def checkbox[T](possible: Seq[T], actual: Seq[T], checked: T => Boolean, func: Seq[T] => Any, attrs: (String, String)*): ChoiceHolder[T] = {
	  val len = possible.length
	  fmapFunc(LFuncHolder((strl: List[String]) => {
		  func(strl.map(toInt(_)).filter(x => x >= 0 && x < len).map(possible(_)))
		  true
	  })) {name => {
		  ChoiceHolder(possible.toList.zipWithIndex.map(p => ChoiceItem(
			  p._1,
		    attrs.foldLeft(<input type="checkbox" name={name} value={p._2.toString} />)(_ % _) % HtmlHelpers.checkedAttribute(checked(p._1)) ++
			    (if (p._2 == 0) <input type="hidden" name={name} value="-1"/> else Nil)
		  )))
	  }}
	}

	/**
	 * Generate a pair of password input fields for "password" and "repeat password". The fields will have the same
	 * name so that they're submitted as a pair.
	 * @param onSubmit What happens when the form is submitted
	 * @param mask_? Should these be password fields or just plain text fields
	 * @return
	 */
	def passwordInputs(onSubmit: S.AFuncHolder, mask_? : Boolean = true) = {
		val inputType = if (mask_?) "password" else "text"

		S.fmapFunc(onSubmit)(funcName => (
			<input type={inputType} name={funcName} value="" />,
			<input type={inputType} name={funcName} value="" />
		))
	}
}