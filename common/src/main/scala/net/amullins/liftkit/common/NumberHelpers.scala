package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util.ControlHelpers._

import java.lang.{Double => JDouble}
import java.math.MathContext
import java.text.{DecimalFormat, MessageFormat, DecimalFormatSymbols}


object NumberHelpers extends NumberHelpers


trait NumberHelpers {

	val usdFormat = {
		val symbols = new DecimalFormatSymbols
			symbols.setDecimalSeparator('.')
			symbols.setGroupingSeparator(',')
		val df = new DecimalFormat("'$'0.##", symbols)
			df.setGroupingSize(3)
			df.setGroupingUsed(true)
			df.setMaximumFractionDigits(2)
			df.setMinimumFractionDigits(2)
		df
	}

  /**
   * Format a decimal as US monies.
   * @param d The decimal value to be represented as currency.
   */
	def usd(d: BigDecimal): String = usdFormat.format(new JDouble(d.toString()))

  /**
   * Format a decimal as a percentage.
   * @param d The decimal value to the represented as a percentage.
   * @param precision How many decimal places should be output.
   */
	def percentage(d: BigDecimal, precision: Int = 4): String = {
		MessageFormat.format("{0,number,#" + (if (precision > 0) "." + ("#" * precision) else "") + "%}", new JDouble(d.toString()))
	}

  /**
   * Parse a string to a decimal.
   * @param s A decimal value as a string.
   * @param context The MathContext to be used for parsing.
   */
	def asDecimal(s: String, context: MathContext = MathContext.DECIMAL64): Box[BigDecimal] = {
		val frmt = new DecimalFormat()
		frmt.setParseBigDecimal(true)
		if (null == s || s == "") Empty
		else tryo(frmt.parse(s).asInstanceOf[java.math.BigDecimal].round(context))
	}

	def toRadians(d: Double) = d * (math.Pi/180)
	def toDegrees(d: Double) = d * (180/math.Pi)

	@throws(classOf[NumberFormatException])
	def toFloat(s: String): Float = java.lang.Float.valueOf(s.trim).floatValue()

  /**
   * Convert a string to a Float
   * @param s A float value as a string.
   */
	def asFloat(s: String): Box[Float] = tryo(toFloat(s))


	implicit def dbl2SuperDouble(d: Double): SuperDouble = new SuperDouble(d)

  class SuperDouble(d: Double) {
  	def degrees: Double = toDegrees(d)
  	def radians: Double = toRadians(d)
  }
}
