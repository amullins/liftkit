package net.amullins.liftkit.common

import net.liftweb.common._

import scala.xml._
import scala.xml.factory.XMLLoader

import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl


object HtmlHelpers extends HtmlHelpers

trait HtmlHelpers {

	/**
	 * Retrieve an attribute value from a given node.
	 * @param n The node from which to retrieve the attribute.
	 * @param name The attribute to be retrieved.
	 * @return The value of the attribute, if found. Empty otherwise.
	 */
	def attr(n: Node, name: String): Box[String] = n.attribute(name).flatMap(_.headOption).map(_.text)

	/**
	 * Optionally create an attribute `UnprefixedAttribute`.
	 * @param attribute The name of the attribute. (used as both name and value)
	 * @param b Whether the attribute should be created.
	 * @return A new attribute that can be appended to a `Elem` via `Elem.%`.
	 */
	def toggle(attribute: String, b: Boolean) = if (b) new UnprefixedAttribute(attribute, attribute, Null) else Null
	def selectedAttribute(b: Boolean) = toggle("selected", b)
	def checkedAttribute(b: Boolean) = toggle("checked", b)

	/**
	 * Add a non-breaking space.
	 * @param n How many spaces to add.
	 * @return Sequence of spaces that can be appended to HTML.
	 */
	def nbsp(n: Int = 1) = Unparsed("&nbsp;" * n)


	/**
	 * Use TagSoup for parsing XML/HTML strings into usable Elem
	 */
  private val saxFactory = new SAXFactoryImpl
	def tagsoup: XMLLoader[Elem] = XML.withSAXParser(saxFactory.newSAXParser())


	/**
	 * Includes SHtml methods.
	 */
	object form extends FormHelpers


	implicit def nodeToSuperNode(n: Node): SuperNode = new SuperNode(n)(this)

}


/**
 * Tack on some additional methods to `Node`
 * @param node An existing Node.
 */
private[common] class SuperNode(node: Node)(implicit htmlHelpers: HtmlHelpers) {

	def name = (node \\ "@name").text
  def id = (node \\ "@id").text

  def data(dataName: String): Box[String] = (node \\ ("@data-" + dataName)).headOption.map(_.text)

	def attr(attributeName: String): Box[String] = htmlHelpers.attr(node, attributeName)

}
