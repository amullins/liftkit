package net.amullins.liftkit.common


package object date {

  private[date] var _helpers: DateHelpers = null
  def Helpers = _helpers

  def init(h: DateHelpers) = synchronized { _helpers = h }

}
