package net.amullins.liftkit.common.date

import net.liftweb.common._
import net.liftweb.util.TimeHelpers._
import java.text.DecimalFormat
import java.util.Date
import org.joda.time.DateTime

import net.amullins.liftkit.common.CustomEnum
import net.liftweb.http.RequestVar


trait DateHelpers {

  def getCurrentTimeZone: Box[HasTimeZone]

  object CurrentTimeZone extends RequestVar[Box[HasTimeZone]](getCurrentTimeZone)

  val java = new JavaDateHelpers {}
  val joda = new JodaDateHelpers {}

  def user(d: Date): LocalDateTime = LocalDateTime(d)
  def user(dt: DateTime): LocalDateTime = LocalDateTime(dt)

  implicit def dToLocal(d: Date): LocalDateTime = LocalDateTime(d)
  implicit def dtToLocal(dt: DateTime): LocalDateTime = LocalDateTime(dt)


	val hourSelectValues = (1 to 12).map((v: Int) => (v, v.toString))
	val minuteSelectValues = (1 to 59).map((v: Int) => (v, v.toString))
	val meridiemSelectValues = Seq(("AM", "AM"), ("PM", "PM"))


	def timespanToReadable(ts: TimeSpan, units: ReadableTimeUnits = ShortReadableUnits) = millisToReadable(ts.millis, units)

	def millisToReadable(millis: Long, units: ReadableTimeUnits = ShortReadableUnits): String = {
		val numFormat = new DecimalFormat("#.##")
		def readable(n: Double, unit: String): String = ("%s " + unit).format(numFormat.format(n))
		val seconds: Double = millis / 1000
		if (math.round(seconds)==1) readable(1, units.second)
		else if (seconds < 60) readable(math.round(seconds), units.seconds)
		else if (seconds == 60) readable(1, units.minute)
		else {
			val minutes: Double = seconds / 60
			if (minutes < 60) readable(math.round(minutes), units.minutes)
			else if (minutes == 60) readable(1, units.hour)
			else {
				val hours: Double = minutes / 60
				if (hours < 24) readable(hours, units.hours)
				else if (hours == 24) readable(1, units.day)
				else readable(math.round(hours / 24), units.days)
			}
		}
	}


	// http://stackoverflow.com/a/4011232/213155
	def daySuffix(day: Int): String = {
		day % 10 match { //
			case _ if day < 1 || day > 31 => JavaDateHelpers.debug("Invalid day of month"); "" // fail quietly
			case _ if day >= 11 && day <= 13 => "th"
			case 1 => "st"
			case 2 => "nd"
			case 3 => "rd"
			case _ => "th"
		}
	}


	val RelativeTimeSelectValues = Seq(
		(15.minutes.millis, "15 Mins Before"),
		(30.minutes.millis, "30 Mins Before"),
		(1.hour.millis, "1 Hr Before"),
		(1.day.millis, "1 Day Before")
	)

	val RelativeTimeSelectValuesExtended = Seq(
		(5.minutes.millis, "5 Mins Before"),
		(15.minutes.millis, "15 Mins Before"),
		(30.minutes.millis, "30 Mins Before"),
		(1.hour.millis, "1 Hr Before"),
		(2.hours.millis, "2 Hrs Before"),
		(3.hours.millis, "3 Hrs Before"),
		(4.hours.millis, "4 Hrs Before"),
		(5.hours.millis, "5 Hrs Before"),
		(6.hours.millis, "6 Hrs Before"),
		(7.hours.millis, "7 Hrs Before"),
		(8.hours.millis, "8 Hrs Before"),
		(9.hours.millis, "9 Hrs Before"),
		(10.hours.millis, "10 Hrs Before"),
		(11.hours.millis, "11 Hrs Before"),
		(12.hours.millis, "12 Hrs Before"),
		(1.day.millis, "1 Day Before"),
		(2.days.millis, "2 Days Before")
	)

}


/**
 * For use with ValidatableEnum. For specifying a time frame, used alongside
 * a time value for a time frame representation like "2 minutes".
 */
case class TimeUnit(name: String) extends TimeUnits.Value
object TimeUnits extends CustomEnum[TimeUnit] {
	val defaultValue = TimeUnit("Choose Type")

	val Minutes = TimeUnit("Minutes")
	val Hours =   TimeUnit("Hours")
	val Days =    TimeUnit("Days")
	val Weeks =   TimeUnit("Weeks")
	val Months =  TimeUnit("Months")
}


case class DayOfWeek(name: String) extends Days.Value
object Days extends CustomEnum[DayOfWeek] {
	val defaultValue =  DayOfWeek("Select Day")

	val Sunday =    DayOfWeek("Sunday")
	val Monday =    DayOfWeek("Monday")
	val Tuesday =   DayOfWeek("Tuesday")
	val Wednesday = DayOfWeek("Wednesday")
	val Thursday =  DayOfWeek("Thursday")
	val Friday =    DayOfWeek("Friday")
	val Saturday =  DayOfWeek("Saturday")
}


abstract class ReadableTimeUnits(
	val second: String, val seconds: String,
	val minute: String, val minutes: String,
	val hour: String, val hours: String,
	val day: String, val days: String
)
case object ShortReadableUnits extends ReadableTimeUnits("Sec","Sec","Min","Mins","Hr","Hrs","Day","Days")
case object LongReadableUnits extends ReadableTimeUnits("Second","Seconds","Minute","Minutes","Hour","Hours","Day","Days")


trait ConvertableTimeZone {
	def asDateTimeZone: org.joda.time.DateTimeZone
	def asTimeZone: java.util.TimeZone
}
trait HasTimeZone {
	def timezone: ConvertableTimeZone
}


trait ReadableDate[D] {
	def readable: Readable

	trait Readable {
		def apply(d: D): String
		def withSuffix(d: D): String
	}
}


trait DateFormats[F] {
	def fullDate: F
	def partialDate: F
	def monthAbbr: F
	def hour: F
	def minute: F
	def meridiem: F
	def time: F
	def time12: F
	def fullDateTime: F
	def partialDateTime: F
	def rfc2822: F
	def readableDate: F
	def fullDateShortYear: F
	def compactDate: F
	def compactDateTime: F
	def rfc1123: F
}


trait NamedDates[D, TZ] {
	def eod(d: D): D

	def xDaysAgo(days: Int, tz: TZ): D

	def xMonthsAgo(months: Int, tz: TZ): D

	def today: D
	def today(tz: TZ): D

	def endOfToday: D
	def endOfToday(tz: TZ): D

	def tomorrow: D
	def tomorrow(tz: TZ): D

	def endOfTomorrow: D
	def endOfTomorrow(tz: TZ): D

	def yesterday: D
	def yesterday(tz: TZ): D

	def endOfYesterday: D
	def endOfYesterday(tz: TZ): D

	def thisWeek: D
	def thisWeek(tz: TZ): D

	def endOfThisWeek: D
	def endOfThisWeek(tz: TZ): D

	def lastWeek: D
	def lastWeek(tz: TZ): D

	def endOfLastWeek: D
	def endOfLastWeek(tz: TZ): D

	def thisMonth: D
	def thisMonth(tz: TZ): D

	def endOfThisMonth: D
	def endOfThisMonth(tz: TZ): D

	def lastMonth: D
	def lastMonth(tz: TZ): D

	def endOfLastMonth: D
	def endOfLastMonth(tz: TZ): D

	def thirtyDaysAgo: D
	def thirtyDaysAgo(tz: TZ): D

	def twelveMonthsAgo: D
	def twelveMonthsAgo(tz: TZ): D

	def firstDayOfYear: D
	def firstDayOfYear(tz: TZ): D

	def firstDayOfLastYear: D
	def firstDayOfLastYear(tz: TZ): D

	def endOfLastYear: D
	def endOfLastYear(tz: TZ): D
}


trait TimeSelectValues[T, TF] {
	def times: Times

	trait Times {
		def apply(from: T, span: Long, increment: Long): Seq[TF]
		def from(startTime: TF): Seq[TF]
	}
}


trait DateParser[T, TZ] {
	def parseDate(d: String): Box[T]
	def parseDate(tz: TZ, d: String): Box[T]
}


trait AsTimestamp[D] {
	def asTimestamp(d: D): Long
}


trait SameDay[D] {
	def sameDay_?(d1: D, d2: D): Boolean
}