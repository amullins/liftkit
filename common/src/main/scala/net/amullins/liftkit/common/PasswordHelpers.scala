package net.amullins.liftkit.common

import com.Ostermiller.util.RandPass


object PasswordHelpers extends PasswordHelpers


trait PasswordHelpers {

	private val defaultChars = """AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789!\#$%&*+-_/?@^""".toCharArray

	val withSymbols: RandPass = new RandPass(defaultChars)
	withSymbols.setFirstAlphabet(RandPass.LETTERS_ALPHABET)
	withSymbols.setMaxRepetition(1)

	val alphaNumeric: RandPass = new RandPass(RandPass.NUMBERS_AND_LETTERS_ALPHABET)
	alphaNumeric.setMaxRepetition(1)

	def generate(len: Int, generator: RandPass = withSymbols): String = generator.getPass(len)

}