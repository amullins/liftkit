package net.amullins.liftkit.common.date

import java.util.Date
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter


object LocalDateTime {
	def apply(d: Date): LocalDateTime =
		LocalDateTime(new DateTime(d.getTime).withZone(JodaDateHelpers.currentTimeZone))
}

case class LocalDateTime(dt: DateTime) {
	def get = dt.withZone(JodaDateHelpers.currentTimeZone)

	def format(formatter: DateTimeFormatter): String = formatter.print(get)

	def format(p: String): String = JodaDateHelpers.format(pattern = p).print(get)
}