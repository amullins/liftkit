package net.amullins.liftkit.common.date

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._

import java.util.Locale

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time._


object JodaDateHelpers extends JodaDateHelpers


trait JodaDateHelpers
  extends ReadableDate[DateTime]
	with DateFormats[DateTimeFormatter]
	with TimeSelectValues[LocalTime, LocalTime]
	with DateParser[DateTime, DateTimeZone]
	with AsTimestamp[DateTime]
	with SameDay[DateTime]
	with NamedDates[DateTime, DateTimeZone]
{

  def currentTimeZone = Helpers.CurrentTimeZone.get.map(_.timezone.asDateTimeZone) openOr DateTimeZone.getDefault


	val readable = new Readable {
		def apply(d: DateTime): String = readableDate.print(d)
		def withSuffix(d: DateTime): String = apply(d) + Helpers.daySuffix(d.getDayOfMonth)
	}


	def format(tz: DateTimeZone = currentTimeZone, pattern: String): DateTimeFormatter =
		DateTimeFormat.forPattern(pattern).withZone(tz)

	val fullDate: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
	val partialDate: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd")
	val monthAbbr: DateTimeFormatter = DateTimeFormat.forPattern("MMM")
	val hour: DateTimeFormatter = DateTimeFormat.forPattern("h")
	val minute: DateTimeFormatter = DateTimeFormat.forPattern("m")
	val meridiem: DateTimeFormatter = DateTimeFormat.forPattern("a")
	val time: DateTimeFormatter = DateTimeFormat.forPattern("hh:mm a")
	val time12: DateTimeFormatter = DateTimeFormat.forPattern("h:mm a")
	val fullDateTime: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a")
	val partialDateTime: DateTimeFormatter = DateTimeFormat.forPattern("M/dd h:mm a")
	val rfc2822: DateTimeFormatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z")
	val readableDate: DateTimeFormatter = DateTimeFormat.forPattern("MMMM dd")
	val fullDateShortYear: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yy")
	val compactDate: DateTimeFormatter = DateTimeFormat.forPattern("M/d")
	val compactDateTime: DateTimeFormatter = DateTimeFormat.forPattern("M/dd/yy h:mm a")
	val rfc1123: DateTimeFormatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'").withLocale(Locale.US).withZone(DateTimeZone.UTC)
	val iso8601: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")

	def fullDate(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = fullDate.withZone(tz)
	def partialDate(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = partialDate.withZone(tz)
	def monthAbbr(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = monthAbbr.withZone(tz)
	def hour(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = hour.withZone(tz)
	def minute(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = minute.withZone(tz)
	def meridiem(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = meridiem.withZone(tz)
	def time(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = time.withZone(tz)
	def time12(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = time12.withZone(tz)
	def fullDateTime(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = fullDateTime.withZone(tz)
	def partialDateTime(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = partialDateTime.withZone(tz)
	def rfc2822(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = rfc2822.withZone(tz)
	def readableDate(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = readableDate.withZone(tz)
	def fullDateShortYear(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = fullDateShortYear.withZone(tz)
	def compactDate(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = compactDate.withZone(tz)
	def compactDateTime(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = compactDateTime.withZone(tz)
	def iso8601(tz: DateTimeZone = currentTimeZone): DateTimeFormatter = iso8601.withZone(tz)


	object times extends Times {
		def apply(
			from: LocalTime = new LocalTime(0, 0, 0, 0),
		  span: Long = 24.hours.millis,
		  increment: Long = 15.minutes.millis
		): Seq[LocalTime] = {
			val rows: Int = (span / increment).toInt
			(0 to (rows - 1)).map((v: Int) => {
				from.plusMillis(increment.toInt * v)
			})
		}

		def from(startTime: LocalTime): Seq[LocalTime] = {
			val increment = 15.minutes
			val rows: Int = ((1.day.millis - startTime.getMillisOfDay - 15.minutes.millis) / increment).toInt
			(0 to rows).map((v: Int) => {
				startTime.plusMinutes(15).plusMillis(increment.toInt * v)
			})
		}
	}


	def parseDate(d: String): Box[DateTime] = tryo { fullDate.parseDateTime(d) }
	def parseDate(tz: DateTimeZone, d: String): Box[DateTime] = tryo { fullDate(tz).parseDateTime(d) }


	def parseTime(t: String): Box[LocalTime] = tryo { time.parseDateTime(t).toLocalTime }
	def parseTime(tz: DateTimeZone, t: String): Box[LocalTime] = tryo { time(tz).parseDateTime(t).toLocalTime }


	def fromTimestamp(d: String): Box[DateTime] = {
		for (l <- asLong(d)) yield {
			new DateTime(l * 1000, DateTimeZone.UTC)
		}
	}

	def fromTimestamp(d: Box[String]): Box[DateTime] = d.flatMap(fromTimestamp)


	def asTimestamp(d: DateTime): Long = d.getMillis / 1000


	def getNextTime(increment: TimeHelpers#TimeSpan, from: Long = millis, timeToAdd: TimeHelpers#TimeSpan = 0.minutes): LocalTime = {
		val dt = new MutableDateTime(from)
		dt.setMillisOfDay({
			val _currentMillis = dt.getMillisOfDay
			(_currentMillis - (_currentMillis % increment.millis) + (increment.millis + timeToAdd.millis)).toInt % 24.hours.millis.toInt
		})

		new LocalTime(dt)
	}


	def sameDay_?(d1: DateTime, d2: DateTime): Boolean = DateTimeComparator.getDateOnlyInstance.compare(d1, d2) == 0


	def now = new DateTime(currentTimeZone)


	def eod(d: DateTime): DateTime = d.property(DateTimeFieldType.millisOfDay()).withMaximumValue()

	def xDaysAgo(days: Int, tz: DateTimeZone = currentTimeZone) = {
		val t = today(tz).toMutableDateTime; t.addDays(-days); t.toDateTime
	}

	def xMonthsAgo(months: Int, tz: DateTimeZone = currentTimeZone) = {
		val t = today(tz).toMutableDateTime; t.addMonths(-months); t.addDays(-1); t.toDateTime
	}

	def today: DateTime = today(DateTimeZone.UTC)
	def today(tz: DateTimeZone = currentTimeZone): DateTime = new DateTime(tz).withTimeAtStartOfDay()

	def endOfToday: DateTime = endOfToday(DateTimeZone.UTC)
	def endOfToday(tz: DateTimeZone = currentTimeZone): DateTime = eod(today(tz))

	def tomorrow: DateTime = tomorrow(DateTimeZone.UTC)
	def tomorrow(tz: DateTimeZone = currentTimeZone): DateTime = today(tz).withTimeAtStartOfDay().plusDays(1)

	def endOfTomorrow: DateTime = endOfTomorrow(DateTimeZone.UTC)
	def endOfTomorrow(tz: DateTimeZone = currentTimeZone): DateTime = eod(tomorrow(tz))

	def yesterday: DateTime = yesterday(DateTimeZone.UTC)
	def yesterday(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).toMutableDateTime; t.addDays(-1); t.toDateTime
	}

	def endOfYesterday: DateTime = endOfYesterday(DateTimeZone.UTC)
	def endOfYesterday(tz: DateTimeZone = currentTimeZone): DateTime = eod(yesterday(tz))

	def thisWeek: DateTime = thisWeek(DateTimeZone.UTC)
	def thisWeek(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).withDayOfWeek(1).toMutableDateTime; t.addDays(-1); t.toDateTime
	}

	def endOfThisWeek: DateTime = endOfThisWeek(DateTimeZone.UTC)
	def endOfThisWeek(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = thisWeek(tz).toMutableDateTime; t.addDays(7); t.addMillis(-1); t.toDateTime
	}

	def lastWeek: DateTime = lastWeek(DateTimeZone.UTC)
	def lastWeek(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = thisWeek(tz).toMutableDateTime; t.addWeeks(-1); t.toDateTime
	}

	def endOfLastWeek: DateTime = endOfLastWeek(DateTimeZone.UTC)
	def endOfLastWeek(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = lastWeek(tz).toMutableDateTime; t.addDays(7); t.addMillis(-1); t.toDateTime
	}

	def thisMonth: DateTime = thisMonth(DateTimeZone.UTC)
	def thisMonth(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).withDayOfMonth(1); t.toDateTime
	}

	def endOfThisMonth: DateTime = endOfThisMonth(DateTimeZone.UTC)
	def endOfThisMonth(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).dayOfMonth().withMaximumValue().toMutableDateTime
		t.addDays(1)
		t.addMillis(-1)
		t.toDateTime
	}

	def lastMonth: DateTime = lastMonth(DateTimeZone.UTC)
	def lastMonth(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = thisMonth(tz).toMutableDateTime; t.addMonths(-1); t.toDateTime
	}

	def endOfLastMonth: DateTime = endOfLastMonth(DateTimeZone.UTC)
	def endOfLastMonth(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = lastMonth(tz).dayOfMonth().withMaximumValue().toMutableDateTime
		t.addDays(1)
		t.addMillis(-1)
		t.toDateTime
	}

	def thirtyDaysAgo: DateTime = thirtyDaysAgo(DateTimeZone.UTC)
	def thirtyDaysAgo(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).toMutableDateTime; t.addDays(-30); t.toDateTime
	}

	def twelveMonthsAgo: DateTime = twelveMonthsAgo(DateTimeZone.UTC)
	def twelveMonthsAgo(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).toMutableDateTime; t.addMonths(-12); t.addDays(-1); t.toDateTime
	}

	def firstDayOfYear: DateTime = firstDayOfYear(DateTimeZone.UTC)
	def firstDayOfYear(tz: DateTimeZone = currentTimeZone): DateTime = today(tz).dayOfYear().withMinimumValue().toDateTime

	def firstDayOfLastYear: DateTime = firstDayOfLastYear(DateTimeZone.UTC)
	def firstDayOfLastYear(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = today(tz).toMutableDateTime; t.addYears(-1); t.toDateTime.dayOfYear().withMinimumValue().toDateTime
	}

	def endOfLastYear: DateTime = endOfLastYear(DateTimeZone.UTC)
	def endOfLastYear(tz: DateTimeZone = currentTimeZone): DateTime = {
		val t = firstDayOfYear(tz).toMutableDateTime; t.addMillis(-1); t.toDateTime
	}

}
