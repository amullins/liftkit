package net.amullins.liftkit.common

import net.liftweb.http._


/**
 * Do something when a session variable is set. In other words, setting the session variable triggers a side effect.
 * @param dflt The default value of the session variable.
 * @param sideEffectFunc The function to be called when the session variable is set.
 * @tparam T The session variable type.
 */
abstract class SessionVarWithSideEffects[T](dflt: T)(sideEffectFunc: T => Unit)
	extends SessionVar[T](dflt)
{
	override def set(v: T) = {
		sideEffectFunc(v)
		super.set(v)
	}
}