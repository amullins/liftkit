package net.amullins.liftkit.common.date

import net.liftweb.common._
import net.liftweb.util.TimeHelpers
import net.liftweb.util.Helpers.tryo

import scala.collection.mutable

import java.util.Date

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time._

import net.amullins.liftkit.common.StringHelpers
import JodaDateHelpers._


object DateRanges extends Enumeration {

	val Today: NamedDateRange =
		NamedDateRange("Today", today _, endOfToday _)
	def Today(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange = {
		NamedDateRange("Today", () => today(user), () => endOfToday(user))
	}

	val Yesterday =
		NamedDateRange("Yesterday", yesterday _, endOfYesterday _)
	def Yesterday(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Yesterday", () => yesterday(user), () => endOfYesterday(user))

	val ThisWeek: NamedDateRange =
		NamedDateRange("This Week", thisWeek _, endOfThisWeek _)
	def ThisWeek(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("This Week", () => thisWeek(user), () => endOfThisWeek(user))

	val LastWeek: NamedDateRange =
		NamedDateRange("Last Week", lastWeek _, endOfLastWeek _)
	def LastWeek(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Last Week", () => lastWeek(user), () => endOfLastWeek(user))

	val ThisMonth: NamedDateRange =
		NamedDateRange("This Month", thisMonth _, endOfThisMonth _)
	def ThisMonth(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("This Month", () => thisMonth(user), () => endOfThisMonth(user))

	val LastMonth: NamedDateRange =
		NamedDateRange("Last Month", lastMonth _, endOfLastMonth _)
	def LastMonth(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Last Month", () => lastMonth(user), () => endOfLastMonth(user))

	val Last30Days: NamedDateRange =
		NamedDateRange("Last 30 Days", thirtyDaysAgo _, endOfYesterday _)
	def Last30Days(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Last 30 Days", () => thirtyDaysAgo(user), () => endOfYesterday(user))

	val Last12Months: NamedDateRange =
		NamedDateRange("Last 12 Months", twelveMonthsAgo _, endOfYesterday _)
	def Last12Months(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Last 12 Months", () => twelveMonthsAgo(user), () => endOfYesterday(user))

	val ThisYear: NamedDateRange =
		NamedDateRange("This Year", firstDayOfYear _, () => now)
	def ThisYear(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("This Year", () => firstDayOfYear(user), () => new DateTime(timezone(user)))

	val LastYear: NamedDateRange =
		NamedDateRange("Last Year", firstDayOfLastYear _, endOfLastYear _)
	def LastYear(user: Box[HasTimeZone] = Helpers.CurrentTimeZone.get): NamedDateRange =
		NamedDateRange("Last Year", () => firstDayOfLastYear(user), () => endOfLastYear(user))


	private implicit def timezone(u: HasTimeZone): DateTimeZone = u.timezone.asDateTimeZone
	private implicit def timezone(u: Box[HasTimeZone]): DateTimeZone = u.map(_.timezone.asDateTimeZone) openOr DateTimeZone.UTC


	object NamedRanges {
		val all = mutable.ListBuffer[NamedDateRange]()
		def unapply(r: DRange): Option[NamedDateRange] = all.find(_ == r)
	}

	case class NamedDateRange(name: String, fromDateTime: () => DateTime, toDateTime: () => DateTime) extends Val(name) with DRange {
		def from = fromDateTime()
		def to = toDateTime()

		val asURLParamValue = StringHelpers.string2Str(name.toLowerCase).dashed

		NamedRanges.all += this
	}

	object Range {
		private def defaultDateFormat = DateTimeFormat.forPattern("MMddyyyy").withZone(currentTimeZone)

		def apply(t: (Date, Date)): Range = Range(new DateTime(t._1), new DateTime(t._2))
		def apply(from: Date, to: Date) : Range = Range(new DateTime(from), new DateTime(to))
		def apply(interval: Interval): Range = Range(interval.getStart, interval.getEnd)

		def fromString(
			dateString: String,
			dateFormat: DateTimeFormatter = defaultDateFormat
		): Box[DRange] = {
			dateString match {
				case "today" => Full(DateRanges.Today())
				case "yesterday" => Full(DateRanges.Yesterday())
				case "this-week" => Full(DateRanges.ThisWeek())
				case "last-week" => Full(DateRanges.LastWeek())
				case "this-month" => Full(DateRanges.ThisMonth())
				case "last-month" => Full(DateRanges.LastMonth())
				case "last-30-days" => Full(DateRanges.Last30Days())
				case "last-12-months" => Full(DateRanges.Last12Months())
				case "this-year" => Full(DateRanges.ThisYear())
				case "last-year" => Full(DateRanges.LastYear())
				case _ =>
					DateFromString.parse(dateString, dateFormat).map(d =>
						Full(DateRanges.Range(d, endOfToday()))
					).openOr(DatesFromString.parse(dateString, dateFormat).map(d =>
						DateRanges.Range(d._1, d._2)
					)) match {
						case Full(NamedRanges(r)) => Full(r)
						case r => r
					}
			}
		}

		def toString(range: DRange, dateFormat: DateTimeFormatter = defaultDateFormat): String =
			dateFormat.print(range.from) + "," + dateFormat.print(range.to)

		object DateFromString {
			def parse(str: String, dateFormat: DateTimeFormatter = defaultDateFormat): Box[DateTime] =
				tryo(dateFormat.withZone(currentTimeZone).parseDateTime(str)) match {
					case Full(d) => Full(d)
					case _ => fromTimestamp(str)
				}

			def unapply(str: String): Option[DateTime] = parse(str)
		}

		object DatesFromString {
			def parse(str: String, dateFormat: DateTimeFormatter = defaultDateFormat): Box[(DateTime, DateTime)] =
				StringHelpers.partition(str,",").productIterator.flatMap{
					case d: String => tryo(dateFormat.withZone(Helpers.joda.currentTimeZone).parseDateTime(d))
					case _ => Empty
				}.toList match {
					case d1 :: d2 :: Nil => Some((d1, Helpers.joda.eod(d2)))
					case _ => None
				}

			def unapply(str: String): Option[(DateTime, DateTime)] = parse(str)
		}
	}
	case class Range(from: DateTime, to: DateTime) extends DRange

	trait DRange {
		def from: DateTime
		def to: DateTime

		def toTuple = (from, to)

		/**
		 * Is the supplied date within this range. (after from, before to)
		 * @param d Date to check.
		 * @return True if date is in range.
		 */
		def contains(d: DateTime): Boolean =
			(d.isAfter(from) || d.getMillis == from.getMillis) &&
			(d.isBefore(to) || d.getMillis == to.getMillis)

		/**
		 * Determine if a start and end date are entirely within this range.
		 * @param d1 First date in range to check
		 * @param d2 Last date in range to check
		 * @return True if the supplied dates are entirely within this range
		 */
		def encapsulates(d1: DateTime, d2: DateTime): Boolean =
			(d1.isAfter(from) && d1.isBefore(to)) &&
			(d2.isAfter(from) && d2.isBefore(to))
		def encapsulates(dr: DRange): Boolean = encapsulates(dr.from, dr.to)

		/**
		 * Determine if a range (defined by a start and end date) overlaps this range.
		 * @param d1 The start date of the range to check.
		 * @param d2 The end date of the range to check.
		 * @return True if the supplied range overlaps this range.
		 */
		def overlaps(d1: DateTime, d2: DateTime): Boolean = {
			((d1.isAfter(from) || d1.equals(from)) && d1.isBefore(to)) || // if from is in range
			(d2.isAfter(from) && (d2.isBefore(to) || d2.equals(to))) || // if to is in range
			Range(d1, d2).encapsulates(this) // supplied range encapsulates this range
		}
		def overlaps(dr: DRange): Boolean = overlaps(dr.from, dr.to)

		def length = TimeHelpers.TimeSpan(to.getMillis - from.getMillis)

		def >(t: TimeHelpers#TimeSpan): Boolean = length > t
		def <(t: TimeHelpers#TimeSpan): Boolean = length < t
		def eq(t: TimeHelpers#TimeSpan): Boolean = length equals t

		def ==(r: DRange): Boolean = r.from.equals(from) && r.to.equals(to)

		override def toString = Range.toString(this, fullDateTime)
	}
}