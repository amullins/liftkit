package net.amullins.liftkit.common

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers.{tryo, today}
import net.liftweb.http._
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds._

import scala.xml.NodeSeq

import org.joda.time.LocalTime

import net.amullins.liftkit.common.date.JodaDateHelpers._


object TimeSelects {
	def apply(): TimeSelects = new TimeSelects {}
}


trait TimeSelects {

	val _7am = new LocalTime(7, 0, 0, 0)
	val _715am = new LocalTime(7, 15, 0, 0)

	private val startTimes =
		times(new LocalTime(today.getTimeInMillis)).map(t => (t, time12.print(t)))
	val selectedStartTime = ValueCell(_7am)

	private var _endLocalTimes = times.from(selectedStartTime.get)
	private val endTimes = selectedStartTime.lift((start: LocalTime) => {
		_endLocalTimes = times.from(start)
		_endLocalTimes.map((t: LocalTime) => {
			(t.getMillisOfDay.toString, time12.print(t))
		})
	})
	val selectedEndTime = ValueCell(_endLocalTimes(0))

	def startTimeSelect(initialValue: Box[LocalTime] = Full(selectedStartTime.get)) = {
		for (t <- initialValue) selectedStartTime.set(t)

		SHtml.ajaxSelectObj[LocalTime](startTimes, initialValue, (t: LocalTime) => {
			selectedStartTime.set(t)
			Noop
		})
	}

	def endTimeSelect(in: NodeSeq, initialValue: Box[LocalTime], callback: JsCmd, attrs: (String, String)*) = {
		import scala.xml._

		initialValue.map(selectedEndTime.set)

		WiringUI.toNode(in, endTimes, {(id, firstRender_?, cmd) => cmd & callback })((times, ns) => {
			val deflt = times.headOption.fold("")(_._2)

			S.fmapFunc(S.SFuncHolder((timeMillisString: String) => {
				(for (t <- tryo(new LocalTime().withMillisOfDay(timeMillisString.toInt))) yield {
					selectedEndTime.set(t)
				}) openOr {
					S.error("Invalid End Time")
				}
			}))(funcName => {
				attrs.foldLeft(
					<select name={funcName}>{
						times.flatMap {
							case (value, text) => <option value={value}>{text}</option> % (
								if (text == deflt) new UnprefixedAttribute("selected", "selected", Null)
								else Null
							)
					}}</select>
				) {
					case (el, (k, v)) => el % new UnprefixedAttribute(k, v, Null)
				}
			})

		})
	}

}