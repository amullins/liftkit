package net.amullins.liftkit.common.date

import net.liftweb.common._
import net.liftweb.util.Helpers._

import java.text.SimpleDateFormat
import java.util.{Date, Calendar, Locale, TimeZone}
import java.util.Calendar._

import org.joda.time._


object JavaDateHelpers extends JavaDateHelpers with Logger


trait JavaDateHelpers
	extends ReadableDate[Date]
	with DateFormats[SimpleDateFormat]
	with TimeSelectValues[Calendar, Date]
	with DateParser[Date, TimeZone]
	with AsTimestamp[Date]
	with SameDay[Date]
{

  def currentTimeZone = Helpers.CurrentTimeZone.get.map(_.timezone.asTimeZone) openOr TimeZone.getDefault


	val readable = new Readable {
		def apply(d: Date): String = readableDate.format(d)
		def withSuffix(d: Date): String = apply(d) + Helpers.daySuffix(new DateTime(d.getTime).getDayOfMonth)
	}


	def fullDate = new SimpleDateFormat("MM/dd/yyyy")
	def partialDate = new SimpleDateFormat("MM/dd")
	def monthAbbr = new SimpleDateFormat("MMM")
	def hour = new SimpleDateFormat("h")
	def minute = new SimpleDateFormat("mm")
	def meridiem = new SimpleDateFormat("a")
	def time = new SimpleDateFormat("hh:mm a")
	def time12 = new SimpleDateFormat("h:mm a")
	def fullDateTime = new SimpleDateFormat("MM/dd/yyyy h:mm a")
	def partialDateTime = new SimpleDateFormat("M/dd h:mm a")
	def rfc2822 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z")
	def readableDate = new SimpleDateFormat("MMMM d")
	def fullDateShortYear = new SimpleDateFormat("MM/dd/yy")
	def compactDate = new SimpleDateFormat("M/d")
	def compactDateTime = new SimpleDateFormat("M/dd/yy h:mm a")
	def rfc1123 = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'", Locale.US)
		rfc1123.setTimeZone(TimeZone.getTimeZone("UTC"))


	object times extends Times {
		def apply(
			calendar: Calendar,
		  span: Long = 24.hours.millis,
		  increment: Long = 15.minutes.millis
		): Seq[Date] = {
			val rows: Int = (span / increment).toInt
			(1 to rows).map((v: Int) => {
				val item = calendar.getTime
				calendar.add(MILLISECOND, increment.toInt)
				item
			})
		}

		// time select values from 15 minutes after startTime
		def from(startTime: Date): Seq[Date] = {
			val start = Calendar.getInstance()
				start.setTime(startTime)
				start.add(Calendar.MINUTE, 15)
			val end = Calendar.getInstance()
				end.set(Calendar.DATE, start.get(Calendar.DATE) + 1)
				end.set(Calendar.HOUR_OF_DAY, 0)
				end.set(Calendar.MINUTE, 0)
				end.set(Calendar.SECOND, 0)
				end.set(Calendar.MILLISECOND, 0)
			apply(start, end.getTimeInMillis - startTime.getTime)
		}
	}


	def parseDate(d: String): Box[Date] = tryo { fullDate.parse(d) }
	def parseDate(tz: TimeZone, d: String) = tryo {
		val f = fullDate
		f.setTimeZone(tz)
		f.parse(d)
	}


	// compose a Calendar object from 2 date objects (one for date and one for time)
	def composeCalendar(date: Date, time: Date): Calendar = {
		val dateCal = Calendar.getInstance
		dateCal.setTime(date)
		val resultCal = Calendar.getInstance
		resultCal.setTime(time)
		resultCal.set(YEAR, dateCal.get(YEAR))
		resultCal.set(MONTH, dateCal.get(MONTH))
		resultCal.set(DATE, dateCal.get(DATE))

		resultCal
	}


	val gmtTimeZone = java.util.TimeZone.getTimeZone("GMT")
	def toGMT(d: Date, f: SimpleDateFormat): String = {
		f.setCalendar(Calendar.getInstance(gmtTimeZone))
		f.format(d)
	}


	def getTimeZone(d: Date): java.util.TimeZone = new DateTime(d.getTime).getZone.toTimeZone


	def asTimestamp(d: Date): Long = d.getTime / 1000


	def sameDay_?(d1: Date, d2: Date): Boolean =
		JodaDateHelpers.sameDay_?(new DateTime(d1), new DateTime(d2))

}