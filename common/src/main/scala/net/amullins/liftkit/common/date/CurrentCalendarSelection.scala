package net.amullins.liftkit.common.date

import org.joda.time.{LocalDateTime => JodaLocalDT}


case class CurrentCalendarSelection(startMillis: Long, endMillis: Long) {
	lazy val start = new JodaLocalDT(startMillis).toDateTime(Helpers.joda.currentTimeZone)
	lazy val end = new JodaLocalDT(endMillis).toDateTime(Helpers.joda.currentTimeZone)
}