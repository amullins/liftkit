package net.amullins.liftkit.common

import org.scalatest._

import java.net.URI


class UrlHelpersSpec extends CommonUnitSpec {
	import net.amullins.liftkit.common.UrlHelpers._

	val baseUrl = "http://www.testing.com/"
	val urlParams1 = Map( "test" -> List("", "1", "3 ") )
	val urlParams2 = Seq(("test", ""), ("test", "1"), ("test", "3 "))

	// params tests
	"params" should "be empty when there are no parameters in the uri" in {
		params(new URI(baseUrl)) should have size 0
		params(new URI(baseUrl + "?")) should have size 0

		params("http://www.testing.com") should have size 0
		params(baseUrl + "?") should have size 0
	}

	it should "not be empty when there are parameters in the uri" in {
		params(new URI(baseUrl + "?test=1")).size should be > 0
		params(new URI(baseUrl + "?test=")).size should be > 0

		params(baseUrl + "?test=1").size should be > 0
		params(baseUrl + "?test=").size should be > 0
		params(baseUrl + "?test=&test=1").size should be (1)
		params(baseUrl + "?test=1&test2=2").size should be (2)

		params(baseUrl + "?test=&test=1").get("test").map(_.length).getOrElse(0) should be (2)
	}


	// query string tests
	"buildQueryString" should "result in a query string with parameters when parameters are present" in {
		buildQueryString(urlParams1) should be ("test=&test=1&test=3+")
		buildQueryString(urlParams2 : _*) should be ("test=&test=1&test=3+")
	}

	it should "result in an empty string when there are no parameters" in {
		assert(buildQueryString(Map.empty[String, List[String]]).isEmpty)
		assert(buildQueryString().isEmpty)
	}

	"encodeQueryString" should "only encode the query string portion of a url" in {
		val testUrl1 = "http://www.testing.com/?test=1&test= 2&test=123 4%"
		val testUrl2 = "http://www.testing.com/?test=1&test= 2&test=%#don't encode this"
		encodeQueryString(testUrl1) should be ("http://www.testing.com/?test=1&test=+2&test=123+4%25")
		encodeQueryString(testUrl2) should be ("http://www.testing.com/?test=1&test=+2&test=%25#don't encode this")
	}

}
