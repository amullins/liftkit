package net.amullins.liftkit.io

import net.liftweb.common._
import net.liftweb.util.Helpers.tryo

import java.io.{InputStream, ByteArrayInputStream, ByteArrayOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import com.google.common.io.ByteStreams


private[io] final class GZipper {

  def compress(input: String, charset: String = "UTF-8"): Array[Byte] = compress(new ByteArrayInputStream(input.getBytes(charset)))
  def compress(input: Array[Byte]): Array[Byte] = compress(new ByteArrayInputStream(input))
  def compress(is: InputStream): Array[Byte] = {
  	var gzos: GZIPOutputStream = null
  	try {
      trace("Compressing input.")
  		val baos = new ByteArrayOutputStream()
  		gzos = new GZIPOutputStream(baos)
  		ByteStreams.copy(is, gzos)
      trace("Finished compressing input.")
  		gzos.finish()
  		baos.toByteArray
  	} finally {
  		tryo { if (gzos != null) gzos.close() }
  	}
  }

  def decompress(input: String, charset: String = "UTF-8"): Array[Byte] = decompress(new ByteArrayInputStream(input.getBytes(charset)))
  def decompress(input: Array[Byte]): Array[Byte] = decompress(new ByteArrayInputStream(input))
  def decompress(is: InputStream): Array[Byte] = {
  	var gzis: GZIPInputStream = null
  	try {
      trace("Decompressing input.")
  		val baos = new ByteArrayOutputStream()
  		gzis = new GZIPInputStream(is)
  		ByteStreams.copy(gzis, baos)
      trace("Finished decompressing input.")
  		baos.toByteArray
  	} finally {
  		tryo { if (gzis != null) gzis.close() }
  	}
  }

}
