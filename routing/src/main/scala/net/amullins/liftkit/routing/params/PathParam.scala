package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.http._
import net.liftweb.sitemap._

import scala.reflect.runtime.universe._


/**
 * Abstract away the complexity of boxing/unboxing param values for locations that should be
 * available for things like autologin. By default, a location is undefined if its
 * value cannot be determined. When a user is not logged in, the param's value may not be available
 * because we use the current user's session information for data retrieval. We work around this by
 * providing a simplified data lookup for non-logged in users and then authenticate the
 * data once the user has logged in.
 *
 * The flip-side to this is that there is an additional method that must be included
 * on the snippets that use the location's parameter(s).
 * @see [[net.amullins.liftkit.routing.params.PathParamAuth]]
 *
 * @param func Function for retrieving the value of the Param based on the current
 *             user's authentication status
 * @param auth Check that user has access to the param value.
 */
case class PathParam[ParamType <: Any](
	func: PartialFunction[Boolean, Box[ParamType]], // boolean value represents a user's authentication status (true if logged in)
	auth: ParamType => Boolean
)(implicit val paramTypeTag: TypeTag[ParamType]) {

	/**
	 * This is the *actual* value of the param
	 */
	val value: Box[ParamType] =
    LiftRules.loggedInTest.map(loggedIn_? => func(loggedIn_?())) openOr Empty

	private var cached = false
	private var canView_? = false

	/**
	 * Authenticate the value and cache the result
	 */
	private def _auth() = {
	  if (!cached) {
		  cached = true
		  canView_? = value match {
			  case Full(v) => auth(v)
			  case _ => false
		  }
	  }
	  canView_?
	}


	/**
	 * Performs authentication on the value according to the provided `auth` function
	 * @return The boxed value Full, if accessible | Empty, if not
	 */
	def get_? : Box[ParamType] = if (_auth()) value else Empty


	/**
	 * This is for use with Menu.param(s) `parser`
	 * If the value is NOT Full, the location will be unavailable
	 */
	def boxed: Box[Box[PathParam[ParamType]]] = {
	  value match {
		  case Full(_) => Full(Full(this))
		  case _ => Empty
	  }
	}

}
