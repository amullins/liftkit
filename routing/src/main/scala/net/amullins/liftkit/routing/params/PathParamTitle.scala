package net.amullins.liftkit.routing.params

import net.liftweb.common._

import scala.xml._


case class PathParamTitle[PT, T <: Box[PathParam[PT]]](calcTitle: PT => String)(implicit ptm: Manifest[PT])
	extends LocPathParam[PT, T]
{
	def get: NodeSeq =
		currentValueAsParamType.map(pt => Text(calcTitle(pt))) openOr NodeSeq.Empty
}