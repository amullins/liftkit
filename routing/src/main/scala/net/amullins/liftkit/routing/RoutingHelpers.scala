package net.amullins.liftkit.routing

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.sitemap._

import scala.xml._

import params._
import net.amullins.liftkit.common.UrlHelpers


object RoutingHelpers extends RoutingHelpers


trait RoutingHelpers {

	/**
	 * Redirect a user to a Loc via Loc name. Optionally, execute a function after redirection.
	 * @param locName The Loc name.
	 * @param func A function to be executed after redirection.
	 */
	def redirect(locName: String, func: () => Unit = () => {}): Nothing = {
		val locHref = UrlHelpers.href(locName)
		S.redirectTo(locHref openOr "/", () => {
			if (locHref.isEmpty) S.error("The page you requested is unavailable.")
			func()
		})
	}

  def redirectToLoc(loc: Loc[_], func: () => Unit = () => {}): Nothing =
    S.redirectTo(locLinkParts(loc)._1, func)

	/**
	 * Refresh the current page (non-js).
	 * @param func A function to be executed after refresh.
	 */
	def refresh(func: () => Unit = () => {}): Nothing = {
		val currentHref = UrlHelpers.href
		S.redirectTo(currentHref openOr "/", () => {
			if (currentHref.isEmpty) S.error("That page you requested is unavailable.")
			func()
		})
	}

	/**
	 * Retrieve URL and Text to be used when creating a link to a Loc.
	 * @param loc The Loc we're going to link to.
	 * @return A tuple containing the URL and Text for creating a link.
	 */
	def locLinkParts(loc: Loc[_]): (String, NodeSeq) = {
    val defaultLinkParts = ("", loc.title)
    loc.params.collectFirst {
			case paramLink: PathParamLink[_, _] =>
        paramLink.createDefaultLink.map(d => (
					d.text,
					loc.params.collectFirst { case pt: PathParamTitle[_, _] => pt.get } getOrElse loc.title
				)) openOr defaultLinkParts
		} getOrElse {
			loc.createDefaultLink.map(ns => (ns.text, loc.title)) getOrElse defaultLinkParts
		}
	}

  def locLinkParts(locName: String): Box[(String, NodeSeq)] =
    SiteMap.findAndTestLoc(locName).map(locLinkParts)

}
