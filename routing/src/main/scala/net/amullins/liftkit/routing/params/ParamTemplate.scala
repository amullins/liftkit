package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.sitemap._
import net.liftweb.http._

import scala.xml._


object ParamTemplate {

	def apply[PT](
    menuable: () => { def toLoc: Loc[Box[PathParam[PT]]] },
    func: Box[PT] => List[String]
  ): Loc.Template =
		Loc.Template(() => {
			func(paramValue(menuable())) match {
				case Nil => NodeSeq.Empty
				case lst => Templates.apply(lst) openOr NodeSeq.Empty
			}
		})


	private def paramValue[PT](
    menuable: { def toLoc: Loc[Box[PathParam[PT]]] }
  ) =
		for {
			boxed <- menuable.toLoc.currentValue
			paramValue <- boxed.flatMap(_.value)
		} yield paramValue

}
