package net.amullins.liftkit.routing

import net.liftweb.http._


object RefreshResponse extends Factory {

  val locationNotFoundMessage =
    new FactoryMaker[String]("An error occurred while processing your request. Please try again."){}

}


case class RefreshResponse()
  extends LiftResponse
{

  private val message = RefreshResponse.locationNotFoundMessage.vend

  def toResponse =
    S.location.flatMap(_.createDefaultLink.map(l => RedirectResponse(l.text).toResponse)) openOr
      RedirectWithState("/", MessageState(message -> NoticeType.Warning)).toResponse

}
