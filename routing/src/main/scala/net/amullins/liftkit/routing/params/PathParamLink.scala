package net.amullins.liftkit.routing.params

import net.liftweb.common._

import scala.xml._
import scala.reflect.runtime.universe.TypeTag


object PathParamLink {

	def apply[ParamType: TypeTag](v: ParamType): Box[PathParam[ParamType]] =
		Full(PathParam({case _ => Full(v)}, (v: ParamType) => true))

}

case class PathParamLink[PT, T <: Box[PathParam[PT]]](calcUrl: PT => String)(implicit ptm: Manifest[PT])
	extends LocPathParam[PT, T]
{

	def createDefaultLink: Box[NodeSeq] =
		currentValueAsParamType.map(pt => Text(calcUrl(pt)))

}