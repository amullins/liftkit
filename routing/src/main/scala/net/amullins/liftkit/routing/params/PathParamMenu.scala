package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.sitemap._

import scala.xml._


object PathParamMenu {
	def apply[PT](
		name: String,
		path: List[LocPath],
		pathParam: String => PathParam[PT],
		encoder: PathParam[PT] => String,
		title: PT => String,
		link: PT => String,
		params: List[Loc.LocParam[Box[PathParam[PT]]]] = Nil,
		submenus: List[ConvertableToMenu] = Nil
	)(
		implicit ptm: Manifest[PT]
	): PathParamMenuable[PT] =
		new PathParamMenuable[PT](name, path, pathParam, encoder, title, link, params, submenus)
}

class PathParamMenuable[PT](
	name: String,
	path: List[LocPath],
	pathParam: String => PathParam[PT],
	encoder: PathParam[PT] => String,
	title: PT => String,
	link: PT => String,
	params: List[Loc.LocParam[Box[PathParam[PT]]]] = Nil,
	submenus: List[ConvertableToMenu] = Nil
)(
	implicit val ptm: Manifest[PT]
) extends Menu.ParamMenuable[Box[PathParam[PT]]](
	name,
	name,
	(str: String) => pathParam(str).boxed,
	(bOfPP: Box[PathParam[PT]]) => bOfPP.map(encoder) openOr "",
	path,
	headMatch = false,
	params ::: List[Loc.LocParam[Box[PathParam[PT]]]](PathParamTitle(title)(ptm), PathParamLink(link)(ptm)),
	submenus
) with Menu.WithSlash with PathParamStuffs[PT] {
	def title(v: PT) = Text(title.apply(v))
	def url(v: PT) = link.apply(v)
}