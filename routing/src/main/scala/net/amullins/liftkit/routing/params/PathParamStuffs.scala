package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.sitemap._

import scala.xml._


trait PathParamStuffs[PT] {
	self: { def toLoc: Loc[Box[PathParam[PT]]]  } =>

	def title(v: PT): NodeSeq

	def url(v: PT): String

	def linkParts(v: PT) = {
		val loc = this.toLoc
		val defaultLinkParts = ("", loc.title)
		loc.params.collectFirst {
			case paramLink: PathParamLink[_, _] =>
				paramLink.createDefaultLink.map(d => (
					d.text,
					loc.params.collectFirst { case pt: PathParamTitle[_, _] => pt.get } getOrElse loc.title
				)) openOr defaultLinkParts
		} getOrElse {
			loc.createDefaultLink.map(ns => (ns.text, loc.title)) getOrElse defaultLinkParts
		}
	}
}