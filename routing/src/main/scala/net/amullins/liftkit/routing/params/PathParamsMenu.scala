package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.sitemap._

import scala.xml._


object PathParamsMenu {
	def apply[PT](
		name: String,
		path: List[LocPath],
		pathParams: List[String] => PathParam[PT],
		encoder: PathParam[PT] => List[String],
		title: PT => String,
		link: PT => String,
		params: List[Loc.LocParam[Box[PathParam[PT]]]] = Nil,
		submenus: List[ConvertableToMenu] = Nil
	)(
		implicit ptm: Manifest[PT]
	): PathParamsMenuable[PT] =
		new PathParamsMenuable[PT](name, path, pathParams, encoder, title, link, params, submenus)
}

class PathParamsMenuable[PT](
	name: String,
	path: List[LocPath],
	pathParams: List[String] => PathParam[PT],
	encoder: PathParam[PT] => List[String],
	title: PT => String,
	link: PT => String,
	params: List[Loc.LocParam[Box[PathParam[PT]]]] = Nil,
	submenus: List[ConvertableToMenu] = Nil
)(
	implicit val ptm: Manifest[PT]
) extends Menu.ParamsMenuable[Box[PathParam[PT]]](
	name,
	name,
	(strings: List[String]) => pathParams(strings).boxed,
	(bOfPP: Box[PathParam[PT]]) => bOfPP.map(encoder) openOr Nil,
	path,
	headMatch = false,
	params ::: List[Loc.LocParam[Box[PathParam[PT]]]](PathParamTitle(title)(ptm), PathParamLink(link)(ptm)),
	submenus
) with Menu.WithSlash with PathParamStuffs[PT] {
	def title(v: PT) = Text(title.apply(v))
	def url(v: PT) = link.apply(v)
}