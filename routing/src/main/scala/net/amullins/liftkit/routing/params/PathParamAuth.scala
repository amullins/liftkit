package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.http.S
import net.liftweb.sitemap._

import scala.reflect.runtime.universe.{TypeTag, typeTag}

import net.amullins.liftkit.routing.RoutingHelpers


object PathParamAuth {

	def paramValueOrElse[ParamType: TypeTag](func: => ParamType) =
		(for (loc <- S.location; value <- loc.currentValue) yield {
			value match {
				case Full(pp: PathParam[_]) if pp.paramTypeTag.tpe =:= typeTag[ParamType].tpe =>
					pp.asInstanceOf[PathParam[ParamType]].get_? openOr func
				case Full(pp: PathParam[_]) => func
				case x => func
			}
		}) openOr func

}

abstract class PathParamAuth[ParamType <: AnyRef](
	val menuable: { def toLoc: Loc[Box[PathParam[ParamType]]] }
) {

	protected def paramValue: ParamType =
		(for {
			param <- menuable.toLoc.currentValue.flatMap(x=>x).headOption
			_value <- param.get_?
		} yield _value) getOrElse {
			RoutingHelpers.redirect("Login", () => S.error(S ? "must.be.logged.in"))
		}

}
