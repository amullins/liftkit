package net.amullins.liftkit.routing.params

import net.liftweb.common._
import net.liftweb.util.Helpers.tryo
import net.liftweb.http.S
import net.liftweb.sitemap.Loc


abstract class LocPathParam[ParamType, T <: Box[PathParam[ParamType]]](
	implicit ptm: Manifest[ParamType]
)
	extends Loc.LocParam[T]
{
	/**
	 * This is an insanely ugly method. But, it works for our use-case.
	 * We need to retrieve values for Locs that are in the breadcrumb nav for a particular Loc. To do so, we have to
	 * extract values for those Locs that have PathParams *from* the current Loc's value (since it's the only value
	 * that's available). Extracting single values is easily done with pattern-matching. Extracting the multi-param
	 * values is more difficult.
	 *
	 * @return Returns the value(s) needed to construct links/titles for Locs
	 *         that are in the current Loc's path (breadcrumbs).
	 */
	def currentValueAsParamType: Box[ParamType] = {
		(for { loc <- S.location; value <- loc.currentValue } yield {
			value match {
				case Full(pp: PathParam[_]) =>
					pp.get_?.map {
						case pt: ParamType => Full(pt)
						case p: Product =>
							// if the Loc's ParamType is a subclass of Product (a tuple)
							if (classOf[Product].isAssignableFrom(ptm.runtimeClass)) {
								val args: List[Object] = ptm.typeArguments.flatMap(argManifest => {
									p.productIterator.find(_.asInstanceOf[AnyRef].getClass == argManifest.runtimeClass).flatMap {
										case o: Object => Full(o)
										case _ => Empty
									}
								})
								tryo {
									val constructor = ptm.runtimeClass.getConstructors()(0)
									constructor.newInstance(args : _*).asInstanceOf[ParamType]
								}
							} else {
								p.productIterator.collectFirst {
									case pt: ParamType => Full(pt)
								} getOrElse Empty
							}
						case _ =>
							Empty
					} openOr Empty

				case _ => Empty
			}
		}) openOr Empty
	}
}