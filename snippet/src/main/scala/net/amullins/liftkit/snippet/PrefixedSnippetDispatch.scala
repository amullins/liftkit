package net.amullins.liftkit.snippet

import net.liftweb.common._
import net.liftweb.http.DispatchSnippet

import scala.collection.mutable


object PrefixedSnippetDispatch extends Logger {
	val delimiter = "|" // don't use colon or period

	private val _dispatchOn = mutable.ListBuffer[PrefixedSnippetDispatch]()

	def append(these: PrefixedSnippetDispatch*) {
		_dispatchOn ++= these
	}

	def unapply(str: String): Option[DispatchSnippet] = {
		lazy val test = new {
			def unapply(d: PrefixedSnippetDispatch): Option[() => DispatchSnippet] = d.unapply(str)
		}

		// limit impact on snippet matching where snippet name doesn't include the delimiter
		if (str.indexOf(delimiter) == -1) None
		else _dispatchOn.collectFirst { case test(vend) => vend() }
	}
}


class PrefixedSnippetDispatch(val prefix: SnippetPrefix, pf: PartialFunction[String, DispatchSnippet]) {
	def matchRules_? = prefix.rules.filterNot(_.apply()).isEmpty

	def unapply(str: String): Option[() => DispatchSnippet] =
		if (matchRules_?) {
			if (pf.isDefinedAt(str)) Some(() => pf(str))
			else None
		} else {
			if (pf.isDefinedAt(str)) Some(() => ClearingSnippet)
			else None
		}
}