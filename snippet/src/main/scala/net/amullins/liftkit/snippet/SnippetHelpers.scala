package net.amullins.liftkit.snippet

import net.liftweb.common._
import net.liftweb.http._

import scala.xml._
import scala.collection.mutable


trait DynamicDispatchSnippet extends DispatchSnippet with SnippetHelpers {
  def dispatch = DispatchBuilder.dispatch
}


trait DynamicStatefulSnippet extends StatefulSnippet with SnippetHelpers {
  def dispatch = DispatchBuilder.dispatch
}


object SnippetHelpers extends SnippetHelpers

trait SnippetHelpers {

	protected val snippetParams = mutable.ListBuffer[SnippetParam[_]]()
	def appendParam(p: SnippetParam[_]) {
		snippetParams += p
	}

	object DispatchBuilder extends DispatchBuilderInst

	implicit def snippetParamToValue[T](param: SnippetParam[T]): T = param.value

}


case class SnippetParam[PT <: Any](
	snippet: SnippetHelpers,
	name: String,
	defaultValue: PT,
	convert: String => Box[PT]
) {
	snippet.appendParam(this)

	def value: PT = (S.attr(name) or S.param(name)).flatMap(convert) openOr defaultValue
}


trait DispatchBuilderInst {
	type DispatchIt = PartialFunction[String, NodeSeq => NodeSeq]

	private val _dispatch = mutable.ListBuffer[DispatchIt]()

	def dispatch = _dispatch.reduceLeft(_ orElse _)

	def append(pf: DispatchIt): DispatchBuilderInst = {
		_dispatch += pf
		this
	}

  def appendFunc(name: String)(func: NodeSeq => NodeSeq): DispatchBuilderInst = {
    _dispatch += { case `name` => func }
    this
  }
}
