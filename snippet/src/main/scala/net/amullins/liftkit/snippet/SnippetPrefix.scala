package net.amullins.liftkit.snippet

import net.liftweb.http.DispatchSnippet

import net.amullins.liftkit.common.CustomEnum


trait SnippetPrefix {

  def name: String
  def rules: Seq[() => Boolean]

  def prefix(snippetName: String) =
  		name + PrefixedSnippetDispatch.delimiter + snippetName

  	def unprefix(fullName: String) =
  		fullName.replaceFirst("%s\\%s".format(name, PrefixedSnippetDispatch.delimiter), "")

  	def apply(snippetName: String, vendSnippet: () => DispatchSnippet): PrefixedSnippetDispatch = {
  		val fullName = prefix(snippetName)
  		new PrefixedSnippetDispatch(this, {
  			case `fullName` => vendSnippet()
  		})
  	}

  	def apply(pf: PartialFunction[String, DispatchSnippet]): PrefixedSnippetDispatch = {
  		new PrefixedSnippetDispatch(this, {
  			case fullName if pf.isDefinedAt(unprefix(fullName)) => pf(unprefix(fullName))
  			case fullName if pf.isDefinedAt("_") => pf("_")
  			case fullName if pf.isDefinedAt(fullName) =>
  				PrefixedSnippetDispatch.warn("Prefixed snippet matched on full name (should match unprefixed name).")
  				pf(fullName)
  		})
  	}

}
