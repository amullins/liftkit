package net.amullins.liftkit.snippet

import net.liftweb.util.ClearNodes
import net.liftweb.http.DispatchSnippet


/**
 * DispatchSnippet that clears everything that's sent in (no questions asked)
 */
object ClearingSnippet extends DispatchSnippet {
	def dispatch = { case _ => ClearNodes }
}