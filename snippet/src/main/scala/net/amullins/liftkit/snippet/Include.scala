package net.amullins.liftkit.snippet

import net.liftweb.common._
import net.liftweb.util.{Schedule => _, _}
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.builtin.snippet.WithParamVar

import scala.xml._


/**
 * An alternative to Lift's builtin Surround snippet.
 * Surround by template name OR path
 * Optionally keep the placeholder element (the element with the id matching the `at` param)
 *
 * @see [[net.amullins.liftkit.snippet.TemplateResolution]]
 */
object Include extends DispatchSnippet with Logger {

  def dispatch = {
    case _ => render _
  }

  private def render(kids: NodeSeq) : NodeSeq = {
	  (for (sess <- S.session if S.request.isDefined) yield {
	    WithParamVar.doWith(Map()) {
		    val ns = {
			    val _ns = sess.processSurroundAndInclude(PageName.get, kids)
		      if (S.attr("noreplace").isDefined || S.attr("replace") === "false") _ns
		      else NodeSeq.fromSeq(_ns.head.child)
		    }
	      val mainParam = (S.attr("at") openOr "main", ns)
				findTemplate(WithParamVar.get + mainParam)
	    }
	  }) match {
	    case Full(x) => x
	    case Empty => error("Either the session or the request is invalid."); NodeSeq.Empty
	    case Failure(msg, _, _) => error(msg); NodeSeq.Empty
	  }
  }

	private def findTemplate(atWhat: Map[String, NodeSeq]) = {
		val templatePath =
			S.attr("what").map(name =>
				TemplateResolution.get(name) match {
					case Nil => (if (name.startsWith("/")) name else "/" + name).split("/").toList
					case xs => xs
				}
			) openOr Nil

		(
			if (templatePath.isEmpty) Failure("Template not provided. Parameter missing.")
			else Templates(templatePath, S.locale)
		) match {
		  case f @ Failure(msg, _, _) if Props.devMode => error(msg); NodeSeq.Empty

		  case Full(ns) =>
			  atWhat.toList match {
			    case Nil => ns
			    case xs =>
				    xs.map { case (id, replacement) => ("#" + id) #> replacement }.reduceLeft(_ & _).apply(ns)
			  }

		  case _ =>
			  atWhat.valuesIterator.toSeq.flatMap(_.toSeq).toList
		}
	}
}


