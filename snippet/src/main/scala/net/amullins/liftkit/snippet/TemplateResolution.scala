package net.amullins.liftkit.snippet

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._

import scala.xml.NodeSeq
import scala.collection.mutable


/**
 * Context-aware template resolution.
 * Also provides guarded resolution.
 *
 * {{{
 *   object Authenticated extends AppContext("authenticatedContext")
 *
 *   // to use contexts, set CurrentAppContext when the context changes
 *   CurrentAppContext.set(Authenticated)
 *
 *   TemplateResolution append (
 *     TemplateMapping("home", "templates-hidden" / "home-authenticated", Authenticated),
 *     TemplateMapping("home", "templates-hidden" / "home-default"),
 *
 *     TemplateMapping("dashboard", "template-hidden" / "dashboard", () => auth.isLoggedIn)
 *   )
 *
 *   // in your html
 *   <div data-lift="include?what=home;at=page"></div>
 * }}}
 */
object TemplateResolution extends Logger {

  private val _templateMappings = mutable.HashMap[String, TemplateMapping]()
  private val _templateNameUUID = mutable.HashMap[String, String]()
  
	def get(name: String): List[String] = {
    val qualifiedName = name + ":" + CurrentAppContext.get.name
    var possibles = _templateNameUUID.collect { case (uuid, `qualifiedName`) => uuid }.toList

    if (possibles.isEmpty) {
      val qualifiedWithAny = name + ":" + AnyAppContext.name
      possibles = _templateNameUUID.collect { case (uuid, `qualifiedWithAny`) => uuid }.toList
    }

    possibles.flatMap { case uuid =>
      _templateMappings.get(uuid).filter(_.isValid())
    } match {
      case Nil => debug("Template not found."); Nil
      case mapping :: Nil => mapping.path
      case multiple => debug("Found multiple matching templates. Using the first matched template."); multiple.head.path
    }
  }

  def append(mappings: TemplateMapping*) {
    _templateMappings ++= mappings map { m =>
      val uuid = randomString(12)
      _templateNameUUID += ((uuid, m.name + ":" + m.context.name))
      (uuid, m)
    }
  }

}


case class TemplateMapping(name: String, path: List[String], context: AppContext = AnyAppContext) {
  val isValid = () => true
}

object TemplateMapping {
  def apply(name: String, path: List[String], guard: () => Boolean): TemplateMapping =
    new TemplateMapping(name, path) {
      override val isValid = guard
    }
}


object CurrentAppContext extends SessionVar[AppContext](AnyAppContext)

case class AppContext(name: String)
object AnyAppContext extends AppContext("anyContext")