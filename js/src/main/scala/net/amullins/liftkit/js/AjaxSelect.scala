package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.json.JsonAST._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad
import net.liftweb.http.S

import scala.xml._
import scala.collection.mutable
import scala.reflect.runtime.universe._


object AjaxSelect {

	def apply[S: TypeTag](
		placeholder: Box[String],
		selected: S,
		onSearch: (String, Int) => JValue, // [{value:"",label:""}] => <option value="{value}">{label}</option>
		onSubmit: String => Unit,
		onSelect: Box[Any => JsCmd] = Empty
	) =
		new AjaxSelect(placeholder, selected, onSearch, onSubmit, onSelect)

}

class AjaxSelect[S: TypeTag](
	placeholder: Box[String],
	selected: S,
	onSearch: (String, Int) => JValue,
	onSubmit: String => Unit,
	onSelect: Box[Any => JsCmd]
) {

	val onSearchFunc = (ignore: String) => {
		JsonResponse(onSearch(S.param("term").openOr(""), limit))
	}


	// maximum number of returned results from search function
	def limit: Int = 15


	/**
	 * Options for chosen
	 */
	private var _options: mutable.Map[String, JsExp] = mutable.Map(
		"delay" -> Num(500),
		"minLength" -> Num(3)
	)
	def opt(key: String, value: JsExp): AjaxSelect[S] = {
		_options += new Tuple2(key, value)
		this
	}
	def opts(_options: (String, JsExp)*): AjaxSelect[S] = {
		_options.foreach(o => opt(o._1, o._2))
		this
	}
	private def options = JsObj(_options.toList:_*)


	/**
	 * Attributes for select
	 */
	private val _attrs: mutable.Map[String, String] = mutable.Map()
	private val _dataPlaceholder = placeholder.map(p => ("data-placeholder", p)).toList
	def attr(attributeName: String, attributeValue: String): AjaxSelect[S] = {
		_attrs += attributeName -> attributeValue
		this
	}
	private def attributes: List[(String, String)] = {
		val _attributes = (_attrs.toList ::: _dataPlaceholder) :+ "tabindex" -> "0"
		selected match {
			case s : List[_] => _attributes :+ ("multiple", "multiple")
			case _ => _attributes
		}
	}


	private val selectedValue: String =
		typeOf[S] match {
			case s if s =:= typeOf[Full[(String, String)]] =>
				selected.asInstanceOf[Full[(String, String)]].openOrThrowException("already type checked")._1

			case s if s =:= typeOf[List[(String, String)]] =>
				selected.asInstanceOf[List[(String, String)]].map(_._1).mkString(",")

			case _ => ""
		}


	// the url of the function to be executed for each search
	def ajaxUrl(funcName: String) =
		S.encodeURL(S.contextPath + "/" + LiftRules.liftPath + "/ajax?" + funcName + "=foo")


	def change(inputId: String, hiddenId: String): AnonFunc = {
		val doSomethingOnTheServer = SHtml.jsonCall(JsRaw("currentValue"), (args: JValue) => {
			(for (onSelectFunc <- onSelect) yield {
				args match {
					// single select
					case JString(str) =>
						onSelectFunc(str)

					// multi select
					case JArray(lst: List[JValue]) =>
						onSelectFunc(lst.flatMap {
							case JString(str) => Full(str)
							case _ => Empty
						})

					case _ => onSelectFunc(Empty)
				}
			}) openOr Noop
		})
		// set the hidden input's value (it's the value that is sent to the server onSubmit) and perform `jsonCall`
		AnonFunc("currentValue", JsRaw(
			"""$("#%s").val($("#%s").val()); %s; return false""".format(
				hiddenId,
				inputId,
				if (onSelect.isDefined) doSomethingOnTheServer._2.toJsCmd else ""
			)
		).cmd)
	}


	val selectedOptions: NodeSeq =
		typeOf[S] match {
			case s if s =:= typeOf[Full[(String, String)]] =>
				optionElem(selected.asInstanceOf[Full[(String, String)]].openOrThrowException("type checked"))

			case s if s =:= typeOf[List[_]] && selected.asInstanceOf[List[_]].nonEmpty =>
				selected.asInstanceOf[List[(String, String)]].map(optionElem).reduceLeft[NodeSeq](_ :+ _)

			case s if s =:= typeOf[List[_]] => NodeSeq.Empty

			case _ => <option></option>
		}


	private def optionElem(kv: (String, String)): Elem =
		<option value={kv._1} selected="selected">{kv._2}</option>


	def element = {

		S.fmapFunc(S.SFuncHolder(onSearchFunc)) { func =>
			val id = Helpers.nextFuncName

			// `hidden` is the id of the input whose value is sent to the `onSubmit` function
			S.fmapFunc(S.SFuncHolder(onSubmit)) { hidden =>

				val opts: JsObj =
					JsObj(
						"url" -> Str(ajaxUrl(func)),
						"change" -> change(id, hidden)
					) +* options

				val chosen: JsCmd =
					JsRaw("""$("#%s").ajaxChosen(%s)""".format(
						id,
						opts.toJsCmd
					)).cmd

				<span>{
					attributes.foldLeft(<select id={id} name={id}>{
						selectedOptions +:
						(if (attributes.exists(_._1 == "data-placeholder")) <option></option> else NodeSeq.Empty)
					}</select>)(_ % _) :+
					<input type="hidden" id={hidden} name={hidden} value={selectedValue} /> :+
					<tail>{Script(JqOnLoad(chosen & JsRaw("$('label[for=%s]')".format(id)).cmd))}</tail>
				}</span>
			}
		}

	}

}
