package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.JE._

import scala.xml._

import net.amullins.liftkit.common.HtmlHelpers


object JqConfirm {

	def apply(
		title: String,
		ns: NodeSeq,
		additionalButtons: NodeSeq,
		continue: (JqConfirm) => JsCmd,
		props: (String, JsExp)*
	): (JqConfirm, JsCmd) = {
		val confirm = new JqConfirm(title, ns, continue, c => Noop) {
			override val buttons: NodeSeq = (cancelButton ++ continueButton) ++ additionalButtons
		}
		(confirm, confirm.dialog(JsObj(props:_*)))
	}

	def apply(
		title: String,
		ns: NodeSeq,
		cancelContinueLabels: (String, String),
		continue: (JqConfirm) => JsCmd,
		props: (String, JsExp)*
	): (JqConfirm, JsCmd) = {
		val confirm = new JqConfirm(title, ns, continue, c => Noop) {
			override def cancelButtonLabel: String = cancelContinueLabels._1
			override def continueButtonLabel: String = cancelContinueLabels._2
		}
		(confirm, confirm.dialog(JsObj(props:_*)).cmd)
	}

	def apply(
		title: String,
		ns: NodeSeq,
		continue: (JqConfirm) => JsCmd,
		props: (String, JsExp)*
	): (JqConfirm, JsCmd) = {
		val confirm = new JqConfirm(title, ns, continue, c => Noop)
		(confirm, confirm.dialog(JsObj(props:_*)).cmd)
	}

	def alert(
		title: String,
		ns: NodeSeq,
		props: (String, JsExp)*
	): (JqConfirm, JsCmd) = {
		val confirm = new JqConfirm(title, ns, c => Noop, c => Noop) {
			override def continueButtonLabel = "Ok"
			override val buttons: NodeSeq = continueButton
		}
		(confirm, confirm.dialog(JsObj(props:_*)))
	}

	def alert(
		title: String,
		ns: NodeSeq,
		afterClose: JsCmd,
		props: (String, JsExp)*
	): (JqConfirm, JsCmd) = {
		val confirm = new JqConfirm(title, ns, c => afterClose, c => Noop) {
			override def continueButtonLabel = "Ok"
			override val buttons: NodeSeq = continueButton
			override def dialog(css: JsObj = JsObj()) = {
				_dialog = JqBlockUI(
					Empty,
					Text(title),
					unblock => {
						SHtml.ajaxForm(<div>{
							<p>{ns}</p> :+ <div class="dialog-buttons">{buttons}</div>
						}</div>, unblock)
					},
					confirmationCss +* css,
					afterClose
				)

				_dialog
			}
		}
		(confirm, confirm.dialog(JsObj(props:_*)))
	}

}

class JqConfirm(
	title: String,
	ns: NodeSeq,
	continue: (JqConfirm) => JsCmd,
	cancel: (JqConfirm) => JsCmd
) {
	protected var _dialog: JqBlockUI = null

	protected val confirmationCss = JsObj(
		"width" -> Num(352),
		"height" -> Str("auto"),
		"minHeight" -> Num(100)
	)

	def cancelButtonLabel: String = "Cancel"
	def cancelButton: NodeSeq = HtmlHelpers.form.ajaxSubmitButton(
		Text(cancelButtonLabel),
		close _,
		"class" -> "btn mediumBlue"
	)

	def continueButtonLabel: String = "Continue"
	def continueButton: NodeSeq = HtmlHelpers.form.ajaxSubmitButton(
		Text(continueButtonLabel),
		() => continue(this),
		"class" -> "btn mediumBlue",
		"style" -> "margin-left:10px;"
	)

	val buttons: NodeSeq = cancelButton ++ continueButton

	def close(): JsCmd = {
		_dialog.unblock.cmd & cancel(this)
	}

	def dialog(css: JsObj = JsObj()) = {
		_dialog = JqBlockUI(
			Empty,
			Text(title),
			unblock => {
				SHtml.ajaxForm(<div>{
					<p>{ns}</p> :+ <div class="dialog-buttons">{buttons}</div>
				}</div>, unblock)
			},
			confirmationCss +* css
		)

		_dialog
	}

}