package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds.{Script, Noop}

import scala.xml._


// opacity, in particular, must be set via `props` argument or via the defaults
case class JqBlockUI(
  toBlock: Box[String], // jquery selector
  title: NodeSeq,
  nsFunc: JsExp => NodeSeq, // the content of the dialog
	css: JsObj = JsObj(),
	onUnblock: JsCmd = Noop,
  props: JsObj = JsObj(),
	wrapperClass: String = ""
) extends JsHelpers {

	lazy val toJsCmd = {
		fixHtmlWithScripts(_html, (html, cmds) => {
			val _props = {
				val onBlockCmd = if (cmds.nonEmpty) cmds.reduceLeft(_&_) else Noop

				val transformed = props.props.map {
					case ("onBlock", f: AnonFunc) =>
						("onBlock", AnonFunc(f.applied.cmd & onBlockCmd))
					case p => p
				}

				val onBlock = transformed.find(_._1 == "onBlock") match {
					case Some(_) => Nil
					case _ => List("onBlock" -> AnonFunc(onBlockCmd))
				}

				JsObj(List("baseZ" -> Num(480), "fadeIn" -> Num(250), "fadeOut" -> Num(500)) ::: transformed ::: onBlock : _*)
			}

			val m: JsObj =
				JsObj("css" -> css) +* _props +* JsObj("message" -> JsRaw("$(%s)".format(html)))

			(toBlock match {
			  case Full(blockElement) => Call("$(%s).block".format(blockElement.encJs), m).cmd
			  case _ => Call("$.blockUI", m).cmd & JsRaw("$('body').addClass('has-dialog');").cmd
			}).toJsCmd
		})
  }

	private lazy val _html = {
		val cssClass =
			(if (title.isEmpty) "no-title" else "") +
			(if (wrapperClass.isEmpty) "" else " " + wrapperClass)

		(
		  ".dialog-wrapper [class+]" #> emptyForBlank(cssClass) andThen
		  ".dialog-title" #> <div class="dialog-title">{title}</div> &
		  ".dialog-content *" #> nsFunc(unblock) &
		  "script" #> Script(JsRaw("""(function($lm){
		    |  $('<div id="%s" style="display:none"></div>').insertBefore($lm);
		    |  $lm.empty().prependTo($('.blockMsg'))
		    |})($('#%s'))""".stripMargin.format(
			    LiftRules.noticesContainerId + "__ph",
			    LiftRules.noticesContainerId
			  )).cmd
		  )
		).apply(template)
	}

	private val template =
		<div class="blockMsgBg">
		  <div class="dialog-wrapper">
		    {closeButton}
		    <div class="dialog-title"></div>
		    <div class="dialog-content"></div>
			</div>
		</div>

	private def _onUnblock =
		AnonFunc("el, opts", onUnblock)

  def unblock: JsExp =
    toBlock match {
      case Full(blockElement) =>
	      JsRaw("""$(%s).unblock({ onUnblock : %s})""".format(
		      blockElement.encJs,
		      _onUnblock.toJsCmd
	      ))
      case _ =>
	      JsRaw("$('body').removeClass('has-dialog');$.unblockUI({ onUnblock : %s})".format(_onUnblock.toJsCmd))
    }

  def closeButton: NodeSeq =
    <div class="dialog-close-btn icon-remove" onclick={unblock.toJsCmd} title="Close"></div>

}
