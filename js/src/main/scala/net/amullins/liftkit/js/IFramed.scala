package net.amullins.liftkit.js

import net.liftweb.http.js._
import net.liftweb.http.js.JE._


trait IFramed {

	private val js = """(function(doc, $){
		var i = doc.createElement("iframe"), $t=$("%s");
		$t.hide().parent().append($(i).css({width:$t.outerWidth(),height:$t.outerHeight()}).attr("frameborder","0"));
		var d = i.contentWindow || i.contentDocument;
		if(d.document){d=d.document};d.open();d.write($t.val());d.close();
		try{d.execCommand("styleWithCSS",0,0)}catch(e){try{d.execCommand("useCSS",0,1)}catch(e){}}
		})(document, jQuery)"""

	def apply(sel: String): JsCmd = JsRaw(js.format(sel)).cmd

}
