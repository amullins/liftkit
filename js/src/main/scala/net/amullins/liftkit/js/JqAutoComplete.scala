package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.json._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad
import net.liftweb.http.S

import scala.xml._
import scala.collection.mutable

import net.amullins.liftkit.common.UrlHelpers


object JqAutoComplete {

	def apply(
		initial: Box[JObject],
		onSearch: (String, Int) => JValue,
		onSubmit: (String, Box[String]) => Any,
		onSelect: String => JsCmd // do something on the server when item is selected
	): JqAutoComplete =
		new JqAutoComplete(initial, onSearch, onSubmit, onSelect)

	case class OnOpen(func: String => AnonFunc) extends OptionFunc

	object MatchWidth extends OnOpen(inputId => {
		AnonFunc(JsRaw("""var $this = $(this); $this.autocomplete("widget").css("width", $this.outerWidth())""").cmd)
	})

	trait OptionFunc extends JsExp {
		def func: String => AnonFunc
		def toJsCmd(inputId: String): String = func(inputId).toJsCmd
		def toJsCmd: String = "function(){}"
	}

}

/**
 * @param props
 *   _1 = the value (sent to server)
 *   _2 = the label (visible in text input)
 *   _3 = the display value for the autocomplete list
 */
class JqAutoComplete(
	initial: Box[JObject],
	onSearch: (String, Int) => JValue,
	onSubmit: (String, Box[String]) => Any, // text input's value, hidden input's value (if the hidden input's value is empty - consider the text input's value a *new* value)
	onSelect: String => JsCmd, // do something on the server when item is selected
	props: (String, String, String) = ("value", "label", "display")
) {

	var limit: Int = 10

	// usually just sets the displayed value (visible text input)
	def focus(inputId: String, hiddenId: String): AnonFunc = {
		AnonFunc("event, ui", JsRaw(
			"""$("#%s").val(ui.item.%s); return false""".format(inputId, props._2)
		).cmd)
	}

	// sets both of the inputs' values (visible text input gets the label and the hidden field gets the id)
	def select(inputId: String, hiddenId: String, doAlso: String => JsCmd): AnonFunc = {
		val doSomethingOnTheServer =
      SHtml.jsonCall(JsRaw("ui.item.%s".format(props._1)), {
        case JString(str) => doAlso(str)
        case _ => Noop
      })
		AnonFunc("event, ui", JsRaw(
			"""var $i = $("#%s").val(ui.item.%s), $h = $("#%s").val(ui.item.%s); %s; return false""".format(
				inputId,
				props._2,
				hiddenId,
				props._1,
				doSomethingOnTheServer._2.toJsCmd
			)
		).cmd)
	}

	// responsible for rendering the list item (must set item.autocomplete data)
	def renderItem(inputId: String, hiddenId: String): AnonFunc = {
		AnonFunc("ul, item", JsRaw(
			"""return $("<li/>").data("item.autocomplete", item).append("<a>"+item.%s+"</a>").appendTo(ul)""".format(props._3)
		).cmd)
	}

	// the url of the function to be executed for each search
	private def ajaxUrl(funcName: String) =
		S.encodeURL(
      S.contextPath + "/" + LiftRules.liftPath + "/ajax?" + funcName + "=_" + (
        UrlHelpers.buildQueryString(_params.toSeq : _*).map("&" + _) openOr ""
      )
    )


	/**
	 * Attributes for input
	 */
	private val _attrs: mutable.Map[String, String] = mutable.Map()
	def attr(attributeName: String, attributeValue: String): JqAutoComplete = {
		_attrs += attributeName -> attributeValue
		this
	}
	def attrs(a: (String, String)*): JqAutoComplete = {
		_attrs ++= a
		this
	}
	private def attributes: List[(String, String)] = _attrs.toList


	/**
	 * Options for autocomplete
	 */
	private var _options: mutable.Map[String, JsExp] = mutable.Map(
		"minLength" -> Num(0)
	)
	private var _optionsFuncs: mutable.Map[String, JqAutoComplete.OptionFunc] = mutable.Map.empty
	def opt(key: String, value: JsExp): JqAutoComplete = {
		(key, value) match {
			case ("limit", l @ Num(_)) => limit = l.n.intValue()
			case (_, optionFunc: JqAutoComplete.OptionFunc) => _optionsFuncs += new Tuple2(key, optionFunc)
			case _ => _options += new Tuple2(key, value)
		}
		this
	}
	def opts(_options: (String, JsExp)*): JqAutoComplete = {
		_options.foreach(o => opt(o._1, o._2))
		this
	}
	private def options(inputId: String) = {
		JsObj(_options.toList:_*) +*
		JsObj(_optionsFuncs.map(optionFunc => (optionFunc._1, optionFunc._2.func(inputId))).toList:_*)
	}


  private var _params = mutable.Map[String, String]()
  def params(p: (String, String)*): JqAutoComplete = {
    _params ++= p
    this
  }


	def element: (String, String, Node) = {

		val f = (ignore: String) => {
			JsonResponse(onSearch(S.param("term").openOr(""), limit))
		}

		S.fmapFunc(S.SFuncHolder(f)) { func =>
			val id = Helpers.nextFuncName

			// `hidden` is the id of the input whose value is sent to the `onSubmit` function
			S.fmapFunc(S.SFuncHolder(str => {
				onSubmit(S.param(id).openOr(""), emptyForBlank(str))
			})) { hidden =>

				val opts = JsObj(
					"source" -> Str(ajaxUrl(func)),
					"focus" -> focus(id, hidden),
					"select" -> select(id, hidden, onSelect)
				) +* options(id)

				val ac = {
					JsRaw("""var $ac = $("#%s").autocomplete(%s).data("ac-hidden", "%s");
					|function acOnInput(){
					| if ($ac.val() != $ac.data("prev")) { $("#%s").val(""); }
					| $ac.data("prev", $ac.val());
					|}
          |$ac.data("prev",$ac.val())
          |  .on("input", function(e){ $ac.unbind("keypress.my"); acOnInput() })
          |  .on("keypress.my", function(e){ acOnInput() });
					|$ac.data("uiAutocomplete")._renderItem = %s""".stripMargin.format(
						id,
						opts.toJsCmd,
						hidden,
						hidden,
						renderItem(id, hidden).toJsCmd
					))
				}

				val (label, value) = (for {
					i <- initial.map(_.values)
					l <- i.get(props._2).map(_.toString)
					v <- i.get(props._1).map(_.toString)
				} yield (l, v)) openOr ("", "")

				(
					id,
					hidden,
					Group(
						attributes.foldLeft(<input type="text" id={id} name={id} value={label}/>)(_ % _) :+
						<input type="hidden" name={hidden} id={hidden} value={value} /> :+
						Script(JqOnLoad(ac))
					)
				)
			}
		}

	}

}
