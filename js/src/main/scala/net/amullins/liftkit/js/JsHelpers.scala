package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http.S
import net.liftweb.http.js._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsExp._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad
import net.liftweb.json.JsonAST.JValue

import scala.collection.mutable.ListBuffer
import scala.xml._
import scala.reflect.runtime.universe.{TypeTag, Type, typeOf}

import net.amullins.liftkit.common.UrlHelpers._
import net.amullins.liftkit.routing.RoutingHelpers


object JsHelpers {

  def Refresh(func: () => Any = () => {}): JsCmd =
  	(for (l <- S.location) yield
  		RedirectTo(RoutingHelpers.locLinkParts(l)._1, () => func.apply())
  	) openOr Noop

  def Refresh(params: Map[String, List[String]]): JsCmd =
    (for (l <- S.location) yield {
      val _params = params.foldLeft(List[(String, String)]()) { case (s, (k, vals)) => s ::: vals.map(v => k -> v) }
  		RedirectTo(RoutingHelpers.locLinkParts(l)._1 + "?" + paramsToUrlParams(_params))
  	}) openOr Noop

  def RefreshWithoutParams(func: () => Any = () => {}): JsCmd =
  	(for (l <- S.location) yield {
  		RedirectTo(makeAbsolute(RoutingHelpers.locLinkParts(l)._1).withoutParams, () => func.apply())
    }) openOr Noop

  def RedirectToLoc(locName: String, func: () => Any = () => {}): JsCmd =
  	href(locName).map(RedirectTo(_, () => func.apply())) openOr Noop


	def anon(exp: JsExp): JsExp = anon_*(exp.toJsCmd)
	def anon(cmd: JsCmd): JsExp = anon_*(cmd.toJsCmd)
	private def anon_*(str: String): JsExp = JsRaw("(function($){\n%s;\n})(jQuery)".format(str))

	def ready(cmd: JsCmd) = <tail>{Script(JqOnLoad(cmd))}</tail>

	def anyToJsExp[T: TypeTag](in: T): JsExp = anyToJsExp(typeOf[T], in)

	// will only correctly convert maps one level deep due to non-existent TypeTags
	def anyToJsExp[T](t: Type, in: T): JsExp = {
		in match {
			case v: Boolean => JsExp.boolToJsExp(v)
			case v: Double => JsExp.doubleToJsExp(v)
			case v: Float => JsExp.floatToJsExp(v)
			case v: Int => JsExp.intToJsExp(v)
			case v: JValue => JsExp.jValueToJsExp(v)
			case v: Long => JsExp.longToJsExp(v)
			case v: Num => JsExp.numToJValue(v)
			case v: Str => JsExp.strToJValue(v)
			case v: String => JsExp.strToJsExp(v)
			case v: JsExp => v
			case _ =>
				if (t <:< typeOf[scala.collection.Map[String, Any]])
					mapToJsObj(in.asInstanceOf[scala.collection.Map[String, Any]])
				else
					JsExp.strToJsExp(in.toString)
		}
	}

	def mapToJsObj(in: scala.collection.Map[String, Any]): JsObj =
		JsObj(in.toList.map { case (k,v) => (k, anyToJsExp(v)) } : _*)

	def encJs(ns: NodeSeq): String =
		new JsExp {
			def toJsCmd: String = fixHtmlFunc("inline", ns)(str => str)
		}.toJsCmd

}

trait JsHelpers extends JsExp {

	implicit def exp[T: TypeTag](in: T): JsExp = JsHelpers.anyToJsExp(in)

	def exp[T](t: Type, in: T): JsExp = JsHelpers.anyToJsExp(t, in)

	implicit def mapToObj(in: scala.collection.Map[String, Any]): JsObj = JsHelpers.mapToJsObj(in)

  def fixHtmlWithScripts(ns: NodeSeq, calc: (String, Seq[JsCmd]) => String): String = {
    (for (s <- S.session) yield {
    	val scriptsToLoad: ListBuffer[String] = ListBuffer.empty
    	val scrubbed = ("script" #> {(script: NodeSeq) =>
    		(for{
    			s: Node <- script.headOption
    			src <- s.attribute("src")
    		} yield {
    			scriptsToLoad += src.text
    			NodeSeq.Empty
    		}) getOrElse script
    	}).apply(ns)
    	val htmlString = {
		    fixHtmlAndJs("inline", scrubbed) match {
		      case (str, Nil) => calc(str, Nil)
		      case (str, cmds) => calc(str, cmds)
		    }
	    }
    	val onScriptLoaded: JsExp = AnonFunc(JsRaw("numScripts--; if(!numScripts) { onScriptsLoaded(); }").cmd)

    	if (scriptsToLoad.nonEmpty) {
    		JsRaw("(function($){%s})(jQuery)".format((
    			SetExp(JsVar("numScripts"), exp(scriptsToLoad.length)) &
    			Function("onScriptsLoaded", Nil, JsRaw(htmlString).cmd) &
    			scriptsToLoad.foldLeft(Noop)((scripts, script) => {
    				scripts & Call("$.getScript", exp(script), onScriptLoaded).cmd
    			})
    		).toJsCmd)).toJsCmd
    	} else {
        htmlString
    	}
    }) openOr Noop.toJsCmd
  }

}
