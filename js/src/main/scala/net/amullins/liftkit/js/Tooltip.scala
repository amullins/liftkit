package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http._
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JE._

import scala.xml._
import scala.collection.mutable
import scala.reflect.runtime.universe._


object AjaxTooltip {
	def apply(selector: String, title: String)(_contentFunc: => NodeSeq): AjaxTooltip = {
		val tt = new AjaxTooltip(selector) {
			protected def contentFunc = _contentFunc
		}
		tt.opt("content", Map("title" -> Map("text" -> title, "button" -> true)))
		tt
	}
}

abstract class AjaxTooltip(val selector: String) extends TT {

	protected def contentFunc: NodeSeq

	private val contentUrl =
		S.fmapFunc(S.NFuncHolder(() => {
			XhtmlResponse(Group(contentFunc), Full("text/html"), Nil, Nil, 200, false)
		})) {func => S.encodeURL(S.contextPath + "/" + LiftRules.liftPath + "/ajax?" + func + "=_")}

	this.opts(
		"show" -> Map("delay" -> 800, "solo" -> true),
		"hide" -> Map("delay" -> 800, "fixed" -> true),
		"position" -> Map("my" -> "top center", "at" -> "bottom center", "viewport" -> JsRaw("$(window)")),
		"style" -> Map("tip" -> JsObj("width" -> Num(8), "height" -> Num(5))),
		"content" -> Map(
			"text" -> "Loading...",
			"ajax" -> JsObj(
				"url" -> Str(contentUrl),
				"type" -> Str("GET")
			)
		)
	)

}


abstract class TemplatedAjaxTooltip(selector: String, title: String)
  extends AjaxTooltip(selector)
  with TemplatedTooltip
{
	protected def render: NodeSeq => NodeSeq
	protected def contentFunc = render.apply(html)
}


object Tooltip {
	def apply(selector: String, title: String, content: NodeSeq): Tooltip = {
		val tt = Tooltip(selector, content)
		tt.opts(
			"content" -> Map(
				"title" -> JsObj("text" -> Str(title)),
				"text" -> Html5.toString(Group(content))
			)
		)
		tt
	}

	def apply(selector: String, ns: NodeSeq): Tooltip = {
		val tt = Tooltip(selector)
		tt.opt("content", Map("text" -> Html5.toString(Group(ns))))
		tt
	}

	def apply(selector: String, options: (String, Any)*): Tooltip = {
		val tt = Tooltip(selector)
		tt.opts(options : _*)
		tt
	}
}

case class Tooltip(selector: String) extends TT {

	this.opts(
		"position" -> Map("my" -> "bottom center", "at" -> "top center"),
		"style" -> Map("tip" -> JsObj("width" -> Num(8), "height" -> Num(5)))
	)

}


trait TemplatedTooltip {
	protected def template: List[String]
	protected def html = Templates(template) openOr NodeSeq.Empty
}


trait TT extends JsCmd with JsHelpers {

	def selector: String

	def toJsCmd =
		"$(" + selector.encJs + ").qtip(" + options.map(o => (o._1, exp(o._2._1, o._2._2))).toJsCmd + ").addClass('tt')"

	protected val options = mutable.Map[String, (Type, Any)]()

	this.opts(
		"events" -> Map(
			"show" -> AnonFunc("e,a", JsRaw("$(a.elements.target).addClass('tt-opened')").cmd),
			"hide" -> AnonFunc("e,a", JsRaw("$(a.elements.target).removeClass('tt-opened')").cmd)
		)
	)

	def opt(option: String, value: Any): TT = opts((option, value))
	def opts[OT: TypeTag](_options: (String, OT)*): TT = {
		_options foreach { case (key, _value) =>
			typeOf[OT] match {
				case v if v <:< typeOf[scala.collection.Map[String, Any]] =>
					val value = _value.asInstanceOf[scala.collection.Map[String, Any]]

					options.get(key) match {
						case Some((ct, cv)) if ct <:< typeOf[scala.collection.Map[String, Any]] =>
							options += (key -> (ct, cv.asInstanceOf[scala.collection.Map[String, Any]] ++ value))
						case Some((ct, cv)) =>
							options += (key -> (ct, value))
						case _ =>
							options += (key -> (v, value))
					}

				case v =>
					options += (key -> (v, _value))
			}
		}

		this
	}

}
