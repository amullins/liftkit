package net.amullins.liftkit.js

import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.json._
import net.liftweb.http._
import net.liftweb.http.js._
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.jquery.JqJsCmds.JqOnLoad
import net.liftweb.http.S

import scala.xml._
import scala.collection.mutable
import scala.reflect.runtime.universe._

import net.amullins.liftkit.common.HtmlHelpers


object Select {

	def apply(
		placeholder: Box[String],
		values: Seq[(String, String)],
		selected: Box[String],
		onSubmit: String => Unit,
		onSelect: Box[Any => JsCmd] = Empty
	) =
		new Select[Box[String]](placeholder, values, selected, onSubmit, onSelect)

}

class Select[S : TypeTag](
	placeholder: Box[String],
	values: Seq[(String, String)],
	selected: S,
	onSubmit: String => Unit,
	onSelect: Box[Any => JsCmd] = Empty
) {

	/**
	 * Options for chosen
	 */
	private var _options: mutable.Map[String, JsExp] = mutable.Map(
		"delay" -> Num(500),
		"minLength" -> Num(3)
	)
	def opt(key: String, value: JsExp): Select[S] = {
		key match {
			case "nosearch" if value == JsTrue => _options += ("disable_search_threshold" -> Num(values.length + 1))
			case _ => _options += new Tuple2(key, value)
		}
		this
	}
	private def options = JsObj(_options.toList:_*)

	/**
	 * Attributes for select
	 */
	private val _attrs: mutable.Map[String, String] = mutable.Map()
	private val _dataPlaceholder = placeholder.map(p => ("data-placeholder", p)).toList
	def attr(attributeName: String, attributeValue: String): Select[S] = {
		_attrs += attributeName -> attributeValue
		this
	}
	def attrs(_attributes: (String, String)*): Select[S] = {
		_attrs ++= _attributes
		this
	}
	private def attributes: List[(String, String)] = {
		val _attributes = (_attrs.toList ::: _dataPlaceholder) :+ "tabindex" -> "0"
		typeOf[S] match {
			case s if s =:= typeOf[List[(String, String)]] => _attributes :+ ("multiple", "multiple")
			case _ => _attributes
		}
	}


	def change = {
		val onServer = SHtml.jsonCall(JsRaw("$(this).val()"), (args: JValue) => {
			(for (onSelectFunc <- onSelect) yield {
				args match {
					// single select
					case JString(str) =>
						onSelectFunc(str)

					// multi select
					case JArray(lst: List[JValue]) =>
						onSelectFunc(lst.flatMap {
							case JString(str) => Full(str)
							case _ => Empty
						})

					case _ => onSelectFunc(Empty)
				}
			}) openOr Noop
		})
		if (onSelect.isDefined) {
			JsRaw("%s; return false".format(onServer._2.toJsCmd)).cmd
		} else Noop
	}


	val selectedOptions = {
		placeholder.map(p => <option></option>).toSeq ++ values.map(v => {
			selected match {
				case Full(str: String) if v._1 == str =>
          <option value={v._1}>{v._2}</option> % HtmlHelpers.selectedAttribute(true)
				case _ =>
          <option value={v._1}>{v._2}</option>
			}
		})
	}


	def element = {

		val s = {
			S.fmapFunc(S.SFuncHolder(onSubmit))(id =>
				(attributes :+ "onchange" -> change.toJsCmd).foldLeft(
					<select id={id} name={id}>{selectedOptions}</select>)(_ % _)
			)
		}

		Group(
			s :+
			Script(JqOnLoad(JsRaw("""$("select[name='%s']").chosen(%s)""".format(
				(s \\ "@name").text,
				options.toJsCmd
			)).cmd))
		)

	}

}