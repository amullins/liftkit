import sbt._
import sbt.Keys._

import sbtrelease.ReleasePlugin._
import com.typesafe.sbt.SbtPgp._
import xerial.sbt.Sonatype.SonatypeKeys._


object LiftKit extends Build {
	import Resolvers._


  lazy val js = MyProject("js", Dependencies.js).dependsOn(common, routing)
  lazy val routing = MyProject("routing", Dependencies.routing).dependsOn(common)
  lazy val snippet = MyProject("snippet", Dependencies.snippet).dependsOn(common)
  lazy val common = MyProject("common", Dependencies.common)
  lazy val io = MyProject("io", Dependencies.io)
  lazy val mapper = MyProject("mapper", Dependencies.mapper).dependsOn(common, js)


  private def MyProject(
  	projectName: String,
  	projectDependencies: Seq[ModuleID],
  	projectPrefix: String = "liftkit-"
  ): Project = {
  	val projectFullName = projectPrefix + projectName
  	val projectSettings =
      baseSettings :+
  			(name := projectFullName) :+
  			(libraryDependencies ++= projectDependencies)

  	Project(id = projectFullName, base = file(projectName), settings = projectSettings)
  }


  val pomExtraXml = {
    <url>https://bitbucket.org/amullins/liftkit</url>
      <licenses>
        <license>
          <name>BSD-style</name>
          <url>https://bitbucket.org/amullins/liftkit/src/master/LICENSE.md</url>
          <distribution>repo</distribution>
        </license>
      </licenses>
      <scm>
        <url>git@bitbucket.org:amullins/liftkit.git</url>
        <connection>scm:git:git@bitbucket.org:amullins/liftkit.git</connection>
      </scm>
      <developers>
        <developer>
          <id>amullins</id>
          <name>Andrew Mullins</name>
          <url>http://andrewmullins.net</url>
        </developer>
      </developers>
  }


  val baseSettings = {
 		Defaults.defaultSettings ++
    releaseSettings ++
    xerial.sbt.Sonatype.sonatypeSettings ++
 		Seq[Def.Setting[_]](
 			resolvers ++= Seq(maven2, mavenLocal, googleapis, sonatypeSnapshots, sonatype, apache),
 			scalacOptions ++= Seq(
 				"-language:implicitConversions",
 				"-language:reflectiveCalls",
 				"-language:existentials",
 				"-language:higherKinds",
 				"-deprecation",
 				"-feature",
 				"-unchecked"
 			),
      pgpReadOnly := false,
      publishTo := {
        val nexus = "https://oss.sonatype.org/"
        if (version.value.trim.endsWith("SNAPSHOT"))
          Some("snapshots" at nexus + "content/repositories/snapshots")
        else
          Some("releases"  at nexus + "service/local/staging/deploy/maven2")
      },
      publishMavenStyle := true,
      publishArtifact in Test := false,
      pomIncludeRepository := { _ => false },
      pomExtra := pomExtraXml,
 			unmanagedBase := file("lib"),
 			ivyXML := Dependencies.globalExclusions,
 			organization := "net.amullins",
 			scalaVersion := "2.11.2",
      crossScalaVersions := Seq("2.10.3", "2.11.2")
 		)
 	}

}


object Resolvers {
	val maven2 =              "Java.net Maven2 Repo"  at "http://download.java.net/maven/2/"
	val mavenLocal =          "Local Maven"           at Path.userHome.asFile.toURI.toURL + ".m2/repository"
	val scalaToolsSnapshots = "Scala Tools Snapshot"  at "http://scala-tools.org/repo-snapshots/"
	val scalaToolsReleases =  "Scala Tools Releases"  at "http://scala-tools.org/repo-releases"
	val googleapis =          "Google APIs"           at "http://mavenrepo.google-api-java-client.googlecode.com/hg/"
  val sonatype =            "sonatype.repo"         at "https://oss.sonatype.org/content/repositories/public/"
	val sonatypeSnapshots =   "Sonatype Snapshot"     at "https://oss.sonatype.org/content/repositories/snapshots"
  val apache =              "apache.repo"           at "https://repository.apache.org/content/repositories/snapshots/"
}
