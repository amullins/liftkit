import sbt._


object Dependencies {

	val liftVersion = "3.0-M2"

	val liftWebkit =        "net.liftweb"                     %%  "lift-webkit"         % liftVersion
  val liftMapper =        "net.liftweb"                     %%  "lift-mapper"         % liftVersion
	val guava =             "com.google.guava"                %   "guava"               % "11.0.2"
  val libphonenumber =    "com.googlecode.libphonenumber"   %   "libphonenumber"      % "6.0"
  val httpClient =        "org.apache.httpcomponents"       %   "httpclient"          % "4.1.3"
  val apacheValidator =   "commons-validator"               %   "commons-validator"   % "1.4.0"
	val slf4jApi =          "org.slf4j"                       %   "slf4j-api"           % "1.7.1"
	val slf4jJcl =          "org.slf4j"                       %   "jcl-over-slf4j"      % "1.7.1"
	val slf4jLog4j =        "org.slf4j"                       %   "log4j-over-slf4j"    % "1.7.1"
	val tagsoup =						"org.ccil.cowan.tagsoup" 					% 	"tagsoup" 						% "1.2.1"
	val ostermiller =				"org.ostermiller" 								% 	"utils" 							% "1.07.00"


	val compileScope = Seq(
		liftWebkit, guava, httpClient, slf4jApi, slf4jJcl, slf4jLog4j, tagsoup, ostermiller
	).map(_.withSources())

	val scalatest = "org.scalatest" %%  "scalatest" % "2.2.2"   % "test"


  val globalExclusions = <dependencies>
    <exclude org="commons-logging" artifact="commons-logging" />
  </dependencies>


	// Groups
	val js: Seq[ModuleID] =
		scalatest +: compileScope

  val routing: Seq[ModuleID] =
    scalatest +: compileScope

  val snippet: Seq[ModuleID] =
    scalatest +: compileScope

  val common: Seq[ModuleID] =
    scalatest +: liftMapper +: libphonenumber +: compileScope

  val io: Seq[ModuleID] =
    scalatest +: Seq(liftWebkit, guava, slf4jApi, slf4jJcl, slf4jLog4j)

  val mapper: Seq[ModuleID] =
    scalatest +: Seq(liftWebkit, liftMapper, libphonenumber, apacheValidator, slf4jApi, slf4jJcl, slf4jLog4j)

}
